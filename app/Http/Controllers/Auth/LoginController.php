<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function redirectTo()
    {
        $pengurus = auth()->user()->IsPengurus();
        $admin = auth()->user()->IsAdmin();
        $penulis_article = auth()->user()->IsPenulisArticle();
        $order_guest = auth()->user()->IsOrderGuest();
        
        if ($pengurus){
            return '/home';
        }
        else if($admin){
            return '/home';
        }
        else if($penulis_article){
            return '/penulis_article';
        }
        else if($order_guest){
            return '/home';
        }
        else{
            return '/home';
        }
        return '/user';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
