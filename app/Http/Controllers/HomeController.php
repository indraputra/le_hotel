<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Kamar;
use App\Model\Layanan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kamar_kotor = kamar::where('status','Dirty')->count();
        $kamar_terpakai = kamar::where('status','In House')->count();
        $kamar_bersih_default = kamar::where('status','Clean')->count();
        $kamar_bersih = $kamar_bersih_default - 1;

        return view('home',compact('kamar_bersih','kamar_terpakai','kamar_kotor'));
    }

    public function user()
    {
        $kamar = kamar::with('kategori')->where('id_kamar','>',0)->get();
        $layanan = Layanan::with('kategori_layanan')->orderBy('kategori_layanan_id', 'asc')->get();
        return view('modul.user.user',compact('kamar','layanan'));
    }
}
