<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\TipebarangImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use App\Model\TipeBarang;

class TipeBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('modul.gudang.tipe_barang.index');
    }
    
    public function index($id_tipe_barang = null)
    {
        if ($id_tipe_barang) {
            echo json_encode(TipeBarang::find($id_tipe_barang));
        }else {
            echo json_encode(TipeBarang::all());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new TipeBarang;
        $data->nama_tipe = $request->nama_tipe;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = TipeBarang::find($request->id_tipe_barang);
        $data->nama_tipe = $request->nama_tipe;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_tipe_barang)
    {
        $test = TipeBarang::find($id_tipe_barang)->delete();
        // dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    public function import_excel(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_satuan',$nama_file);
 
        // import data
        Excel::import(new TipebarangImport, public_path('/file_satuan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/tipe_barang');
    }
}
