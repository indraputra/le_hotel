<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Layanan;
use App\Model\KategoriLayanan;
use App\Imports\SuplierImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class LayananController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        $kategori = KategoriLayanan::where('status', 'Aktif')->get();
        return view('modul.layanan.layanan.index', compact('kategori'));
    }
    
    public function index($id_layanan = null)
    {
        if ($id_layanan) {
            echo json_encode(Layanan::find($id_layanan));
        }else {
            echo json_encode(Layanan::with('kategori_layanan')->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Layanan;
        $data->kategori_layanan_id = $request->kategori_layanan_id;
        $data->nama_layanan = $request->nama_layanan;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->harga_layanan = $request->harga_layanan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Layanan::find($request->id_layanan);
        $data->kategori_layanan_id = $request->kategori_layanan_id;
        $data->nama_layanan = $request->nama_layanan;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->harga_layanan = $request->harga_layanan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_layanan)
    {
        $test = Layanan::find($id_layanan)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    public function import_excel(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_satuan',$nama_file);
 
        // import data
        Excel::import(new LayananImport, public_path('/file_satuan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/suplier');
    }



    //Front Office

    public function view_fo()
    {
        $kategori = KategoriLayanan::where('status', 'Aktif')->get();
        return view('modul.layanan.layanan.index_fo', compact('kategori'));
    }
    
    public function index_fo($id_layanan = null)
    {
        if ($id_layanan) {
            echo json_encode(Layanan::find($id_layanan));
        }else {
            echo json_encode(Layanan::with('kategori_layanan')->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {
        $data = new Layanan;
        $data->kategori_layanan_id = $request->kategori_layanan_id;
        $data->nama_layanan = $request->nama_layanan;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->harga_layanan = $request->harga_layanan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request)
    {
        $data = Layanan::find($request->id_layanan);
        $data->kategori_layanan_id = $request->kategori_layanan_id;
        $data->nama_layanan = $request->nama_layanan;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->harga_layanan = $request->harga_layanan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_layanan)
    {
        $test = Layanan::find($id_layanan)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    public function import_excel_fo(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_satuan',$nama_file);
 
        // import data
        Excel::import(new LayananImport, public_path('/file_satuan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/layanan_fo');
    }
}
