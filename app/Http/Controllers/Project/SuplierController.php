<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\SuplierImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use App\Model\Suplier;

class SuplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('modul.gudang.suplier.index');
    }
    
    public function index($id_suplier = null)
    {
        if ($id_suplier) {
            echo json_encode(Suplier::find($id_suplier));
        }else {
            echo json_encode(Suplier::all());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Suplier;
        $data->nama_suplier = $request->nama_suplier;
        $data->no_telp = $request->no_telp;
        $data->alamat_suplier = $request->alamat_suplier;
        $data->status = $request->status;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Suplier::find($request->id_suplier);
        $data->nama_suplier = $request->nama_suplier;
        $data->no_telp = $request->no_telp;
        $data->alamat_suplier = $request->alamat_suplier;
        $data->status = $request->status;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_suplier)
    {
        $test = Suplier::find($id_suplier)->delete();
        // dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    public function import_excel(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_satuan',$nama_file);
 
        // import data
        Excel::import(new SuplierImport, public_path('/file_satuan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/suplier');
    }
}
