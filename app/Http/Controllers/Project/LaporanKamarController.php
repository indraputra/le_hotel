<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Kamar;
use Carbon\Carbon;

class LaporanKamarController extends Controller
{
    public function view(){
        return view('modul.laporan.laporan_kamar.index');
    }

    public function preview(Request $request){
        $input1 = request('tanggal1');
        $input2 = request('tanggal2');
        $today    = new Carbon($input2);
        $today->addDay();  //pass l for lion aphabet in format 
        $kamar = Kamar::where('id_kamar','>',0)->whereBetween('created_at',[$input1, $today])->get();
        // $kamar = Kamar::all();
        // $counts = Carbon::today();
        // $count = $counts->addDays(30);
        // $today = Carbon::now()->toDateToString();
        $count = Kamar::where('id_kamar','>',0)->whereBetween('created_at', array($input1, $today))->count();
        return view('modul.laporan.laporan_kamar.preview',compact("kamar","count","today", "input1", "input2"));
    }
}
