<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Fasilitas;
use App\Model\kategori_kamar;
use App\Model\Kamar;
use Alert;

class KamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        $test = "abcd";
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        return view('modul.ruang_kamar.kamar.index',compact("kategori","fasilitas","test"));
    }
    
    public function index($id_kamar = null)
    {
        if ($id_kamar) {
            echo json_encode(Kamar::find($id_kamar));
        }else {
            echo json_encode(Kamar::with('kategori','fasilitas')->where('id_kamar','>',0)->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = new Kamar;
        $data->nama_kamar = $request->nama_kamar;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_kamar = $request->harga_kamar;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();
        return back();
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kamar $id_kamar)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $kamar = Kamar::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.kamar.detail',['data'=>$id_kamar] ,compact("kategori","fasilitas","kamar"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kamar $id_kamar)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $kamar = Kamar::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.kamar.edit',['data'=>$id_kamar] ,compact("kategori","fasilitas","kamar"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_kamar)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = Kamar::where('id_kamar',$id_kamar)->first();
        $data->nama_kamar = $request->nama_kamar;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_kamar = $request->harga_kamar;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        Alert::success('Data berhasil Terupdate', 'success');
        return redirect()->route("kamar.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_kamar)
    {
        $test = Kamar::find($id_kamar)->delete();
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }





    //Front Office
    public function view_fo()
    {
        $test = "abcd";
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        return view('modul.ruang_kamar.kamar.index_fo',compact("kategori","fasilitas","test"));
    }
    
    public function index_fo($id_kamar = null)
    {
        if ($id_kamar) {
            echo json_encode(Kamar::find($id_kamar));
        }else {
            echo json_encode(Kamar::with('kategori','fasilitas')->where('id_kamar','>',0)->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = new Kamar;
        $data->nama_kamar = $request->nama_kamar;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_kamar = $request->harga_kamar;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();
        return back();
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo(Kamar $id_kamar)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $kamar = Kamar::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.kamar.detail_fo',['data'=>$id_kamar] ,compact("kategori","fasilitas","kamar"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo(Kamar $id_kamar)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $kamar = Kamar::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.kamar.edit_fo',['data'=>$id_kamar] ,compact("kategori","fasilitas","kamar"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request, $id_kamar)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = Kamar::where('id_kamar',$id_kamar)->first();
        $data->nama_kamar = $request->nama_kamar;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_kamar = $request->harga_kamar;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        Alert::success('Data berhasil Terupdate', 'success');
        return redirect()->route("kamar.index_fo");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_kamar)
    {
        $test = Kamar::find($id_kamar)->delete();
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    //Front Office

    public function view_og()
    {
        $test = "abcd";
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        return view('modul.ruang_kamar.kamar.index_og',compact("kategori","fasilitas","test"));
    }
    
    public function index_og($id_kamar = null)
    {
        if ($id_kamar) {
            echo json_encode(Kamar::find($id_kamar));
        }else {
            echo json_encode(Kamar::with('kategori','fasilitas')->where('id_kamar','>',0)->get());
        }
    }

    /**
     * Show the ogrm ogr creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_og()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_og(Request $request)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = new Kamar;
        $data->nama_kamar = $request->nama_kamar;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_kamar = $request->harga_kamar;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();
        return back();
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_og(Kamar $id_kamar)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $kamar = Kamar::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.kamar.detail_og',['data'=>$id_kamar] ,compact("kategori","fasilitas","kamar"));
    }

    /**
     * Show the ogrm ogr editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_og(Kamar $id_kamar)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $kamar = Kamar::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.kamar.edit_og',['data'=>$id_kamar] ,compact("kategori","fasilitas","kamar"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_og(Request $request, $id_kamar)
    {
        $data = Kamar::where('id_kamar',$id_kamar)->first();
        $data->nama_kamar = $request->nama_kamar;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_kamar = $request->harga_kamar;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        Alert::success('Data berhasil Terupdate', 'success');
        return redirect()->route("kamar_og.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_og($id_kamar)
    {
        $test = Kamar::find($id_kamar)->delete();
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }
}
