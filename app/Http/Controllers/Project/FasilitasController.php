<?php

namespace App\Http\Controllers\Project;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Fasilitas;
use App\Imports\FasilitasKamarImport;
use Maatwebsite\Excel\Facades\Excel;

class FasilitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('modul.ruang_kamar.fasilitas.index');
    }
    
    public function index($id_fasilitas = null)
    {
        if ($id_fasilitas) {
            echo json_encode(Fasilitas::find($id_fasilitas));
        }else {
            echo json_encode(Fasilitas::all());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Fasilitas;
        $data->nama_fasilitas = $request->nama_fasilitas;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Fasilitas::find($request->id_fasilitas);
        $data->nama_fasilitas = $request->nama_fasilitas;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_fasilitas)
    {
        $test = Fasilitas::find($id_fasilitas)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    public function import_excel(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_satuan',$nama_file);
 
        // import data
        Excel::import(new FasilitasKamarImport, public_path('/file_satuan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/fasilitas');
    }



    //Front Office
    public function view_fo()
    {
        return view('modul.ruang_kamar.fasilitas.index_fo');
    }
    
    public function index_fo($id_fasilitas = null)
    {
        if ($id_fasilitas) {
            echo json_encode(Fasilitas::find($id_fasilitas));
        }else {
            echo json_encode(Fasilitas::all());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {
        $data = new Fasilitas;
        $data->nama_fasilitas = $request->nama_fasilitas;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request)
    {
        $data = Fasilitas::find($request->id_fasilitas);
        $data->nama_fasilitas = $request->nama_fasilitas;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_fasilitas)
    {
        $test = Fasilitas::find($id_fasilitas)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    public function import_excel_fo(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_satuan',$nama_file);
 
        // import data
        Excel::import(new FasilitasKamarImport, public_path('/file_satuan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/fasilitas_fo');
    }
}
