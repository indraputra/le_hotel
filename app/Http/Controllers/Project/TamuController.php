<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Tamu;
use App\Model\Booking;
use Alert;

class TamuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('modul.tamu.tamu.index');
    }
    
    public function index($id_tamu = null)
    {
        if ($id_tamu) {
            echo json_encode(Tamu::find($id_tamu));
        }else {
            echo json_encode(Tamu::all());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Tamu;
        $data->nama_tamu = $request->nama_tamu;
        $data->tipe_tamu = $request->tipe_tamu;
        $data->alamat_tamu = $request->alamat_tamu;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->tempat_lahir = $request->tempat_lahir;
        $data->no_ktp = $request->no_ktp;
        $data->agama = $request->agama;
        $data->pendidikan = $request->pendidikan;
        $data->no_telp = $request->no_telp;
        $data->status = "In House";
        $data->save();

        $id = $request->input('id_booking');
        $booking = Booking::where('id_booking', $id)->first();
        $booking->status = "Accept";
        $booking->save();

        Alert::success('Data berhasil Terupdate', 'success');
        echo json_encode(array('status'=>'Sukses Simpan'));
        return redirect('booking');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Tamu::find($request->id_tamu);
        $data->nama_tamu = $request->nama_tamu;
        $data->tipe_tamu = $request->tipe_tamu;
        $data->alamat_tamu = $request->alamat_tamu;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->tempat_lahir = $request->tempat_lahir;
        $data->no_ktp = $request->no_ktp;
        $data->agama = $request->agama;
        $data->pendidikan = $request->pendidikan;
        $data->no_telp = $request->no_telp;
        $data->status = $request->status;
        $data->save();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_tamu)
    {
        $test = Tamu::find($id_tamu)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }




    //Front Office
    public function view_fo()
    {
        return view('modul.tamu.tamu.index_fo');
    }
    
    public function index_fo($id_tamu = null)
    {
        if ($id_tamu) {
            echo json_encode(Tamu::find($id_tamu));
        }else {
            echo json_encode(Tamu::all());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {
        $data = new Tamu;
        $data->nama_tamu = $request->nama_tamu;
        $data->tipe_tamu = $request->tipe_tamu;
        $data->alamat_tamu = $request->alamat_tamu;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->tempat_lahir = $request->tempat_lahir;
        $data->no_ktp = $request->no_ktp;
        $data->agama = $request->agama;
        $data->pendidikan = $request->pendidikan;
        $data->no_telp = $request->no_telp;
        $data->status = "In House";
        $data->save();

        $id = $request->input('id_booking');
        $booking = Booking::where('id_booking', $id)->first();
        $booking->status = "Accept";
        $booking->save();

        Alert::success('Data berhasil Terupdate', 'success');
        echo json_encode(array('status'=>'Sukses Simpan'));
        return redirect('booking_fo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request)
    {
        $data = Tamu::find($request->id_tamu);
        $data->nama_tamu = $request->nama_tamu;
        $data->tipe_tamu = $request->tipe_tamu;
        $data->alamat_tamu = $request->alamat_tamu;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->tempat_lahir = $request->tempat_lahir;
        $data->no_ktp = $request->no_ktp;
        $data->agama = $request->agama;
        $data->pendidikan = $request->pendidikan;
        $data->no_telp = $request->no_telp;
        $data->status = $request->status;
        $data->save();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_tamu)
    {
        $test = Tamu::find($id_tamu)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }
}
