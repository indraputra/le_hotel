<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SewaKamar;
use App\Model\Tamu;
use App\Model\Kamar;
use Carbon\Carbon;

class SewaKamarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('modul.transaksi.sewa_kamar.index');
    }
    
    public function index($id_sewa_kamar = null)
    {
        if ($id_sewa_kamar) {
            echo json_encode(SewaKamar::find($id_sewa_kamar));
        }else {
            echo json_encode(SewaKamar::where("status","Check In")->with("tamu","kamar")->get());
        }
    }

    public function view_check_out()
    {
        return view('modul.transaksi.check_out_kamar.index');
    }
    
    public function index_check_out($id_sewa_kamar = null)
    {
        if ($id_sewa_kamar) {
            echo json_encode(SewaKamar::where('id_sewa_kamar',$id_sewa_kamar)->get());
        }else {
            $today = Carbon::today()->toDateString();
            // $query = SewaKamar::where('tanggal_check_out',$today)->with("tamu","kamar")->get();
            echo json_encode(SewaKamar::where('tanggal_check_out',$today)->with("tamu","kamar")->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = SewaKamar::find($request->id_sewa_kamar);
        $data->status = $request->status;
        $data->update();

        $id_kamar = $request->kamar_id;
        $kamar = Kamar::find($id_kamar);
        $kamar->status = "Dirty";
        $kamar->save();

        $id_tamu = $request->tamu_id;
        $tamu = Tamu::find($id_tamu);
        $tamu->status = "Return";
        $tamu->save();

        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_sewa_kamar)
    {
        $test = SewaKamar::find($id_sewa_kamar)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    //Front Office
    public function view_fo()
    {
        return view('modul.transaksi.sewa_kamar.index_fo');
    }
    
    public function index_fo($id_sewa_kamar = null)
    {
        if ($id_sewa_kamar) {
            echo json_encode(SewaKamar::find($id_sewa_kamar));
        }else {
            echo json_encode(SewaKamar::with("tamu","kamar")->get());
        }
    }

    public function view_check_out_fo()
    {
        return view('modul.transaksi.check_out_kamar.index_fo');
    }
    
    public function index_check_out_fo($id_sewa_kamar = null)
    {
        if ($id_sewa_kamar) {
            echo json_encode(SewaKamar::find($id_sewa_kamar));
        }else {
            $today = Carbon::today()->toDateString();
            // $query = SewaKamar::where('tanggal_check_out',$today)->with("tamu","kamar")->get();
            echo json_encode(SewaKamar::where('tanggal_check_out',$today)->with("tamu","kamar")->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request)
    {
        $data = SewaKamar::find($request->id_sewa_kamar);
        $data->nama_kategori = $request->nama_kategori;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_sewa_kamar)
    {
        $test = SewaKamar::find($id_sewa_kamar)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));

    }
    
    public function print_preview($id_sewa_kamar)
    {
        $data = SewaKamar::select('total_biaya')->where('id_sewa_kamar',$id_sewa_kamar)->value('total_biaya');
        $today = Carbon::today();
        return view('modul.transaksi.sewa_kamar.preview',compact("data","today"));
    }
}
