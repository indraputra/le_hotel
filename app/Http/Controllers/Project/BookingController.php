<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Booking;
use App\Model\Tamu;
use App\Model\Pembayaran;
use App\Model\PembayaranList;
use App\Model\Ruangan;
use App\Model\Kamar;
use App\Model\SewaKamar;
use App\Model\SewaRuangan;
use Alert;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
    	$kamar = Kamar::where('status','Clean')->get();
        $ruangan = Ruangan::where('status','Clean')->get();
        $tamugan = Tamu::all();
        
        return view('modul.transaksi.booking.index',compact('kamar','ruangan','tamugan'));
    }

    public function calender()
    {
        $booking = Booking::with('kamar','ruangan')->get();
        
        return view('modul.transaksi.booking.calendar',compact('booking'));
    }
    
    public function index($id_booking = null)
    {
        if ($id_booking) {
            echo json_encode(Booking::find($id_booking));
        }else {
            echo json_encode(Booking::with('ruangan','kamar')->orderBy('created_at','desc')->get());
        }

    }

    public function tamuList($id_tamu = null)
    {
        if ($id_tamu) {
            echo json_encode(Tamu::find($id_tamu));
        }else {
            echo json_encode(Tamu::where('status','In House')->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Booking;
        $data->atas_nama = $request->atas_nama;
        $data->ruangan_id = $request->ruangan_id;
        $data->kamar_id = $request->kamar_id;
        $data->tanggal_penempatan = $request->tanggal_penempatan;
        $data->status = "Pending";
        $data->save();
        Alert::success('Success Message', 'Optional Title');

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    public function storeKamar(Request $request)
    {
        $data = new SewaKamar;
        $data->tamu_id = $request->tamu_id;
        $data->kamar_id = $request->kamar_id;
        $data->jumlah_pengunjung = $request->jumlah_pengunjung;
        $data->tanggal_check_in = $request->tanggal_check_in;
        $data->tanggal_check_out = $request->tanggal_check_out;
        $data->total_biaya = $request->total_biaya;
        $data->lamanya = $request->lamanya;
        $data->status = "Check In";
        $data->save();

        $pembayaran = new Pembayaran;
        $pembayaran->tamu_id = $request->tamu_id;
        $pembayaran->status = "Belum Lunas";
        $pembayaran->total_harga = $request->total_biaya;
        $pembayaran->total_bayar = 0;
        $pembayaran->save();
        
        $last_id = $pembayaran->id_pembayaran;
        // dd($last_id);

        $item = new PembayaranList;
        $item->pembayaran_id = $last_id;
        $item->kamar_id = $request->kamar_id;
        $item->pembelian = "Sewa Kamar";
        $item->ruangan_id = $request->ruangan_id;
        $item->layanan_id = $request->layanan_id;
        $item->total_harga = $request->total_biaya;
        $item->save();

        $id = $request->id_booking;

        $booking = Booking::find($id);
        $booking->status = "Arrival";
        $booking->save();
        
        $id_kamar = $request->kamar_id;
        $kamar = Kamar::find($id_kamar);
        $kamar->status = "In House";
        $kamar->save();
        dd($kamar);
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    public function storeRuangan(Request $request)
    {
        $data = new SewaRuangan;
        $data->tamu_id = $request->tamu_id;
        $data->ruangan_id = $request->ruangan_id;
        $data->tanggal_check_in = $request->tanggal_check_in;
        $data->tanggal_check_out = $request->tanggal_check_out;
        $data->total_biaya = $request->total_biaya;
        $data->lamanya = $request->lamanya;
        $data->status = "Check In";
        $data->save();

        $pembayaran = new Pembayaran;
        $pembayaran->tamu_id = $request->tamu_id;
        $pembayaran->status = "Belum Lunas";
        $pembayaran->total_harga = $request->total_biaya;
        $pembayaran->total_bayar = 0;
        $pembayaran->save();
        
        $last_id = $pembayaran->id_pembayaran;
        // dd($last_id);

        $item = new PembayaranList;
        $item->pembayaran_id = $last_id;
        $item->ruangan_id = $request->ruangan_id;
        $item->pembelian = "Sewa Ruangan";
        $item->ruangan_id = $request->ruangan_id;
        $item->layanan_id = $request->layanan_id;
        $item->total_harga = $request->total_biaya;
        $item->save();

        $id = $request->id_booking;

        $booking = Booking::find($id);
        $booking->status = "Arrival";
        $booking->save();
        
        $id_ruangan = $request->ruangan_id;
        $kamar = Ruangan::find($id_ruangan);
        $kamar->status = "In House";
        $kamar->save();
        dd($kamar);
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Booking::find($request->id_booking);
        $data->atas_nama = $request->atas_nama;
        $data->ruangan_id = $request->ruangan_id;
        $data->kamar_id = $request->kamar_id;
        $data->tanggal_penempatan = $request->tanggal_penempatan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_booking)
    {
        $test = Booking::find($id_booking)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }






    //Front Office
    public function view_fo()
    {
        $kamar = Kamar::where('status','Clean')->get();
        $ruangan = Ruangan::where('status','Clean')->get();
        $tamugan = Tamu::all();
        
        return view('modul.transaksi.booking.index_fo',compact('kamar','ruangan','tamugan'));
    }
    
    public function index_fo($id_booking = null)
    {
        if ($id_booking) {
            echo json_encode(Booking::find($id_booking));
        }else {
            echo json_encode(Booking::with('ruangan','kamar')->orderBy('created_at','desc')->get());
        }

    }

    public function calender_fo()
    {
        $booking = Booking::with('kamar','ruangan')->get();
        
        return view('modul.transaksi.booking.calendar',compact('booking'));
    }

    public function tamuList_fo($id_tamu = null)
    {
        if ($id_tamu) {
            echo json_encode(Tamu::find($id_tamu));
        }else {
            echo json_encode(Tamu::where('status','In House')->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {
        $data = new Booking;
        $data->atas_nama = $request->atas_nama;
        $data->ruangan_id = $request->ruangan_id;
        $data->kamar_id = $request->kamar_id;
        $data->tanggal_penempatan = $request->tanggal_penempatan;
        $data->status = "Pending";
        $data->save();
        Alert::success('Success Message', 'Optional Title');

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    public function storeKamar_fo(Request $request)
    {
        $data = new SewaKamar;
        $data->tamu_id = $request->tamu_id;
        $data->kamar_id = $request->kamar_id;
        $data->jumlah_pengunjung = $request->jumlah_pengunjung;
        $data->tanggal_check_in = $request->tanggal_check_in;
        $data->tanggal_check_out = $request->tanggal_check_out;
        $data->total_biaya = $request->total_biaya;
        $data->lamanya = $request->lamanya;
        $data->status = "Check In";
        $data->save();

        $pembayaran = new Pembayaran;
        $pembayaran->tamu_id = $request->tamu_id;
        $pembayaran->status = "Belum Lunas";
        $pembayaran->total_harga = $request->total_biaya;
        $pembayaran->total_bayar = 0;
        $pembayaran->save();
        
        $last_id = $pembayaran->id_pembayaran;
        // dd($last_id);

        $item = new PembayaranList;
        $item->pembayaran_id = $last_id;
        $item->kamar_id = $request->kamar_id;
        $item->pembelian = "Sewa Kamar";
        $item->ruangan_id = $request->ruangan_id;
        $item->layanan_id = $request->layanan_id;
        $item->total_harga = $request->total_biaya;
        $item->save();

        $id = $request->id_booking;

        $booking = Booking::find($id);
        $booking->status = "Arrival";
        $booking->save();
        
        $id_kamar = $request->kamar_id;
        $kamar = Kamar::find($id_kamar);
        $kamar->status = "In House";
        $kamar->save();
        dd($kamar);
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request)
    {
        $data = Booking::find($request->id_booking);
        $data->atas_nama = $request->atas_nama;
        $data->ruangan_id = $request->ruangan_id;
        $data->kamar_id = $request->kamar_id;
        $data->tanggal_penempatan = $request->tanggal_penempatan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_booking)
    {
        $test = Booking::find($id_booking)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }
}
