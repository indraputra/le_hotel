<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PesanBarang;
use App\Model\Satuan;
use App\Model\TipeBarang;
use App\Model\Suplier;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use App\Imports\PesanBarangImport;

class PesanBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        $satuan = Satuan::where('status', 'Aktif')->get();
        $tipe = TipeBarang::where('status','Aktif')->get();
        $suplier = Suplier::where('status','Aktif')->get();
        return view('modul.gudang.pesan_barang.index', compact('satuan', 'tipe', 'suplier'));
    }
    
    public function index($id_pesan = null)
    {
        if ($id_pesan) {
            echo json_encode(PesanBarang::find($id_pesan));
        }else {
            echo json_encode(PesanBarang::with('satuan','suplier','tipe_barang')->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new PesanBarang;
        $data->nama_barang = $request->nama_barang;
        $data->satuan_id = $request->satuan_id;
        $data->tipe_id = $request->tipe_id;
        $data->suplier_id = $request->suplier_id;
        $data->harga = $request->harga;
        $data->jumlah = $request->jumlah;
        $data->status = "Pending";
        $data->tanggal_pemesanan = $request->tanggal_pemesanan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = kategori_kamar::find($request->id_kategori);
        $data->nama_kategori = $request->nama_kategori;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_pesan)
    {
        $test = PesanBarang::find($id_pesan)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    public function import_excel(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_satuan',$nama_file);
 
        // import data
        Excel::import(new PesanBarangImport, public_path('/file_satuan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/suplier');
    }
}
