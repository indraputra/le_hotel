<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Kamar;
use App\Model\Tamu;
use Carbon\Carbon;

class LaporanTamuController extends Controller
{
    public function view(){
        return view('modul.laporan.laporan_tamu.index');
    }

    public function preview(Request $request){
        $input1 = request('tanggal1');
        $input2 = request('tanggal2');
        $today    = new Carbon($input2);
        $today->addDay();  //pass l for lion aphabet in format 
        $kamar = Tamu::whereBetween('created_at',[$input1, $today])->get();
        // $kamar = Kamar::all();
        // $counts = Carbon::today();
        // $count = $counts->addDays(30);
        // $today = Carbon::now()->toDateToString();
        $count = Tamu::whereBetween('created_at', array($input1, $today))->count();
        return view('modul.laporan.laporan_tamu.preview',compact("kamar","count","today","input1","input2"));
    }
}
