<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Fasilitas;
use App\Model\kategori_kamar;
use App\Model\Ruangan;
use Alert;

class RuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.ruangan.index',compact("kategori","fasilitas","ruangan"));
    }

    public function index($id_ruangan = null)
    {
        if ($id_ruangan) {
            echo json_encode(Ruangan::find($id_ruangan));
        }else {
            echo json_encode(Ruangan::with('kategori','fasilitas')->where('id_ruangan','>',0)->get());
        }
    }

    /**
     * Show the ogrm ogr creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = new Ruangan;
        $data->nama_ruangan = $request->nama_ruangan;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_ruangan = $request->harga_ruangan;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();
        Alert::success('Data berhasil Tersimpan', 'success');
        return back();
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ruangan $id_ruangan)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view("modul.ruang_kamar.ruangan.show",["data" => $id_ruangan],compact("kategori","fasilitas","ruangan"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ruangan $id_ruangan)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view("modul.ruang_kamar.ruangan.edit",["data" => $id_ruangan],compact("kategori","fasilitas","ruangan"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_ruangan)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = Ruangan::where('id_ruangan',$id_ruangan)->first();
        $data->nama_ruangan = $request->nama_ruangan;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_ruangan = $request->harga_ruangan;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        Alert::success('Data berhasil Terupdate', 'success');
        return redirect()->route("ruangan.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_ruangan)
    {
        Ruangan::find($id_ruangan)->delete();
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }




    //Front Office
    public function view_fo()
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.ruangan.index_fo',compact("kategori","fasilitas","ruangan"));
    }

    public function index_fo($id_ruangan = null)
    {
        if ($id_ruangan) {
            echo json_encode(Ruangan::find($id_ruangan));
        }else {
            echo json_encode(Ruangan::with('kategori','fasilitas')->where('id_ruangan','>',0)->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = new Ruangan;
        $data->nama_ruangan = $request->nama_ruangan;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_ruangan = $request->harga_ruangan;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();
        Alert::success('Data berhasil Tersimpan', 'success');
        return back();
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo(Ruangan $id_ruangan)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view("modul.ruang_kamar.ruangan.show_fo",["data" => $id_ruangan],compact("kategori","fasilitas","ruangan"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo(Ruangan $id_ruangan)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view("modul.ruang_kamar.ruangan.edit_fo",["data" => $id_ruangan],compact("kategori","fasilitas","ruangan"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request, $id_ruangan)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = Ruangan::where('id_ruangan',$id_ruangan)->first();
        $data->nama_ruangan = $request->nama_ruangan;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_ruangan = $request->harga_ruangan;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        Alert::success('Data berhasil Terupdate', 'success');
        return redirect()->route("ruangan.index_fo");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_ruangan)
    {
        Ruangan::find($id_ruangan)->delete();
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    //Order Guest
    public function view_og()
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view('modul.ruang_kamar.ruangan.index_og',compact("kategori","fasilitas","ruangan"));
    }

    public function index_og($id_ruangan = null)
    {
        if ($id_ruangan) {
            echo json_encode(Ruangan::find($id_ruangan));
        }else {
            echo json_encode(Ruangan::with('kategori','fasilitas')->where('id_ruangan','>',0)->get());
        }
    }

    /**
     * Show the ogrm ogr creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_og()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_og(Request $request)
    {
        $foto = $request->file('foto');
        $nama_file = time()."_".$foto->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $foto->move($tujuan_upload,$nama_file);

        $data = new Ruangan;
        $data->nama_ruangan = $request->nama_ruangan;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_ruangan = $request->harga_ruangan;
        $data->foto = $nama_file;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();
        Alert::success('Data berhasil Tersimpan', 'success');
        return back();
        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_og(Ruangan $id_ruangan)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view("modul.ruang_kamar.ruangan.show_og",["data" => $id_ruangan],compact("kategori","fasilitas","ruangan"));
    }

    /**
     * Show the ogrm ogr editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_og(Ruangan $id_ruangan)
    {
        $kategori = kategori_kamar::where('status','aktif')->get();
        $fasilitas = Fasilitas::where('status','aktif')->get();
        $ruangan = Ruangan::with('kategori','fasilitas')->get();
        return view("modul.ruang_kamar.ruangan.edit_og",["data" => $id_ruangan],compact("kategori","fasilitas","ruangan"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_og(Request $request, $id_ruangan)
    {
        $data = Ruangan::where('id_ruangan',$id_ruangan)->first();
        $data->nama_ruangan = $request->nama_ruangan;
        $data->kategori_id = $request->kategori_id;
        $data->fasilitas_id = $request->fasilitas_id;
        $data->harga_ruangan = $request->harga_ruangan;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        Alert::success('Data berhasil Terupdate', 'success');
        return redirect()->route("ruangan_og.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_og($id_ruangan)
    {
        Ruangan::find($id_ruangan)->delete();
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }
}
