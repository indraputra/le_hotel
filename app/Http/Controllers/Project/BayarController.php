<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Bayar;
use App\Model\Pembayaran;

class BayarController extends Controller
{
    public function index($id_pembayaran = null)
    {
		echo json_encode(Bayar::where("pembayaran_id",$id_pembayaran)->get());
    }

    public function store(Request $request)
    {
    	$data = new Bayar;
        $data->pembayaran_id = $request->pembayaran_id;
        $data->bayar = $request->bayar;
        $data->keterangan = $request->keterangan;
        $data->save();

        $id = $request->pembayaran_id;
        $cek = $request->cek;
        $tb = $request->total_bayar;

        if ($cek <= $tb) {
            $booking = Pembayaran::find($id);
            $booking->total_bayar = $request->total_bayar;
            $booking->status = "Lunas";
            $booking->save();
        }else{
            $booking = Pembayaran::find($id);
            $booking->total_bayar = $request->total_bayar;
            $booking->save();
        }
    }
}
