<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SewaRuangan;
use App\Model\Tamu;
use App\Model\Ruangan;
use Carbon\Carbon;

class SewaRuaganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('modul.transaksi.sewa_ruangan.index');
    }
    
    public function index($id_sewa_ruangan = null)
    {
        if ($id_sewa_ruangan) {
            echo json_encode(SewaRuangan::find($id_sewa_ruangan));
        }else {
            echo json_encode(SewaRuangan::where("status","Check In")->with("tamu","ruangan")->get());
        }
    }

    public function view_check_out()
    {
        return view('modul.transaksi.check_out_ruangan.index');
    }
    
    public function index_check_out($id_sewa_ruangan = null)
    {
        if ($id_sewa_ruangan) {
            echo json_encode(SewaRuangan::where('id_sewa_ruangan',$id_sewa_ruangan)->get());
        }else {
            $today = Carbon::today()->toDateString();
            // $query = SewaKamar::where('tanggal_check_out',$today)->with("tamu","kamar")->get();
            echo json_encode(SewaRuangan::where('tanggal_check_out',$today)->with("tamu","ruangan")->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = SewaRuangan::find($request->id_sewa_ruangan);
        $data->status = $request->status;
        $data->save();

        $id_ruangan = $request->ruangan_id;
        $ruangan = Ruangan::find($id_ruangan);
        $ruangan->status = "Dirty";
        $ruangan->save();

        $id_tamu = $request->tamu_id;
        $tamu = Tamu::find($id_tamu);
        $tamu->status = "Return";
        $tamu->save();

        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_sewa_ruangan)
    {
        $test = SewaRuangan::find($id_sewa_ruangan)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    //Front Office
    public function view_fo()
    {
        return view('modul.transaksi.sewa_ruangan.index_fo');
    }
    
    public function index_fo($id_sewa_ruangan = null)
    {
        if ($id_sewa_ruangan) {
            echo json_encode(SewaRuangan::find($id_sewa_ruangan));
        }else {
            echo json_encode(SewaRuangan::with("tamu","ruangan")->get());
        }
    }

    public function view_check_out_fo()
    {
        return view('modul.transaksi.check_out_ruangan.index_fo');
    }
    
    public function index_check_out_fo($id_sewa_ruangan = null)
    {
        if ($id_sewa_ruangan) {
            echo json_encode(SewaRuangan::find($id_sewa_ruangan));
        }else {
            $today = Carbon::today()->toDateString();
            // $query = SewaKamar::where('tanggal_check_out',$today)->with("tamu","kamar")->get();
            echo json_encode(SewaRuangan::where('tanggal_check_out',$today)->with("tamu","ruangan")->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request)
    {
        $data = SewaRuangan::find($request->id_sewa_ruangan);
        $data->nama_kategori = $request->nama_kategori;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_sewa_ruangan)
    {
        $test = SewaRuangan::find($id_sewa_ruangan)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));

    }
    
    public function print_preview_fo($id_sewa_ruangan)
    {
        $data = SewaRuangan::select('total_biaya')->where('id_sewa_ruangan',$id_sewa_ruangan)->value('total_biaya');
        $today = Carbon::today();
        return view('modul.transaksi.sewa_ruangan.preview_fo',compact("data","today"));
    }
}
