<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TerimaBarang;
use App\Model\PesanBarang;
use App\Model\Satuan;
use App\Model\DataBarang;
use Carbon\Carbon;

class TerimaBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        $pesan = PesanBarang::where('status', 'Pending')->get();
        $satuan = Satuan::all();
        $today = Carbon::today()->toDateString();
        return view('modul.gudang.terima_barang.index', compact('pesan','satuan','today'));
    }
    
    public function index($id_terima_barang = null)
    {
        if ($id_terima_barang) {
            echo json_encode(TerimaBarang::find($id_terima_barang));
        }else {
            echo json_encode(TerimaBarang::with('pesan_barang')->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TerimaBarang
        $data = new TerimaBarang;
        $data->pesan_id = $request->pesan_id;
        $data->jumlah = $request->jumlah;
        $data->harga = $request->harga;
        $data->nama_pengirim = $request->nama_pengirim;
        $data->nama_penerima = $request->nama_penerima;
        $data->total_harga = $request->total_harga;
        $data->keterangan = $request->keterangan;
        $data->tanggal_terima = $request->tanggal_terima;
        $data->cara_bayar = $request->cara_bayar;
        $data->satuan = $request->satuan;
        $data->save();

        //Barangs
        $item = new DataBarang;
        $item->nama_barang = $request->nama_barang;
        $item->jumlah_barang = $request->jumlah;
        $item->satuan_id = $request->satuan;
        $item->harga = $request->harga;
        $item->status = "Normal";
        $item->keterangan = $request->keterangan;
        $item->save();

        // Update status pesanan
        $get_id = $request->pesan_id;
        $order = PesanBarang::find($get_id);
        $order->status = "Complete";
        $order->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = KategoriLayanan::find($request->id_kategori_layanan);
        $data->nama_kategori_layanan = $request->nama_kategori_layanan;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_terima_barang)
    {
        $test = TerimaBarang::find($id_terima_barang)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }
}
