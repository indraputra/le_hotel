<?php

namespace App\Http\Controllers\Project;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\DatabarangImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\DataBarang;
use App\Model\Satuan;


class DataBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        $satuan = Satuan::where('status', 'Aktif')->get();
        return view('modul.gudang.data_barang.index', compact('satuan'));
    }
    
    public function index($id_barang = null)
    {
        if ($id_barang) {
            echo json_encode(DataBarang::find($id_barang));
        }else {
            echo json_encode(DataBarang::with('satuan')->get());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Suplier;
        $data->nama_suplier = $request->nama_suplier;
        $data->alamat_suplier = $request->alamat_suplier;
        $data->status = $request->status;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = DataBarang::find($request->id_barang);
        $data->nama_barang = $request->nama_barang;
        $data->jumlah_barang = $request->jumlah_barang;
        $data->satuan_id = $request->satuan_id;
        $data->harga = $request->harga;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_barang)
    {
        $test = DataBarang::find($id_barang)->delete();
        // dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

     public function import_excel(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_satuan',$nama_file);
 
        // import data
        Excel::import(new DatabarangImport, public_path('/file_satuan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/DataBarang');

    }    
}
