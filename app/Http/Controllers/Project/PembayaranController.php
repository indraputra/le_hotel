<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Pembayaran;
use App\Model\PembayaranList;
use App\Model\Tamu;
use App\Model\Layanan;
use DB;

class PembayaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        view()->share([
            'tamu' => Tamu::get(),
        ]);
        // $all_total_harga = pembayaranList::where('pembayaran_id',11)->sum('total_harga');
        return view('modul.transaksi.pembayaran.index');
    }
    
    public function index($id_pembayaran = null)
    {
        if ($id_pembayaran) {
            echo json_encode(Pembayaran::find($id_pembayaran));
        }else {
            echo json_encode(Pembayaran::with('tamu')->get());
        }
    }

    public function item($id_pembayaran = null)
    {
        $layanan = Layanan::with('kategori_layanan')->get();
        $test = PembayaranList::where('pembayaran_id',$id_pembayaran)->with('kamar','ruangan','layanan')->get();
        $harga_awal = Pembayaran::select('total_harga')->where('id_pembayaran',$id_pembayaran)->value('total_harga');
        return view('modul.transaksi.pembayaran.item',compact("test","id_pembayaran","layanan","id_pembayaran","harga_awal"));
        // if ($id_pembayaran) {
        //     // $test = PembayaranList::where('pembayaran_id',$id_pembayaran)->with('layanan','kamar','ruangan')->get();
        //     $test = PembayaranList::with('layanan','kamar','ruangan')->get();
        //     // echo json_encode(PembayaranList::where('pembayaran_id',$id_pembayaran)->get());
        //     return view('modul.transaksi.pembayaran.item',compact("test","id_pembayaran"));
        // }else {
        //     echo json_encode("gagal bro coba lagi");
        // }
    }

    public function itemList($id_pembayaran = null)
    {
        if ($id_pembayaran) {
            echo json_encode(PembayaranList::where('pembayaran_id',$id_pembayaran)->with('kamar','ruangan','layanan')->get());
        }else {
            echo json_encode("ERROR");
        }
    }

    public function pembayaranList($id_pembayaran = null)
    {
        if ($id_pembayaran) {
            // $pembayaran_list = DB::table("pembayaran_list")
            //     ->where("pembayaran_id",$id_pembayaran)
            //     ->join('kamar', 'pembayaran_list.kamar_id', '=', 'kamar.id_kamar')
            //     ->join('layanan', 'pembayaran_list.layanan_id', '=', 'layanan.id_layanan')
            //     ->join('ruangan', 'pembayaran_list.ruangan_id', '=', 'ruangan.id_ruangan')
            //     ->select('pembayaran_list.*','layanan.nama_layanan','kamar.nama_kamar','ruangan.nama_ruangan')
            //     ->get();
            //     echo json_encode($pembayaran_list);
            //     dd($pembayaran_list);
                $all_total_harga = pembayaranList::where('pembayaran_id',$id_pembayaran)->sum('total_harga');
                echo json_encode(PembayaranList::where("pembayaran_id",$id_pembayaran)->with('layanan','kamar','ruangan')->get());
                echo json_encode($all_total_harga);
        }else {
            echo json_encode("ERROR");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Pembayaran;
        $data->nama_kategori = $request->nama_kategori;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    public function storeLayanan(Request $request)
    {
        $data = new pembayaranList;
        $data->pembayaran_id = $request->pembayaran_id;
        $data->pembelian = $request->pembelian;
        $data->total_harga = $request->total_harga;
        $data->save();

        $id = $request->pembayaran_id;

        $booking = Pembayaran::find($id);
        $booking->total_harga = $request->grand_total;
        $booking->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Pembayaran::find($request->id_pembayaran);
        $data->tamu_id = $request->tamu_id;
        $data->status = $request->status;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_pembayaran)
    {
        $test = Pembayaran::find($id_pembayaran)->delete();
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }

    public function close(Request $request)
    {
        $data = Pembayaran::find($request->id_pembayaran);
        $data->status = "Closed";
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    public function open(Request $request)
    {
        $data = Pembayaran::find($request->id_pembayaran);
        $data->status = "Belum Lunas";
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }
    
    public function preview($id_pembayaran)
    {
        $preview = PembayaranList::where('pembayaran_id',$id_pembayaran)->get();
        $sub_total = PembayaranList::where('pembayaran_id',$id_pembayaran)->sum('total_harga');
        return view("modul.transaksi.pembayaran.preview",compact("preview","sub_total"));
    }







    //Front Office
    public function view_fo()
    {
        view()->share([
            'tamu' => Tamu::get(),
        ]);
        // $all_total_harga = pembayaranList::where('pembayaran_id',11)->sum('total_harga');
        return view('modul.transaksi.pembayaran.index_fo');
    }
    
    public function index_fo($id_pembayaran = null)
    {
        if ($id_pembayaran) {
            echo json_encode(Pembayaran::find($id_pembayaran));
        }else {
            echo json_encode(Pembayaran::with('tamu')->get());
        }
    }

    public function item_fo($id_pembayaran = null)
    {
        $layanan = Layanan::with('kategori_layanan')->get();
        $test = PembayaranList::where('pembayaran_id',$id_pembayaran)->with('kamar','ruangan','layanan')->get();
        $harga_awal = Pembayaran::select('total_harga')->where('id_pembayaran',$id_pembayaran)->value('total_harga');
        return view('modul.transaksi.pembayaran.item_fo',compact("test","id_pembayaran","layanan","id_pembayaran","harga_awal"));
        // if ($id_pembayaran) {
        //     // $test = PembayaranList::where('pembayaran_id',$id_pembayaran)->with('layanan','kamar','ruangan')->get();
        //     $test = PembayaranList::with('layanan','kamar','ruangan')->get();
        //     // echo json_encode(PembayaranList::where('pembayaran_id',$id_pembayaran)->get());
        //     return view('modul.transaksi.pembayaran.item',compact("test","id_pembayaran"));
        // }else {
        //     echo json_encode("gagal bro coba lagi");
        // }
    }

    public function itemList_fo($id_pembayaran = null)
    {
        if ($id_pembayaran) {
            echo json_encode(PembayaranList::where('pembayaran_id',$id_pembayaran)->with('kamar','ruangan','layanan')->get());
        }else {
            echo json_encode("ERROR");
        }
    }

    public function pembayaranList_fo($id_pembayaran = null)
    {
        if ($id_pembayaran) {
            // $pembayaran_list = DB::table("pembayaran_list")
            //     ->where("pembayaran_id",$id_pembayaran)
            //     ->join('kamar', 'pembayaran_list.kamar_id', '=', 'kamar.id_kamar')
            //     ->join('layanan', 'pembayaran_list.layanan_id', '=', 'layanan.id_layanan')
            //     ->join('ruangan', 'pembayaran_list.ruangan_id', '=', 'ruangan.id_ruangan')
            //     ->select('pembayaran_list.*','layanan.nama_layanan','kamar.nama_kamar','ruangan.nama_ruangan')
            //     ->get();
            //     echo json_encode($pembayaran_list);
            //     dd($pembayaran_list);
                $all_total_harga = pembayaranList::where('pembayaran_id',$id_pembayaran)->sum('total_harga');
                echo json_encode(PembayaranList::where("pembayaran_id",$id_pembayaran)->with('layanan','kamar','ruangan')->get());
                echo json_encode($all_total_harga);
        }else {
            echo json_encode("ERROR");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_fo()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_fo(Request $request)
    {
        $data = new Pembayaran;
        $data->nama_kategori = $request->nama_kategori;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    public function storeLayanan_fo(Request $request)
    {
        $data = new pembayaranList;
        $data->pembayaran_id = $request->pembayaran_id;
        $data->pembelian = $request->pembelian;
        $data->total_harga = $request->total_harga;
        $data->save();

        $id = $request->pembayaran_id;

        $booking = Pembayaran::find($id);
        $booking->total_harga = $request->grand_total;
        $booking->save();

        echo json_encode(array('status'=>'Sukses Simpan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_fo($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_fo($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_fo(Request $request)
    {
        $data = Pembayaran::find($request->id_pembayaran);
        $data->tamu_id = $request->tamu_id;
        $data->status = $request->status;
        $data->update();
        echo json_encode(array('status' => 'Sukses Update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_fo($id_pembayaran)
    {
        $test = Pembayaran::find($id_pembayaran)->delete();
        dd($test);
        echo json_encode(array('status'=>'Sukses Menghapus'));
    }
}
