<?php

namespace App\Http\Middleware;

use Closure;

class IsPenulisArticle
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->isPenulisArticle()) {
            return $next($request);
        }
        return redirect('home');
    }
}
