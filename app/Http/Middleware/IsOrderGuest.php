<?php

namespace App\Http\Middleware;

use Closure;

class IsOrderGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->isOrderGuest()) {
            return $next($request);
        }
        return redirect('home');
    }
}
