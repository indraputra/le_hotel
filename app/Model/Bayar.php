<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bayar extends Model
{
    protected $table = 'bayar';
    protected $primaryKey = 'id_bayar';
    protected $fillable = ['pembayaran_id','bayar','keterangan'];
}
