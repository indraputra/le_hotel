<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Satuan;

class DataBarang extends Model
{
    protected $table = 'barang';
    protected $primaryKey = 'id_barang';
    protected $fillable = ['nama_barang','jumlah_barang','satuan_id','harga','keterangan', 'status'];

    public function satuan()
    {
    	return $this->hasMany(Satuan::class,'id_satuan','satuan_id');
    }
}
