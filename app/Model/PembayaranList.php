<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Layanan;
use App\Model\Ruangan;
use App\Model\Kamar;

class PembayaranList extends Model
{
    protected $table = 'pembayaran_list';
    protected $primaryKey = 'id_pembayaran_list';
    protected $fillable = ['pembayaran_id','kamar_id','pembelian','ruangan_id','layanan_id','total_harga','cara_bayar'];

    public function kamar()
    {
    	return $this->hasMany(Kamar::class,'id_kamar','kamar_id');
    }

    public function ruangan()
    {
    	return $this->hasMany(Ruangan::class,'id_ruangan','ruangan_id');
    }

    public function layanan()
    {
    	return $this->hasMany(Layanan::class,'id_layanan','layanan_id');
    }
}
