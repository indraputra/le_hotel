<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    protected $table = 'layanan';
    protected $primaryKey = 'id_layanan';
    protected $fillable = ['kategori_layanan_id','status','keterangan','harga_layanan', 'nama_layanan'];

    public function kategori_layanan()
    {
    	return $this->hasMany(KategoriLayanan::class,'id_kategori_layanan','kategori_layanan_id');
    }
}
