<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    protected $table = 'ruangan';
    protected $primaryKey = 'id_ruangan';
    protected $fillable = ['kategori_id','nama_ruangan','fasilitas_id','status','keterangan','harga_ruangan','foto'];

    public function kategori()
    {
    	return $this->hasMany(kategori_kamar::class,'id_kategori','kategori_id');
    }

    public function fasilitas()
    {
    	return $this->hasMany(fasilitas::class,'id_fasilitas','fasilitas_id');
    }
}
