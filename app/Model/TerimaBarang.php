<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TerimaBarang extends Model
{
    protected $table = 'terima_barang';
    protected $primaryKey = 'id_terima_barang';
    protected $fillable = ['pesan_id','tanggal_terima','cara_bayar','nama_pengirim','nama_penerima','keterangan','harga', 'total_harga','jumlah'];

    public function pesan_barang()
    {
    	return $this->hasMany(PesanBarang::class,'id_pesan','pesan_id');
    }    
}