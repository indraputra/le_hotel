<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KategoriLayanan extends Model
{
    protected $table = 'kategori_layanan';
    protected $primaryKey = 'id_kategori_layanan';
    protected $fillable = ['nama_kategori_layanan','status','keterangan'];
}
