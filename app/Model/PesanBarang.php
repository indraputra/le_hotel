<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PesanBarang extends Model
{
    protected $table = 'pesan_barang';
    protected $primaryKey = 'id_pesan';
    protected $fillable = ['tanggal_pemesanan','nama_barang','satuan_id', 'tipe_id', 'jumlah', 'suplier_id' ,'tanggal_tiba', 'keterangan', 'harga', 'status'];


    public function satuan()
    {
    	return $this->hasMany(Satuan::class,'id_satuan','satuan_id');
    }

    public function suplier()
    {
    	return $this->hasMany(Suplier::class,'id_suplier','suplier_id');
    }

    public function tipe_barang()
    {
        return $this->hasMany(TipeBarang::class,'id_tipe_barang','tipe_id');
    }
}
