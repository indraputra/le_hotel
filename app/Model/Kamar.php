<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kamar extends Model
{
    protected $table = 'kamar';
    protected $primaryKey = 'id_kamar';
    protected $fillable = ['kategori_id','nama_kamar','fasilitas_id','status','keterangan','harga_kamar','foto'];

    public function kategori()
    {
    	return $this->hasMany(kategori_kamar::class,'id_kategori','kategori_id');
    }

    public function fasilitas()
    {
    	return $this->hasMany(fasilitas::class,'id_fasilitas','fasilitas_id');
    }
}
