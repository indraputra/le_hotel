<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Tamu;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';
    protected $primaryKey = 'id_pembayaran';
    protected $fillable = ['tamu_id','status','total_harga','total_bayar'];

    public function tamu()
    {
    	return $this->hasMany(Tamu::class,'id_tamu','tamu_id');
    }
}
