<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tamu extends Model
{
    protected $table = 'tamu';
    protected $primaryKey = 'id_tamu';
    protected $fillable = ['nama_tamu','tipe_tamu','alamat_tamu','tanggal_lahir','no_ktp','agama','pendidikan','no_telp','status'];
}
