<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TipeBarang extends Model
{
    protected $table = 'tipe_barang';
    protected $primaryKey = 'id_tipe_barang';
    protected $fillable = ['nama_tipe','status','keterangan'];
}
