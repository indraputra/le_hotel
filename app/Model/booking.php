<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'booking';
    protected $primaryKey = 'id_booking';
    protected $fillable = ['atas_nama','ruangan_id','kamar_id','tanggal_penempatan','status'];

    public function kamar()
    {
    	return $this->hasMany(Kamar::class,'id_kamar','kamar_id');
    }

    public function ruangan()
    {
    	return $this->hasMany(Ruangan::class,'id_ruangan','ruangan_id');
    }
}
