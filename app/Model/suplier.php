<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Suplier extends Model
{
    protected $table = 'suplier';
    protected $primaryKey = 'id_suplier';
    protected $fillable = ['nama_suplier','no_telp','alamat_suplier','status'];
}
