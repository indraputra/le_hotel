<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Tamu;
use App\Model\Kamar;

class SewaKamar extends Model
{
    protected $table = "sewa_kamar";
    protected $primaryKey = 'id_sewa_kamar';
    protected $fillable = ["tamu_id","kamar_id","jumlah_pengunjung","tanggal_check_in","tanggal_check_out","total_biaya","lamanya","status"];

    public function tamu()
    {
        return $this->hasMany(Tamu::class,'id_tamu','tamu_id');
    }

    public function kamar()
    {
        return $this->hasMany(Kamar::class,'id_kamar','kamar_id');
    }
}
