<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class kategori_kamar extends Model
{
    protected $table = 'kategori';
    protected $primaryKey = 'id_kategori';
    protected $fillable = ['nama_kategori','status','keterangan'];
}
