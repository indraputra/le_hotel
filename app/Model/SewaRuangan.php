<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SewaRuangan extends Model
{
    protected $table = "sewa_ruangan";
    protected $primaryKey = "id_sewa_ruangan";
    protected $fillable = ["tamu_id","ruangan_id","tanggal_check_in","tanggal_check_out","total_biaya","lamanya","status"];

    public function tamu()
    {
        return $this->hasMany(Tamu::class,'id_tamu','tamu_id');
    }

    public function ruangan()
    {
        return $this->hasMany(Ruangan::class,'id_ruangan','ruangan_id');
    }
}
