<?php

namespace App\Imports;

use App\Model\Suplier;
use Maatwebsite\Excel\Concerns\ToModel;

class SuplierImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Suplier([
            'nama_suplier' => $row[1],
            'no_telp' => $row[2],
            'alamat_suplier' => $row[3],
            'status' => $row[4],
        ]);
    }
}
