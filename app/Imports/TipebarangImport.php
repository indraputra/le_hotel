<?php

namespace App\Imports;

use App\Model\TipeBarang;
use Maatwebsite\Excel\Concerns\ToModel;

class TipebarangImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new TipeBarang([
            'nama_tipe' => $row [1],
            'status' => $row [2],
        ]);
    }
}
