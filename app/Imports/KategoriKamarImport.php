<?php

namespace App\Imports;

use App\Model\kategori_kamar;
use Maatwebsite\Excel\Concerns\ToModel;

class KategoriKamarImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new kategori_kamar([
            'nama_kategori'=>$row[1],
            'status'=>$row[2],
        ]);
    }
}
