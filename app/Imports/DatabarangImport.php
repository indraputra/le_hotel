<?php

namespace App\Imports;

use App\Model\Databarang;
use Maatwebsite\Excel\Concerns\ToModel;

class DatabarangImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Databarang([
            'nama_barang' => $row[1],
            'jumlah_barang' => $row[2],
            'satuan_id' => $row[3],
            'harga' => $row[4],
            'status' =>$row[5],
        ]);
    }
}
