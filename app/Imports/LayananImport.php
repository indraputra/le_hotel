<?php

namespace App\Imports;

use App\Model\Layanan;
use Maatwebsite\Excel\Concerns\ToModel;

class LayananImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Layanan([
            'kategori_layanan_id' => $row[1],
            'nama_layanan' =>$row[2],
            'harga_layanan' =>$row[3],
            'status' =>$row[4],
        ]);
    }
}
