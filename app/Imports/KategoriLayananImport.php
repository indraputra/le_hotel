<?php

namespace App\Imports;

use App\Model\KategoriLayanan;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class KategoriLayananImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new KategoriLayanan([
            'nama_kategori_layanan' =>$row[1],
            'status' =>$row[2],
            'keterangan' =>$row[3],
        ]);
    }

    public function headingRow(): int
    {
        return 2;
    }
}
