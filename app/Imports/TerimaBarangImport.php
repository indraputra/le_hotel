<?php

namespace App\Imports;

use App\Model\TerimaBarang;
use Maatwebsite\Excel\Concerns\ToModel;

class TerimaBarangImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new TerimaBarang([
            ''
        ]);
    }
}
