<?php

namespace App\Imports;

use App\Model\PesanBarang;
use Maatwebsite\Excel\Concerns\ToModel;

class PesanBarangImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new PesanBarang([
            'tanggal_pemesanan' => $row[1],
            'nama_barang' =>$row[2],
            'satuan_id'=>$row[3],
            'tipe_id'=>$row[4],
            'jumlah'=>$row[5],
            'suplier_id'=>$row[6],
            'harga'=>$row[7],
            'status'=>$row[8],
        ]);
    }
}
