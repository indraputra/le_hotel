<?php

namespace App\Imports;

use App\Model\Satuan;
use Maatwebsite\Excel\Concerns\ToModel;

class SatuanImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Satuan([
            'nama_satuan' => $row[1],
            'status' => $row[2],
        ]);
    }
}
