<?php

namespace App\Imports;

use App\Model\Fasilitas;
use Maatwebsite\Excel\Concerns\ToModel;

class FasilitasKamarImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Fasilitas([
            'nama_fasilitas'=>$row[1],
            'status'=>$row[2],
        ]);
    }
}
