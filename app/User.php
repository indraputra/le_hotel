<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const PENGURUS_TYPE = 'pengurus';
    const ADMIN_TYPE = 'admin';
    const ORDER_GUEST_TYPE = 'order_guest';
    const PENULIS_ARTICLE_TYPE = 'penulis_article';
    const DEFAULT_TYPE = 'default';

    public function isPengurus(){        
        return $this->type === self::PENGURUS_TYPE;    
    }

    public function isAdmin(){        
        return $this->type === self::ADMIN_TYPE;    
    }

    public function isPenulisArticle(){        
        return $this->type === self::PENULIS_ARTICLE_TYPE;
    }

    public function isOrderGuest(){        
        return $this->type === self::ORDER_GUEST_TYPE;
    }
}
