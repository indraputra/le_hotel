<?php

use Illuminate\Database\Seeder;
use App\User;
class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seeAccount();
    }

    public function seeAccount(){
        factory(User::class)->create([
            'email' => 'admin@hotel.com',
            'password'=>bcrypt('admin'),
            'type' => 'admin'
        ]);
        factory(User::class)->create([
            'email' => 'pengurus@hotel.com',
            'password' =>bcrypt('admin'),
            'type' => 'pengurus'
        ]);
        factory(User::class)->create([
            'email' => 'user@hotel.com',
            'password' =>bcrypt('admin'),
            'type' => 'default'
        ]);
    }
}
