<?php

use Illuminate\Database\Seeder;
use App\Model\Kamar;
use Faker\Factory;

class KamarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->component();
    }

    public function component(){
        $kategori_ruangan = ['A-10'];
        $status = ['Aktif', 'Tidak Aktif'];
        $faker = Factory::create('id_ID');
        

        for ($i=1; $i <= 20; $i++) { 
            for ($u=100000; $u < 500000; $u++) { 
                Kamar::create([
                    'nama_fasilitas'=>$faker->randomElement($kategori_ruangan).$i,
                    'kategori_id' =>$faker->randomElement($i),
                    'fasilitas_id' =>$faker->randomElement($i),
                    'status'=>$faker->randomElement($status),
                    'keterangan'=>"-",
                    'foto'=>"-",
                    'harga-kamar'=>$faker->randomElement($u),
                ]);
            }
        }
    }
}
