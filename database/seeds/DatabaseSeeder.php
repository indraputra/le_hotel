<?php

use Illuminate\Database\Seeder;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AccountSeeder::class);
        // $this->call(KategoriKamar::class);
        // $this->call(FasilitasSeeder::class);
        // $this->call(KamarSeeder::class);
    }
}
