<?php

use Illuminate\Database\Seeder;
use App\Model\kategori_kamar;
use Faker\Factory;

class KategoriKamar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->component();
    }

    public function component(){
        $kategori_ruangan = ['VIP','Ekonomi','Bisnis'];
        $status = ['Aktif', 'Tidak Aktif'];
        $faker = Factory::create('id_ID');

        for ($i=1; $i <= 20; $i++) { 
            kategori_kamar::create([
                'nama_kategori'=>$faker->randomElement($kategori_ruangan).$i,
                'status'=>$faker->randomElement($status),
                'keterangan'=>"-"
            ]);
        }
    }
}
