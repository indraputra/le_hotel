<?php

use Illuminate\Database\Seeder;
use App\Model\Fasilitas;
use Faker\Factory;

class FasilitasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->component();
    }

    public function component(){
        $kategori_ruangan = ['Diamond','Family','Single'];
        $status = ['Aktif', 'Tidak Aktif'];
        $faker = Factory::create('id_ID');

        for ($i=1; $i <= 20; $i++) { 
            Fasilitas::create([
                'nama_fasilitas'=>$faker->randomElement($kategori_ruangan).$i,
                'status'=>$faker->randomElement($status),
                'keterangan'=>"-"
            ]);
        }
    }
}
