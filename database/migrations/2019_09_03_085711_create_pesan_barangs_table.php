<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesanBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesan_barang', function (Blueprint $table) {
            $table->bigIncrements('id_pesan');
            $table->date('tanggal_pemesanan');
            $table->string('nama_barang');
            $table->bigInteger('satuan_id')->unsigned();
            $table->bigInteger('tipe_id')->unsigned();
            $table->bigInteger('jumlah');
            $table->bigInteger('suplier_id')->unsigned();
            $table->date('tanggal_tiba')->nullable();
            $table->string('keterangan')->nullable();
            $table->bigInteger('harga');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesan_barang');
    }
}
