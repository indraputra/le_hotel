<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSewaKamarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sewa_kamar', function (Blueprint $table) {
            $table->bigIncrements('id_sewa_kamar');
            $table->bigInteger('tamu_id')->unsigned();
            $table->bigInteger('kamar_id')->unsigned();
            $table->bigInteger('jumlah_pengunjung');
            $table->date('tanggal_check_in');
            $table->date('tanggal_check_out')->nullable();
            $table->bigInteger('total_biaya');
            $table->bigInteger('lamanya');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sewa_kamar');
    }
}
