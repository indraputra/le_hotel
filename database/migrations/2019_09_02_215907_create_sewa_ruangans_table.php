<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSewaRuangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sewa_ruangan', function (Blueprint $table) {
            $table->bigIncrements('id_sewa_ruangan');
            $table->bigInteger('tamu_id')->unsigned();
            $table->bigInteger('ruangan_id')->unsigned();
            $table->date('tanggal');
            $table->date('tanggal_check_in');
            $table->date('tanggal_check_out');
            $table->bigInteger('total_biaya');
            $table->bigInteger('lamanya');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sewa_ruangan');
    }
}
