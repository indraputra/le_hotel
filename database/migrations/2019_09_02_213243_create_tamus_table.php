<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTamusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tamu', function (Blueprint $table) {
            $table->bigIncrements('id_tamu');
            $table->string('nama_tamu');
            $table->string('tipe_tamu');
            $table->string('alamat_tamu');
            $table->date('tanggal_lahir');
            $table->string('tempat_lahir');
            $table->bigInteger('no_ktp');
            $table->string('agama');
            $table->string('pendidikan');
            $table->bigInteger('no_telp');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tamu');
    }
}
