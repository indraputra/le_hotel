<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('petugas', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        // Schema::table('ruangan', function (Blueprint $table) {
        //     $table->foreign('kategori_id')->references('id_kategori')->on('kategori')->onDelete('cascade')->onUpdate('cascade');
        //     $table->foreign('fasilitas_id')->references('id_fasilitas')->on('fasilitas')->onDelete('cascade')->onUpdate('cascade');
        // });

        Schema::table('kamar', function (Blueprint $table) {
            $table->foreign('kategori_id')->references('id_kategori')->on('kategori')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fasilitas_id')->references('id_fasilitas')->on('fasilitas')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('ruangan', function (Blueprint $table) {
            $table->foreign('kategori_id')->references('id_kategori')->on('kategori')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fasilitas_id')->references('id_fasilitas')->on('fasilitas')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('pembayaran', function (Blueprint $table) {
            $table->foreign('tamu_id')->references('id_tamu')->on('tamu')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('pembayaran_list', function (Blueprint $table) {
            $table->foreign('pembayaran_id')->references('id_pembayaran')->on('pembayaran')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kamar_id')->references('id_kamar')->on('kamar')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ruangan_id')->references('id_ruangan')->on('ruangan')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('layanan_id')->references('id_layanan')->on('layanan')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('booking', function (Blueprint $table) {
            $table->foreign('kamar_id')->references('id_kamar')->on('kamar')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ruangan_id')->references('id_ruangan')->on('ruangan')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('sewa_kamar', function (Blueprint $table) {
            $table->foreign('tamu_id')->references('id_tamu')->on('tamu')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kamar_id')->references('id_kamar')->on('kamar')->onDelete('cascade')->onUpdate('cascade');
        });

        // Schema::table('sewa_ruangan', function (Blueprint $table) {
        //     $table->foreign('tamu_id')->references('id_tamu')->on('tamu')->onDelete('cascade')->onUpdate('cascade');
        //     $table->foreign('ruangan_id')->references('id_ruangan')->on('ruangan')->onDelete('cascade')->onUpdate('cascade');
        // });

        Schema::table('sewa_ruangan', function (Blueprint $table) {
            $table->foreign('tamu_id')->references('id_tamu')->on('tamu')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ruangan_id')->references('id_ruangan')->on('ruangan')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('pesan_barang', function (Blueprint $table) {
            $table->foreign('suplier_id')->references('id_suplier')->on('suplier')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('terima_barang', function (Blueprint $table) {
            $table->foreign('pesan_id')->references('id_pesan')->on('pesan_barang')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('barang', function (Blueprint $table) {
            $table->foreign('satuan_id')->references('id_satuan')->on('satuan')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('bayar', function (Blueprint $table) {
            $table->foreign('pembayaran_id')->references('id_pembayaran')->on('pembayaran')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi');
    }
}
