<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran_list', function (Blueprint $table) {
            $table->bigIncrements('id_pembayaran_list');
            $table->bigInteger('pembayaran_id')->unsigned()->nullable();
            $table->bigInteger('kamar_id')->unsigned()->nullable();
            $table->bigInteger('ruangan_id')->unsigned()->nullable();
            $table->bigInteger('layanan_id')->unsigned()->nullable();
            $table->string('pembelian')->nullable();
            $table->bigInteger('total_harga');
            $table->string('cara_bayar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran_list');
    }
}
