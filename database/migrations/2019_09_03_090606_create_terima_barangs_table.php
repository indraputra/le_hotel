<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerimaBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terima_barang', function (Blueprint $table) {
            $table->bigIncrements('id_terima_barang');
            $table->bigInteger('pesan_id')->unsigned();
            $table->date('tanggal_terima');
            $table->string('cara_bayar');
            $table->string('satuan');
            $table->string('nama_pengirim');
            $table->string('nama_penerima');
            $table->string('keterangan')->nullable();
            $table->bigInteger('harga');
            $table->string('jumlah');
            $table->bigInteger('total_harga');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terima_barangs');
    }
}
