@extends('layouts.template') @section('title') home @stop @section('content')
<div class="card background-header">
    <div class="card-body">
        <div id="date" class="waktu-tanggal"></div>
        <div id="time" class="waktu-tanggal"></div>
    </div>
</div>

<div class="product-sales-area mg-tb-30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-sales-chart">
                    <div>
                        <h3>Home Page {{ Auth::user()->type }}</h3>
                    </div>
                    <div>
                        Selamat datang {{ Auth::user()->name }} semoga harimu ceria selalu 
                    </div>
                    <div>
                        <h6>Motto hotel/perusahaan</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm">
            <div class="card mx-auto" style="width: 18rem;background-color: #78a1e3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-7">
                            <p class="text-light" style="font-size: 50px">{{$kamar_bersih}}</p>
                        </div>
                        <div class="col-3">
                            <h1 class="text-light"><i class="fas fa-bed fa-2x"></i></h1>
                        </div>
                    </div>
                    <h5 class="card-title text-light" style="margin-top: -30px">Kamar Tersedia</h5>
                </div>
            </div>
        </div>
        <div class="col-sm">
                <div class="card mx-auto bg-success" style="width: 18rem;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-7">
                                <p class="text-light" style="font-size: 50px">{{$kamar_terpakai}}</p>
                            </div>
                            <div class="col-3">
                                <h1 class="text-light"><i class="fas fa-bed fa-2x"></i></h1>
                            </div>
                        </div>
                        <h5 class="card-title text-light" style="margin-top: -30px">Kamar Terpakai</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                    <div class="card mx-auto bg-danger" style="width: 18rem;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-7">
                                    <p class="text-light" style="font-size: 50px">{{$kamar_kotor}}</p>
                                </div>
                                <div class="col-3">
                                    <h1 class="text-light"><i class="fas fa-bed fa-2x"></i></h1>
                                </div>
                            </div>
                            <h5 class="card-title text-light" style="margin-top: -30px">Kamar Kotor</h5>
                        </div>
                    </div>
                </div>
    </div>
</div>

<div class="product-sales-area mg-tb-30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-sales-chart">
                    <canvas id="canvas" height="280" width="600"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/js/bootstrap-select.min.js" charset="utf-8"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script> -->
<script src="{{asset('js/Chart.js')}}"></script>

<script>
    var url = "{{route('pendapatan.perbulan')}}";
    var Years = new Array();
    var Labels = new Array();
    var Prices = new Array();
    $(document).ready(function(){
      $.get(url, function(response){
        response.forEach(function(data){
            Years.push(data.id_pembayaran);
            Labels.push(data.tamu_id);
            Prices.push(data.total_harga);
        });
        var ctx = document.getElementById("canvas").getContext('2d');
            var myChart = new Chart(ctx, {
              type: 'line',
              data: {
                    labels:[1,2,3,4,5],
                    datasets: [{
                        label: 'Pemasukan Hotel',
                        borderColor: "#80b6f4",
                        pointBorderColor: "#80b6f4",
                        pointBackgroundColor: "#80b6f4",
                        pointHoverBackgroundColor: "#80b6f4",
                        pointHoverBorderColor: "#80b6f4",
                        pointBorderWidth: 10,
                        pointHoverRadius: 10,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: false,
                        data: Prices,
                    }]
              },
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero:true
                          }
                      }]
                  }
              }
          });
      });
    });
</script>
@endsection