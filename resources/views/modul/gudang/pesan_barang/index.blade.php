@extends('layouts.template')

@section('title')
 Pesan Barang
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Pesan Barang</div>
                    <div class="space-button">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#importExcel" @click="addBtn()">
                           <i class="fas fa-file-excel"></i> Import Excel
                        </button>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal" @click="addBtn()">
                           <i class="fas fa-cart-plus"></i> Memesan Barang
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Harga Barang</th>
                            <th>Jumlah Barang dipesan</th>
                            <th>Status</th>
                            <th width="110">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas">
                            <td>@{{++index}}</td>
                            <td>@{{data.nama_barang}}</td>
                            <td>@{{data.satuan[0].nama_satuan}}</td>
                            <td>@{{data.harga}}</td>
                            <td>@{{data.jumlah}}</td>
                            <td>@{{data.status}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_pesan)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-edit"></i></button>
                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_pesan)"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="post" action="/pesan_barang/import_excel" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                        </div>
                        <div class="modal-body">

                            {{ csrf_field() }}

                            <label>Pilih file excel</label>
                            <div class="form-group">
                                <input type="file" name="file" required="required">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
<div class="modal fade bd-example-modal-lg" id="tambahKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Memesan Barang</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label for="">Tanggal Pemesanan</label>
                  <input type="date" id="tanggal_pemesanan" class="form-control" name="tanggal_pemesanan" v-model="tanggal_pemesanan" required>
                </div>
                <div class="form-group">
                  <label for="">Nama Barang</label>
                  <input type="input" id="nama_barang" class="form-control" name="nama_barang" v-model="nama_barang" required>
                </div>
                <div class="form-group">
                    <label for="list">Nama Toko</label>
                        <select name="suplier_id" id="list" v-model="suplier_id" class="form-control">
                            <option readonly value="">Pilih Nama Suplier</option>
                            @foreach ($suplier as $data)
                            <option value="{{$data->id_suplier}}"> {{ $data->nama_suplier }} </option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group">
                    <label for="list">Tipe Barang</label>
                        <select name="tipe_id" id="list" v-model="tipe_id" class="form-control">
                            <option readonly value="">Pilih Tipe Barang</option>
                            @foreach ($tipe as $data)
                            <option value="{{$data->id_tipe_barang}}"> {{ $data->nama_tipe }} </option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group">
                  <label for="">Jumlah</label>
                  <input type="input" class="form-control" name="jumlah" v-model="jumlah" required>
                </div>
                <div class="form-group">
                    <label for="list">Satuan</label>
                        <select name="satuan_id" id="list" v-model="satuan_id" class="form-control">
                            <option readonly value="">Pilih Satuan</option>
                            @foreach ($satuan as $data)
                            <option value="{{$data->id_satuan}}"> {{ $data->nama_satuan }} </option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group">
                  <label for="">Harga Barang</label>
                  <input type="input" class="form-control" name="harga" v-model="harga" required>
                </div>
                <div class="form-group">
                  <input type="hidden" class="form-control" value="Pending" name="status" v-model="status" required>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal" @click="addData()" >Submit</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Pesan Barang</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                                <label for="">Nama Barang</label>
                                <input type="input" class="form-control" name="nama_fasilitas" v-model="nama_fasilitas">
                              </div>
                              <div class="form-group">
                                <label for="">Status</label>
                                <input type="input" class="form-control" name="status" v-model="status">
                              </div>
                              <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="input" class="form-control" name="keterangan" v-model="keterangan">
                                <input type="hidden" class="form-control" name="id_fasilitas" v-model="id_fasilitas" required>
                              </div>
                    <button class="btn btn-primary" data-dismiss="modal" @click="updateData()">Submit</button>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true
        });
    });

    // window.onload = function () {
        var app = new Vue({
            el:'#app',
            data:{
                id_pesan:"",
                tipe_id:"",
                suplier_id:"",
                nama_barang:"",
                satuan_id:"",
                nama_satuan:"",
                harga:"",
                satuan:"",
                jumlah:"",
                status:"",
                tanggal_pemesanan:"",
                tanggal_tiba:"",
                datas:[],
            },
            mounted(){
                this.getData();
            },

            methods:{
                getData(){
                    var self=this;
                    axios.get("{{route('pesan_barang.data')}}").then(function(response){
                        self.datas = response.data;
                    });
                },

                addData(){
                    var self=this;
                    var url="{{route('pesan_barang.add')}}";
                    
                    var formData = new FormData();
                    formData.append("nama_barang", self.nama_barang);
                    formData.append("satuan_id", self.satuan_id);
                    formData.append("tipe_id", self.tipe_id);
                    formData.append("suplier_id", self.suplier_id);
                    formData.append("harga", self.harga);
                    formData.append("jumlah", self.jumlah);
                    formData.append("status", self.status);
                    formData.append("tanggal_pemesanan", self.tanggal_pemesanan);
                    if(self.nama_barang == null){
                        swal("Error", "Nama Barang Tidak Boleh Kosong", "error");
                    }else if(self.satuan_id == null){
                        swal("Error", "Satuan harus diisi", "error");
                    }else if(self.tipe_id == null){
                        swal("Error", "Tipe harus diisi", "error");
                    }else if(self.suplier_id == null){
                        swal("Error", "Suplier harus diisi", "error");
                    }else if(self.harga == null){
                        swal("Error", "Harga harus diisi", "error");
                    }else if(self.jumlah == null){
                        swal("Error", "Jumlah harus diisi", "error");
                    }else if(self.tanggal_pemesanan == null){
                        swal("Error", "Tanggal harus diisi", "error");
                    }
                    else{
                        axios.post(url, formData).then(function(response){
                            self.getData();
                            $('#tambahKategoriModal').modal('hide');
                            swal("Success", "Data berhasil disimpan", "success");
                        }).catch(function(err){
                            swal("Error", "Koneksi ke Databse Gagal", "error");
                        })
                    }
                },

                deleteData(id_pesan){
                    var self=this;
                    var url = "{{route('pesan_barang.delete')}}"+"/"+id_pesan;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Success", "Data Sukses dihapus", "success");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                searchData(id_pesan){
                    var url = "{{route('pesan_barang.data')}}" + "/" + id_pesan;
                    var self=this;
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function (response){
                        self.nama_barang = response.data.nama_barang;
                        self.satuan_id = response.data.satuan_id;
                        self.harga = response.data.harga;
                        self.jumlah = response.data.jumlah;
                        self.status = response.data.status;
                        self.tanggal_pemesanan = response.data.tanggal_pemesanan;
                        self.id_pesan = response.data.id_pesan;
                    })
                    console.log(id_pesan);
                },

                addBtn(){
                    var url = "{{route('pesan_barang.data')}}";
                    var self=this;
                    axios.get(url).then(function (response){
                        self.nama_barang = null;
                        self.satuan_id = null;
                        self.harga = null;
                        self.jumlah = null;
                        self.status = null;
                        self.tanggal_pemesanan = null;
                    })
                },

                updateData(){
                    var self = this;
                    var url = "{{route('pesan_barang.update')}}";
                    console.log("save");

                    var formData = new FormData();
                    formData.append("nama_barang", self.nama_barang);
                    formData.append("satuan_id", self.satuan_id);
                    formData.append("tipe_id", self.tipe_id
                        );
                    formData.append("harga", self.harga);
                    formData.append("jumlah", self.jumlah);
                    formData.append("status", self.status);
                    formData.append("tanggal_pemesanan", self.tanggal_pemesanan);
                    formData.append("id_pesan", self.id_pesan);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Success", "Data berhasil diupdate", "success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                }
            }
        })
    
    </script>

@endsection