@extends('layouts.template')

@section('title')
 Terima Barang
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Terima Barang</div>
                    <div class="space-button">
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal" @click="addBtn()">
                           <i class="fas fa-cart-plus"></i> Barang Datang
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Nama Barang</th>
                            <th>Tanggal Diterima</th>
                            <th>Nama Pengirim</th>
                            <th>Nama Penerima</th>
                            <th width="110">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas">
                            <td>@{{++index}}</td>
                            <td>@{{data.pesan_barang[0].nama_barang}}</td>
                            <td>@{{data.tanggal_terima}}</td>
                            <td>@{{data.nama_pengirim}}</td>
                            <td>@{{data.nama_penerima}}</td>
                            <td>
                                <!-- <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_terima_barang)" data-toggle="modal" data-target="#editKategoriModal">Edit</button> -->
                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_terima_barang)"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<div class="modal fade bd-example-modal-lg" id="tambahKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Barang Datang</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label for="">Tanggal Terima</label>
                  <input type="date" id="tanggal_terima" class="form-control" name="tanggal_terima" required>
                </div>
                <div class="form-group">
                    <label for="list">Nama Barang</label>
                        <select name="pesan_id" id="list" class="form-control" onchange="tampil();">
                            <option  value="">Pilih Nama Barang</option>
                            @foreach ($pesan as $data)
                            <option value="{{$data->id_pesan}}" data-satuan="{{$data->satuan_id}}" data-jumlah="{{$data->jumlah}}" data-harga="{{$data->harga}}" data-nama="{{$data->nama_barang}}" readonly> {{ $data->nama_barang }} </option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group">
                    <label for="list">Satuan Barang</label>
                        <select name="satuan" id="satuan" class="form-control" readonly>
                            @foreach ($satuan as $data)
                            <option value="{{$data->id_satuan}}"> {{$data->nama_satuan}} </option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group">
                    <label for="list">Jumlah Barang</label>
                    <input type="input" readonly name="jumlah" id="jumlah" class="form-control">
                    <input type="input" readonly name="nama_barang" id="nama_barang" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Harga Barang</label>
                    <div class="input-group mb-3">
                      <input type="input" readonly class="form-control" id="harga" name="harga" required>
                      <div class="input-group-append">
                        <button class="btn btn-primary" type="button" id="cek_harga" onclick="cekHarga();">Cek Harga</button>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label for="">Total Harga Barang</label>
                  <input type="input" class="form-control" id="total_harga" name="total_harga" required>
                </div>
                <div class="form-group">
                  <label for="">Nama Pengirim</label>
                  <input type="input" class="form-control" name="nama_pengirim" id="nama_pengirim" required>
                </div>
                <div class="form-group">
                  <label for="">Nama Penerima</label>
                  <input type="input" class="form-control" name="nama_penerima" id="nama_penerima" required>
                </div>
                <div class="form-group">
                    <label for="list">Cara Pembayaran</label>
                        <select name="cara_bayar" id="cara_bayar" class="form-control">
                            <option readonly value="">Pilih Cara Pembayaran</option>
                            <option value="Bayar Ditempat">Bayar Ditempat</option>
                        </select>
                </div>
                <div class="form-group">
                  <label for="">Keterangan</label>
                  <input type="input" class="form-control" name="keterangan" id="keterangan" required>
                </div>
                <div class="form-group">
                  <input type="hidden" class="form-control" value="Pending" name="status" required>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal" @click="addData()" >Submit</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Pesan Barang</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                                <label for="">Nama Barang</label>
                                <input type="input" class="form-control" name="nama_fasilitas" v-model="nama_fasilitas">
                              </div>
                              <div class="form-group">
                                <label for="">Status</label>
                                <input type="input" class="form-control" name="status" v-model="status">
                              </div>
                              <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="input" class="form-control" name="keterangan" v-model="keterangan">
                                <input type="hidden" class="form-control" name="id_fasilitas" v-model="id_fasilitas" required>
                              </div>
                    <button class="btn btn-primary" data-dismiss="modal" @click="updateData()">Submit</button>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true
        });
    });

    function tampil() {
        var list = document.getElementById('list');
        document.getElementById("satuan").value = list.options[list.selectedIndex].getAttribute('data-satuan');
        document.getElementById("harga").value = list.options[list.selectedIndex].getAttribute('data-harga');
        document.getElementById("jumlah").value = list.options[list.selectedIndex].getAttribute('data-jumlah');
        document.getElementById("nama_barang").value = list.options[list.selectedIndex].getAttribute('data-nama');
        console.log(list.options[list.selectedIndex].getAttribute('data-nama'));
        console.log(list.options[list.selectedIndex].getAttribute('data-jumlah'));
    }
    function totalan(){
        var Harga = document.getElementById('harga').value;
        var Jumlah = document.getElementById('jumlah').value;
        var hitung = Harga * Jumlah;
        document.getElementById('total_harga').value = hitung;
        // document.getElementById('total2').value = hitung;
    }

    // window.onload = function () {
        var app = new Vue({
            el:'#app',
            data:{
                id_terima_barang:"",
                pesan_id:"",
                jumlah:"",
                harga:"",
                total_harga:"",
                nama_pengirim:"",
                nama_penerima:"",
                keterangan:"",
                status:"",
                tanggal_terima:"",
                cara_bayar:"",
                datas:[],
            },
            mounted(){
                this.getData();
            },

            methods:{
                // getData(){
                //     var self=this;
                //     axios.get("{{route('terima_barang.data')}}").then(function(response){
                //         self.datas = response.data;
                //         console.log(self.datas);
                //     }).catch(function(err){
                //         swal("Error","Koneksi Kedatabase Gagal","error");
                //     })
                // },

                getData(){
                    var self=this;
                    axios.get("{{route('terima_barang.data')}}").then(function(response){
                        self.datas = response.data;
                        console.log(self.datas);
                    }).catch(function(err){
                        swal("Error","Koneksi Kedatabase Gagal","error");
                    })
                },

                addData(){
                    var self=this;
                    var url="{{route('terima_barang.add')}}";

                    var test = document.getElementById("jumlah").value;
                    var txtTanggalTerima = document.getElementById("tanggal_terima").value;
                    var txtNamaBarang = document.getElementById("list").value;
                    var txtSatuan = document.getElementById("satuan").value;
                    var txtJumlah = document.getElementById("jumlah").value;
                    var txtHarga = document.getElementById("harga").value;
                    var txtTotalHarga = document.getElementById("total_harga").value;
                    var txtPengirim = document.getElementById("nama_pengirim").value;
                    var txtPenerima = document.getElementById("nama_penerima").value;
                    var txtCaraBayar = document.getElementById("cara_bayar").value;
                    var txtKeterangan = document.getElementById("keterangan").value;
                    var txtBarang = document.getElementById("nama_barang").value;

                    var formData = new FormData();
                    formData.append("pesan_id", txtNamaBarang);
                    formData.append('tanggal_terima', txtTanggalTerima);
                    formData.append('cara_bayar', txtCaraBayar);
                    formData.append('satuan', txtSatuan);
                    formData.append('nama_pengirim', txtPengirim);
                    formData.append('nama_penerima', txtPenerima);
                    formData.append('keterangan', txtKeterangan);
                    formData.append("harga", txtHarga);
                    formData.append("jumlah", test);
                    formData.append('total_harga', txtTotalHarga);
                    formData.append('nama_barang', txtBarang);


                    axios.post(url, formData).then(function(response){
                        self.getData();
                        swal("Success", "Data Berhasil Disimpan", "success");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal. Segera hubungi admin", "")
                    })
                },

                maintance(){
                    swal("Error","Masih dalam pengembangan","error");
                },

                deleteData(id_terima_barang){
                    var self=this;
                    var url = "{{route('terima_barang.delete')}}"+"/"+id_terima_barang;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Success", "Data berhasil dihapus", "success");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                searchData(id_terima_barang){
                    var url = "{{route('terima_barang.data')}}" + "/" + id_terima_barang;
                    var self=this;
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function (response){
                        self.pesan_id = response.data.pesan_id;
                        self.jumlah = response.data.jumlah;
                        self.harga = response.data.harga;
                        self.total_harga = response.data.total_harga;
                        self.nama_pengirim = response.data.nama_pengirim;
                        self.nama_penerima = response.data.nama_penerima;
                        self.keterangan = response.data.keterangan;
                        self.status = response.data.status;
                        self.tanggal_terima = response.data.tanggal_terima;
                        seld.cara_bayar = response.data.cara_bayar;
                        self.id_terima_barang = response.data.id_terima_barang;
                    })
                    console.log(id_terima_barang);
                },

                addBtn(){
                    
                },

                updateData(){
                    var self = this;
                    var url = "{{route('pesan_barang.update')}}";
                    console.log("save");

                    var formData = new FormData();
                    formData.append("nama_barang", self.nama_barang);
                    formData.append("satuan_id", self.satuan_id);
                    formData.append("tipe_id", self.tipe_id);
                    formData.append("harga", self.harga);
                    formData.append("jumlah", self.jumlah);
                    formData.append("status", self.status);
                    formData.append("tanggal_pemesanan", self.tanggal_pemesanan);
                    formData.append("id_pesan", self.id_pesan);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Success", "Data berhasil diupdate", "success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke database gagal", "error");
                    })
                }
            }
        })
    
    function cekHarga(){
        var Harga = document.getElementById('harga').value;
        var Jumlah = document.getElementById('jumlah').value;
        var hitung = Harga * Jumlah;
        document.getElementById('total_harga').value = hitung;
    }

</script>

@endsection