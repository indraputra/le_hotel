@extends('layouts.template') @section('title') Tamu @endsection @section('content')

<div id="app">
    <div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Data Tamu</div>
                    <div class="space-button hidden">
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal" @click="addBtn()">
                           <i class="fas fa-plus"></i> Tambah Tamu
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Nama Kategori</th>
                            <th>Tipe Tamu</th>
                            <th>No Ktp</th>
                            <th>Status</th>
                            <th width="170">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas" v-if="data.status == 'In House'" style="background-color: #a5eda4">
                            <td>@{{++index}}</td>
                            <td>@{{data.nama_tamu}}</td>
                            <td>@{{data.tipe_tamu}}</td>
                            <td>@{{data.no_ktp}}</td>
                            <td>@{{data.status}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_tamu)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-outline-info btn-sm" @click="searchData(data.id_tamu)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-eye"></i></button>
                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_tamu)"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>

                        <tr v-for="(data,index) in datas" v-if="data.status == 'Return'" style="background-color: #eda4a4">
                                <td>@{{++index}}</td>
                                <td>@{{data.nama_tamu}}</td>
                                <td>@{{data.tipe_tamu}}</td>
                                <td>@{{data.no_ktp}}</td>
                                <td>@{{data.status}}</td>
                                <td>
                                    <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_tamu)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-edit"></i></button>
                                    <button class="btn btn-outline-info btn-sm" @click="searchData(data.id_tamu)" data-toggle="modal" data-target="#showKategoriModal"><i class="fas fa-eye"></i></button>
                                    <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_tamu)"><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Tamu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nama Tamu</label>
                        <input type="hidden" class="form-control" name="id_tamu" v-model="id_tamu" required>
                        <input type="input" class="form-control" name="nama_kategori" v-model="nama_tamu">
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" id="list" v-model="status" class="form-control">
                                    <option value="In House">In House</option>
                                    <option value="Return">Return</option>
                                </select>
                    </div>
                    <div class="form-group">
                        <label for="">Tipe Tamu</label>
                        <input type="input" class="form-control" name="keterangan" v-model="tipe_tamu">
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <input type="input" class="form-control" name="keterangan" v-model="alamat_tamu">
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="keterangan" v-model="tanggal_lahir">
                    </div>
                    <div class="form-group">
                        <label for="">No Ktp</label>
                        <input type="input" class="form-control" name="keterangan" v-model="no_ktp">
                    </div>
                    <div class="form-group">
                        <label for="">Agama</label>
                        <input type="input" class="form-control" name="keterangan" v-model="agama">
                    </div>
                    <div class="form-group">
                        <label for="">Pendidikan</label>
                        <input type="input" class="form-control" name="keterangan" v-model="pendidikan">
                    </div>
                    <div class="form-group">
                        <label for="">No Telepon</label>
                        <input type="input" class="form-control" name="keterangan" v-model="no_telp">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" @click="updateData()">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="showKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Tamu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nama Tamu</label>
                        <input type="hidden" class="form-control" name="id_tamu" v-model="id_tamu" required>
                        <input type="input" class="form-control" name="nama_kategori" v-model="nama_tamu">
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" id="list" v-model="status" class="form-control">
                                    <option value="In House">In House</option>
                                    <option value="Return">Return</option>
                                </select>
                    </div>
                    <div class="form-group">
                        <label for="">Tipe Tamu</label>
                        <input type="input" class="form-control" name="keterangan" v-model="tipe_tamu">
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <input type="input" class="form-control" name="keterangan" v-model="alamat_tamu">
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="keterangan" v-model="tanggal_lahir">
                    </div>
                    <div class="form-group">
                        <label for="">No Ktp</label>
                        <input type="input" class="form-control" name="keterangan" v-model="no_ktp">
                    </div>
                    <div class="form-group">
                        <label for="">Agama</label>
                        <input type="input" class="form-control" name="keterangan" v-model="agama">
                    </div>
                    <div class="form-group">
                        <label for="">Pendidikan</label>
                        <input type="input" class="form-control" name="keterangan" v-model="pendidikan">
                    </div>
                    <div class="form-group">
                        <label for="">No Telepon</label>
                        <input type="input" class="form-control" name="keterangan" v-model="no_telp">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{--
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src="http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true
        });
    });

    // window.onload = function () {
    var app = new Vue({
        el: '#app',
        data: {
            id_tamu: "",
            nama_tamu: "",
            tipe_tamu: "",
            alamat_tamu: "",
            tempat_lahir: "",
            tanggal_lahir: "",
            no_ktp: "",
            pendidikan: "",
            agama: "",
            status: "",
            no_telp: "",
            datas: [],
        },
        mounted() {
            this.getData();
        },

        methods: {
            getData() {
                var self = this;
                axios.get("{{route('tamu.data')}}").then(function(response) {
                    self.datas = response.data;
                });
            },

            deleteData(id_tamu) {
                var self = this;
                var url = "{{route('tamu.delete')}}" + "/" + id_tamu;

                var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                if (!konfirm) {
                    return false;
                }
                axios.get(url).then(function(resposnse) {
                    // alert(response.data.status);
                    self.getData();
                    alert("data sukses dihapus");
                }).catch(function(err) {
                    alert("koneksi kedatabase gagal");
                })
            },

            searchData(id_tamu) {
                var url = "{{route('tamu.data')}}" + "/" + id_tamu;
                var self = this;
                var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                if (!konfirm) {
                    return false;
                }
                axios.get(url).then(function(response) {
                    self.nama_tamu = response.data.nama_tamu;
                    self.status = response.data.status;
                    self.tipe_tamu = response.data.tipe_tamu;
                    self.alamat_tamu = response.data.alamat_tamu;
                    self.tanggal_lahir = response.data.tanggal_lahir;
                    self.tempat_lahir = response.data.tempat_lahir;
                    self.no_ktp = response.data.no_ktp;
                    self.agama = response.data.agama;
                    self.pendidikan = response.data.pendidikan;
                    self.no_telp = response.data.no_telp;
                    self.id_tamu = response.data.id_tamu;
                })
                console.log(id_tamu);
            },

            updateData() {
                var self = this;
                var url = "{{route('tamu.update')}}";
                console.log("save");

                var formData = new FormData();
                formData.append("nama_tamu", self.nama_tamu);
                formData.append("status", self.status);
                formData.append("tipe_tamu", self.tipe_tamu);
                formData.append("alamat_tamu", self.alamat_tamu);
                formData.append("tanggal_lahir", self.tanggal_lahir);
                formData.append("tempat_lahir", self.tempat_lahir);
                formData.append("no_ktp", self.no_ktp);
                formData.append("agama", self.agama);
                formData.append("pendidikan", self.pendidikan);
                formData.append("no_telp", self.no_telp);
                formData.append("id_tamu", self.id_tamu);

                axios.post(url, formData).then(function(response) {
                    // alert(response.data.status);
                    swal("Sukses", "Data berhasil diupdate", "success");
                    self.getData();
                }).catch(function(err) {
                    swal("Error", "Koneksi ke Database Gagal", "error");
                })
            }
        }
    })
</script>

@endsection