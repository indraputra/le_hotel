@extends('layouts.template')

@section('title')
 Fasilitas
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Data Fasilitas</div>
                    <div class="space-button">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#importExcel" @click="addBtn()">
                          <i class="fas fa-file-excel"></i> Import Excel
                        </button>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal" @click="addBtn()">
                          <i class="fas fa-plus"></i> Tambah Fasilitas
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Nama Fasilitas</th>
                            <th>Status</th>
                            <th>Keterangan</th>
                            <th width="110">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas">
                            <td>@{{++index}}</td>
                            <td>@{{data.nama_fasilitas}}</td>
                            <td>@{{data.status}}</td>
                            <td>@{{data.keterangan}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_fasilitas)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-edit"></i></button>
                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_fasilitas)"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="post" action="/fasilitas/import_excel" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                        </div>
                        <div class="modal-body">

                            {{ csrf_field() }}

                            <label>Pilih file excel</label>
                            <div class="form-group">
                                <input type="file" name="file" required="required">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

<div class="modal fade bd-example-modal-lg" id="tambahKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Fasilitas</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label for="">Nama Fasilitas</label>
                  <input type="input" id="nama_fasilitas" class="form-control" name="nama_fasilitas" v-model="nama_fasilitas" required>
                </div>
                <div class="form-group">
                  <label for="">Status</label>
                  {{-- <input type="input" class="form-control" name="status" v-model="status" required> --}}
                    <select name="status" id="list" v-model="status" class="form-control">
                        <option value="Aktif">Aktif</option>
                        <option value="Tidak aktif">Tidak aktif</option>
                    </select>
                </div>
                <div class="form-group">
                  <label for="">Keterangan</label>
                  <input type="input" class="form-control" name="keterangan" v-model="keterangan" required>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal" @click="addData()" ><i class="fas fa-save"></i> Submit</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Fasilitas</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                                <label for="">Nama Fasilitas</label>
                                <input type="input" class="form-control" name="nama_fasilitas" v-model="nama_fasilitas">
                              </div>
                              <div class="form-group">
                                <label for="">Status</label>
                                {{-- <input type="input" class="form-control" name="status" v-model="status" required> --}}
                                  <select name="status" id="list" v-model="status" class="form-control">
                                    <option value="Aktif">Aktif</option>
                                    <option value="Tidak aktif">Tidak aktif</option>
                                  </select>
                              </div>
                              <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="input" class="form-control" name="keterangan" v-model="keterangan">
                                <input type="hidden" class="form-control" name="id_fasilitas" v-model="id_fasilitas" required>
                              </div>
                            </div>
                            <div class="modal-footer">
                            <button class="btn btn-primary" data-dismiss="modal" @click="updateData()"><i class="fas fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true
        });
    });

    // window.onload = function () {
        var app = new Vue({
            el:'#app',
            data:{
                id_fasilitas:"",
                nama_fasilitas:"",
                status:"",
                keterangan:"",
                datas:[],
            },
            mounted(){
                this.getData();
            },

            methods:{
                getData(){
                    var self=this;
                    axios.get("{{route('fasilitas.data')}}").then(function(response){
                        self.datas = response.data;
                    });
                },

                addData(){
                    var self=this;
                    var url="{{route('fasilitas.add')}}";
                    
                    var formData = new FormData();
                    formData.append("nama_fasilitas", self.nama_fasilitas);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                    if(self.nama_fasilitas == null){
                        swal("Error", "Nama Fasilitas Tidak Boleh Kosong", "error");
                    }
                    else if(self.status == null){
                        swal("Error", "Status tidak boleh kosong", "error");
                    }
                    else{
                        axios.post(url, formData).then(function(response){
                            self.getData();
                            swal("Sukses", "Data berhasil disimpan", "success");
                        }).catch(function(err){
                            swal("Error", "Koneksi ke Database Gagal", "error");
                        })
                    }
                },

                deleteData(id_fasilitas){
                    var self=this;
                    var url = "{{route('fasilitas.delete')}}"+"/"+id_fasilitas;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Sukses", "Data berhasil dihapus", "success");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                searchData(id_fasilitas){
                    var url = "{{route('fasilitas.data')}}" + "/" + id_fasilitas;
                    var self=this;
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function (response){
                        self.nama_fasilitas = response.data.nama_fasilitas;
                        self.status = response.data.status;
                        self.keterangan = response.data.keterangan;
                        self.id_fasilitas = response.data.id_fasilitas;
                    })
                    console.log(id_fasilitas);
                },

                addBtn(){
                    var url = "{{route('fasilitas.data')}}";
                    var self=this;
                    axios.get(url).then(function (response){
                        self.nama_fasilitas = null;
                        self.status = null;
                        self.keterangan = null;
                        self.id_fasilitas = null;
                    })
                },

                updateData(){
                    var self = this;
                    var url = "{{route('fasilitas.update')}}";
                    console.log("save");

                    var formData = new FormData();
                    formData.append("nama_fasilitas", self.nama_fasilitas);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                    formData.append("id_fasilitas", self.id_fasilitas);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Sukses", "Data berhasil diupdate", "success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                }
            }
        })
    
    </script>

@endsection