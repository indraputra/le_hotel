@extends('layouts.template')

@section('title')
 Detail Kamar
@endsection

@section('content')
<div class="container-fluid header-pages ">
<div class="card shadow" >
  <div class="card-header">
    Detail Kamar
  </div>
  <div class="card-body">
	<form action="{{route("kamar_fo.add")}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="">Nama Kamar</label>
                                    <input type="input" id="nama_kategori" class="form-control" name="nama_kamar" v-model="nama_kamar" value="{{$data->nama_kamar}}" readonly="">
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                <label for="list">Kategori</label>
                                    <select name="kategori_id" id="list" v-model="kategori_id" class="form-control" readonly>
                                        <option value="{{$data->kategori_id}}">{{$data->kategori[0]->nama_kategori}}</option> 
                                        @foreach ($kategori as $datas)
                                        <option value="{{$datas->id_kategori}}"> {{ $datas->nama_kategori }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="list">Fasilitas</label>
                                        <select name="fasilitas_id" id="list" v-model="fasilitas_id" class="form-control" readonly>
                                            <option value="{{$data->fasilitas_id}}">{{$data->fasilitas[0]->nama_fasilitas}}</option> 
                                            @foreach ($fasilitas as $datas)
                                            <option value="{{$datas->id_fasilitas}}"> {{ $datas->nama_fasilitas }} </option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
        
                        <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="">Harga</label>
                                        <input type="number" class="form-control" name="harga_kamar" value="{{$data->harga_kamar}}" readonly="" required>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select name="status" id="list" readonly v-model="status" class="form-control">
                                            <option value="{{$data->status}}">{{$data->status}}</option>
                                            <option value="Clean">Clean</option>
                                            <option value="In House">In House</option>
                                            <option value="Dirty">Dirty</option>
                                        </select>
                                    </div>
                                </div>
                                
                        </div>
                            
                        <label for="">Foto</label>
                        <br>
                        <img src="{{asset('data_file/'.$data->foto)}}" alt="">
                        
                        <div class="form-group">
                          <label for="">Keterangan</label>
                          <input type="input" readonly="" class="form-control" name="keterangan" value="{{$data->keterangan}}" required>
                        </div>
                    </form>    
  </div>
</div>
</div>
@stop