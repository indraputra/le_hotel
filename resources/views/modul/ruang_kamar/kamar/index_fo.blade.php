@extends('layouts.template')

@section('title')
 Kamar
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Data Kamar</div>
                    <div class="space-button">
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal" @click="addBtn()">
                           <i class="fas fa-plus"></i> Tambah Kamar
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Nama Kamar</th>
                            <th>Kategori</th>
                            <th>Fasilitas</th>
                            <th>Harga</th>
                            <th>Status</th>
                            <th width="150">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas" v-if="data.status == 'Clean'">
                            <td>@{{++index}}</td>
                            <td>@{{data.nama_kamar}}</td>
                            <td>@{{data.kategori[0].nama_kategori}}</td>
                            <td>@{{data.fasilitas[0].nama_fasilitas}}</td>
                            <td>@{{data.harga_kamar | currency}}</td>
                            <td>@{{data.status}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_kamar)"><i class="fas fa-edit"></i></button>

                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_kamar)"><i class="fas fa-trash"></i></button>

                                <button id="detail" onclick="disable();" class="btn btn-outline-info btn-sm" @click="detailData(data.id_kamar)" data-target=""><i class="fas fa-eye"></i></button>

                            </td>
                        </tr>

                        <tr v-for="(data,index) in datas" v-if="data.status == 'In House'" style="background-color: #a5eda4">
                            <td>@{{++index}}</td>
                            <td>@{{data.nama_kamar}}</td>
                            <td>@{{data.kategori[0].nama_kategori}}</td>
                            <td>@{{data.fasilitas[0].nama_fasilitas}}</td>
                            <td>@{{data.harga_kamar | currency}}</td>
                            <td>@{{data.status}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_kamar)"><i class="fas fa-edit"></i></button>

                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_kamar)"><i class="fas fa-trash"></i></button>

                                <button id="detail" onclick="disable();" class="btn btn-outline-info btn-sm" @click="detailData(data.id_kamar)" data-target=""><i class="fas fa-eye"></i></button>

                            </td>
                        </tr>

                        <tr v-for="(data,index) in datas" v-if="data.status == 'Dirty'" style="background-color: #eda4a4">
                            <td>@{{++index}}</td>
                            <td>@{{data.nama_kamar}}</td>
                            <td>@{{data.kategori[0].nama_kategori}}</td>
                            <td>@{{data.fasilitas[0].nama_fasilitas}}</td>
                            <td>@{{data.harga_kamar | currency}}</td>
                            <td>@{{data.status}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_kamar)"><i class="fas fa-edit"></i></button>

                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_kamar)"><i class="fas fa-trash"></i></button>

                                <button id="detail" onclick="disable();" class="btn btn-outline-info btn-sm" @click="detailData(data.id_kamar)" data-target=""><i class="fas fa-eye"></i></button>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<div class="modal fade bd-example-modal-xl" id="tambahKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Kamar</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

            <form action="{{route("kamar_fo.add")}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="">Nama Kamar</label>
                            <input type="input" id="nama_kategori" class="form-control" name="nama_kamar" v-model="nama_kamar" required>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                        <label for="list">Kategori</label>
                            <select name="kategori_id" id="list" v-model="kategori_id" class="form-control">
                                <option value=""></option>
                                @foreach ($kategori as $data)
                                <option value="{{$data->id_kategori}}"> {{ $data->nama_kategori }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="list">Fasilitas</label>
                                <select name="fasilitas_id" id="list" v-model="fasilitas_id" class="form-control">
                                    <option value=""></option>
                                    @foreach ($fasilitas as $data)
                                    <option value="{{$data->id_fasilitas}}"> {{ $data->nama_fasilitas }} </option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                        <div class="col-sm">
                            <div class="form-group">
                                <label for="">Harga</label>
                                <input type="number" class="form-control" name="harga_kamar" v-model="harga_kamar" required>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" id="list" v-model="status" class="form-control">
                                    <option value="Clean">Clean</option>
                                    <option value="In House">In House</option>
                                    <option value="Dirty">Dirty</option>
                                </select>
                            </div>
                        </div>
                        
                </div>
                    
                <label for="">Foto</label>
                <div class="custom-file">
                    <input type="file" name="foto" class="custom-file-input" id="foto" v-on="foto" ref="fileInput">
                    <label class="custom-file-label" for="foto">Choose file</label>
                </div>
                
                <div class="form-group">
                  <label for="">Keterangan</label>
                  <input type="input" class="form-control" name="keterangan" v-model="keterangan" required>
                </div>
                <button id="submit" class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                        <form action="{{route("kamar_fo.add")}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="">Nama Kamar</label>
                                        <input type="input" id="nama_kategori" class="form-control" name="nama_kamar" v-model="nama_kamar" required>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="form-group">
                                    <label for="list">Kategori</label>
                                        <select name="kategori_id" id="list" v-model="kategori_id" class="form-control">
                                            <option value=""></option>
                                            @foreach ($kategori as $data)
                                            <option value="{{$data->id_kategori}}"> {{ $data->nama_kategori }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="list">Fasilitas</label>
                                            <select name="fasilitas_id" id="list" v-model="fasilitas_id" class="form-control">
                                                <option value=""></option>
                                                @foreach ($fasilitas as $data)
                                                <option value="{{$data->id_fasilitas}}"> {{ $data->nama_fasilitas }} </option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>
            
                            <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">Harga</label>
                                            <input type="number" class="form-control" name="harga_kamar" v-model="harga_kamar" required>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">Status</label>
                                            <select name="status" id="list" v-model="status" class="form-control">
                                                <option value="Aktif">Aktif</option>
                                                <option value="Tidak aktif">Tidak aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                            </div>
                                
                            <label for="">Foto</label>
                            <div class="custom-file">
                                <input type="file" name="foto" class="custom-file-input" id="foto" v-on="foto" ref="fileInput">
                                <label class="custom-file-label" for="foto">Choose file</label>
                            </div>
                            
                            <div class="form-group">
                              <label for="">Keterangan</label>
                              <input type="input" class="form-control" name="keterangan" v-model="keterangan" required>
                            </div>
                            <button id="simpan" class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal">Submit</button>
                        </form>
                              
                    {{-- <button class="btn btn-primary" data-dismiss="modal" @click="updateData()">Submit</button> --}}
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
</div>
<div class="modal fade bd-example-modal-lg" id="detailKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    <form action="{{route("kamar_fo.add")}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="">Nama Kamar</label>
                                    <input type="input" id="nama_kategori" class="form-control" name="nama_kamar" v-model="nama_kamar" required>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                <label for="list">Kategori</label>
                                    <select name="kategori_id" id="list" v-model="kategori_id" class="form-control">
                                        <option value=""></option>
                                        @foreach ($kategori as $data)
                                        <option value="{{$data->id_kategori}}"> {{ $data->nama_kategori }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="list">Fasilitas</label>
                                        <select name="fasilitas_id" id="list" v-model="fasilitas_id" class="form-control">
                                            <option value=""></option>
                                            @foreach ($fasilitas as $data)
                                            <option value="{{$data->id_fasilitas}}"> {{ $data->nama_fasilitas }} </option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
        
                        <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="">Harga</label>
                                        <input type="number" class="form-control" name="harga_kamar" v-model="harga_kamar" required>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select name="status" id="list" v-model="status" class="form-control">
                                            <option value="Aktif">Aktif</option>
                                            <option value="Tidak aktif">Tidak aktif</option>
                                        </select>
                                    </div>
                                </div>
                                
                        </div>
                            
                        <label for="">Foto</label>
                        <div class="custom-file">
                            <input type="file" name="foto" class="custom-file-input" id="foto" v-on="foto" ref="fileInput">
                            <label class="custom-file-label" for="foto">Choose file</label>
                        </div>
                        
                        <div class="form-group">
                          <label for="">Keterangan</label>
                          <input type="input" class="form-control" name="keterangan" v-model="keterangan" required>
                        </div>
                        <button class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal">Submit</button>
                    </form>
                          
                {{-- <button class="btn btn-primary" data-dismiss="modal" @click="updateData()">Submit</button> --}}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>

<script type="text/javascript">
    var test = {!! json_encode($test) !!};
    console.log(test);

    

    $(document).ready(function(){
        $('#table').DataTable({
            "processing": true,
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true,
            "lengthChange": true,
            "oLanguage": {
                "sZeroRecords": "",
                "sEmptyTable": ""
            }
        });
    });

    // window.onload = function () {
        Vue.filter('currency', function (value) {
            // symbol : 'Rp.',
            // thousandsSeparator: '.',
            // fractionCount: 0,
            // fractionSeparator: ',',
            // symbolPosition: 'front',
            // symbolSpacing: true
            return accounting.formatMoney(value, "Rp ", 2, ".", ",")
            // return '€' + parseFloat(value).toFixed(2);
        });
        var app = new Vue({
            el:'#app',
            data:{
                id_kamar:"",
                nama_kamar:"",
                fasilitas_id:"",
                kategori_id:"",
                nama_kategori:"",
                harga_kamar:"",
                kategori:"",
                fasilitas:"",
                foto:"",
                status:"",
                keterangan:"",
                datas:[],
            },
            mounted(){
                this.getData();
            },

            methods:{
                avatarChange(e) {
                    var fileReader = new FileReader()
                    fileReader.readAsDataURL(e.target.files[0])
                    fileReader.onload = (e) => {
                    this.data.foto = e.target.datas
                    }
                    console.log(this.data)
                },
                getData(){
                    var self=this;
                    axios.get("{{route('kamar_fo.data')}}").then(function(response){
                        self.datas = response.data;
                    }).catch(function(err){
                        alert("koneksi kedatabase gagal");
                    })
                },

                addData(){
                    var self=this;
                    var url="{{route('kamar_fo.add')}}";
                    
                    var formData = new FormData();
                    formData.append("nama_kamar", self.nama_kamar);
                    formData.append("kategori_id", self.kategori_id);
                    formData.append("fasilitas_id", self.fasilitas_id);
                    formData.append("harga_kamar", self.harga_kamar);
                    formData.append("foto", self.foto);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                        axios.post(url, formData).then(function(response){
                            self.getData();
                        }).catch(function(err){
                            alert("koneksi kedatabase gagal");
                        })
                    
                },

                deleteData(id_kamar){
                    var self=this;
                    var url = "{{route('kamar_fo.delete')}}"+"/"+id_kamar;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Sukses", "Data berhasil dihapus", "success");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                searchData(id_kamar){
                    window.location.href = "kamar_fo/edit/"+id_kamar;
                },

                detailData(id_kamar){
                    console.log("detail data pressed");
                    console.log(id_kamar);
                    window.location.href = "kamar_fo/detail/"+id_kamar;
                    // this.$route.router.go('/home');
                },

                addBtn(){
                    var url = "{{route('kamar_fo.data')}}";
                    var self=this;
                    axios.get(url).then(function (response){
                        self.nama_kamar = null;
                        self.foto = null;
                        self.kategori_id = null;
                        self.fasilitas_id = null;
                        self.harga_kamar = null;
                        self.status = null;
                        self.keterangan = null;
                        self.id_kamar = null;
                    })
                },

                updateData(){
                    var self = this;
                    
                    console.log("save");

                    var formData = new FormData();
                    formData.append("nama_kamar", self.nama_kamar);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                    formData.append("id_kamar", self.id_kamar);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        alert("Data sukses diupdate");
                        self.getData();
                    }).catch(function(err){
                        alert("Koneksi ke database gagal");
                    })
                }
            }
        })
        
        $('#foto').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                //replace the "Choose a file" label
                var cleanFileName = fileName.replace('C:\\fakepath\\', "");
                $(this).next('.custom-file-label').html(cleanFileName);
            })

        function disable(){
            document.getElementById("simpan").disabled = true;
        }
    </script>

@endsection