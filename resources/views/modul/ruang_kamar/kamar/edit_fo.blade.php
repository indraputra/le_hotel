@extends('layouts.template')

@section('title')
 Edit Kamar
@endsection

@section('content')
<div class="container-fluid header-pages ">
<div class="card shadow" >
  <div class="card-header">
    Edit Kamar
  </div>
  <div class="card-body">
	<form action="{{route('kamar_fo.update',$data->id_kamar)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="">Nama Kamar</label>
                                    <input type="input" id="nama_kategori" class="form-control" name="nama_kamar" v-model="nama_kamar" value="{{$data->nama_kamar}}" required>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                <label for="list">Kategori</label>
                                    <select name="kategori_id" id="list" v-model="kategori_id" class="form-control">
                                        <option value="{{$data->kategori_id}}">{{$data->kategori[0]->nama_kategori}}</option> 
                                        @foreach ($kategori as $datas)
                                        <option value="{{$datas->id_kategori}}"> {{ $datas->nama_kategori }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="list">Fasilitas</label>
                                        <select name="fasilitas_id" id="list" v-model="fasilitas_id" class="form-control">
                                            <option value="{{$data->fasilitas_id}}">{{$data->fasilitas[0]->nama_fasilitas}}</option> 
                                            @foreach ($fasilitas as $datas)
                                            <option value="{{$datas->id_fasilitas}}"> {{ $datas->nama_fasilitas }} </option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
        
                        <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="">Harga</label>
                                        <input type="number" class="form-control" name="harga_kamar" value="{{$data->harga_kamar}}" required>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select name="status" id="list" v-model="status" class="form-control">
                                            <option value="{{$data->status}}">{{$data->status}}</option>
                                            <option value="Clean">Clean</option>
                                            <option value="In House">In House</option>
                                            <option value="Dirty">Dirty</option>
                                        </select>
                                    </div>
                                </div>
                                
                        </div>
                            
                        <label for="">Foto</label>
                        <div class="custom-file">
                            <input type="file" name="foto" class="custom-file-input" id="foto" v-on="foto" value="{{$data->foto}}" ref="fileInput" required="">
                            <label class="custom-file-label" for="foto">Choose file</label>
                        </div>
                        <br>
                        <img src="{{asset('data_file/'.$data->foto)}}" alt="">
                        
                        <div class="form-group">
                          <label for="">Keterangan</label>
                          <input type="input" class="form-control" name="keterangan" value="{{$data->keterangan}}" required>
                        </div>
                        <div class="row">
                            <div class="col-10"></div>
                            <div class="col-2"><button type="submit" class="btn btn-primary">Save Changes</button></div>
                        </div>
                    </form>    
  </div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script>
    $('#foto').on('change',function(){
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            var cleanFileName = fileName.replace('C:\\fakepath\\', "");
            $(this).next('.custom-file-label').html(cleanFileName);
        })
</script>
@stop