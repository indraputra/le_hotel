@extends('layouts.template')

@section('title')
 Ruangan
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Data Ruangan</div>
                    <div class="space-button">
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal">
                           <i class="fas fa-plus"></i> Tambah Ruangan
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Room Name</th>
                            <th>Category</th>
                            <th>Fasilitas</th>
                            <th>Price</th>
                            <th>Room Status</th>
                            <th width="150">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas">
                            <td>@{{++index}}</td>
                            <td>@{{data.nama_ruangan}}</td>
                            <td>@{{data.kategori[0].nama_kategori}}</td>
                            <td>@{{data.fasilitas[0].nama_fasilitas}}</td>
                            <td>@{{data.harga_ruangan}}</td>
                            <td>@{{data.status}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_ruangan)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-edit"></i></button>

                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_ruangan)"><i class="fas fa-trash"></i></button>

                                <button id="detail" onclick="disable();" class="btn btn-outline-info btn-sm" @click="detailData(data.id_ruangan)" data-target=""><i class="fas fa-eye"></i></button>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<div class="modal fade bd-example-modal-xl" id="tambahKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Ruangan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

            <form action="{{route("ruangan_fo.add")}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="">Room Name</label>
                            <input type="input" id="nama_kategori" class="form-control" name="nama_ruangan" v-model="nama_ruangan" required>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                        <label for="list">Kategori</label>
                            <select name="kategori_id" id="list" v-model="kategori_id" class="form-control">
                                <option value=""></option>
                                @foreach ($kategori as $data)
                                <option value="{{$data->id_kategori}}"> {{ $data->nama_kategori }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="list">Fasilitas</label>
                                <select name="fasilitas_id" id="list" v-model="fasilitas_id" class="form-control">
                                    <option value=""></option>
                                    @foreach ($fasilitas as $data)
                                    <option value="{{$data->id_fasilitas}}"> {{ $data->nama_fasilitas }} </option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                        <div class="col-sm">
                            <div class="form-group">
                                <label for="">Price</label>
                                <input type="number" class="form-control" name="harga_ruangan" v-model="harga_ruangan" required>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="form-group">
                                <label for="">Room Status</label>
                                <select name="status" id="list" v-model="status" class="form-control">
                                    <option value="Clean">Clean</option>
                                    <option value="In House">In House</option>
                                    <option value="Dirty">Dirty</option>
                                </select>
                            </div>
                        </div>
                        
                </div>
                    
                <label for="">Foto</label>
                <div class="custom-file">
                    <input type="file" name="foto" class="custom-file-input" id="foto" v-on="foto" ref="fileInput">
                    <label class="custom-file-label" for="foto">Choose file</label>
                </div>
                
                <div class="form-group">
                  <label for="">Keterangan</label>
                  <input type="input" class="form-control" name="keterangan" v-model="keterangan" required>
                </div>
                <button id="submit" class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal">Submit</button>
            </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal" @click="addData()" >Submit</button>
            </div>
          </div>
        </div>
      </div>
      
</div>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable({
            "processing": true,
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true,
            "lengthChange": true,
            "oLanguage": {
                "sZeroRecords": "",
                "sEmptyTable": ""
            }
        });
    });

        function disable(){
            document.getElementById("simpan").disabled = true;
        }

        var app = new Vue({
            el:'#app',
            data:{
                id_ruangan:"",
                nama_ruangan:"",
                fasilitas_id:"",
                kategori_id:"",
                nama_kategori:"",
                harga_ruangan:"",
                kategori:"",
                fasilitas:"",
                foto:"",
                status:"",
                keterangan:"",
                datas:[],
            },
            mounted(){
                this.getData();
            },

            methods:{
                getData(){
                    var self=this;
                    axios.get("{{route('ruangan_og.data')}}").then(function(response){
                        self.datas = response.data;
                    }).catch(function(err){
                        alert("koneksi kedatabase gagal");
                    })
                },

                searchData(id_ruangan){
                    window.location.href = "ruangan_og/edit/"+id_ruangan;
                },

                detailData(id_ruangan){
                    console.log("detail data pressed");
                    console.log(id_ruangan);
                    window.location.href = "ruangan_og/detail/"+id_ruangan;
                    // this.$route.router.go('/home');
                },

                deleteData(id_ruangan){
                    var self=this;
                    var url = "{{route('ruangan_og.delete')}}"+"/"+id_ruangan;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Sukses", "Data berhasil dihapus", "success");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },
            }
        });

        $('#foto').on('change',function(){
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            var cleanFileName = fileName.replace('C:\\fakepath\\', "");
            $(this).next('.custom-file-label').html(cleanFileName);
        });
    </script>

@endsection