@extends('layouts.template')

@section('title')
 Ruangan Edit
@endsection

@section('content')
<div class="container-fluid header-pages">

    <div class="card">
        <div class="card-header">
            Ruagan Edit
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm">
                    <form action="{{route('ruangan_fo.update',$data->id_ruangan)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method("put")
                    <div class="form-group">
                        <label for="">Room Name</label>
                        <input type="input" id="nama_kategori" class="form-control" value="{{$data->nama_ruangan}}" name="nama_ruangan" v-model="nama_ruangan" >
                    </div>
                </div>
                <div class="col-sm">
                    <div class="form-group">
                        <label for="list">Kategori</label>
                        <select name="kategori_id" id="list" v-model="kategori_id" class="form-control" >
                            <option value="{{$data->kategori_id}}">{{$data->kategori[0]->nama_kategori}}</option>
                            @foreach ($kategori as $datas)
                            <option value="{{$datas->id_kategori}}">{{$datas->nama_kategori}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="form-group">
                        <label for="list">Fasilitas</label>
                        <select name="fasilitas_id" id="list" v-model="fasilitas_id" class="form-control" >
                            <option value="{{$data->fasilitas_id}}">{{$data->fasilitas[0]->nama_fasilitas}}</option>
                            @foreach ($fasilitas as $datas)
                            <option value="{{$datas->id_fasilitas}}"> {{ $datas->nama_fasilitas }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm">
                    <div class="form-group">
                        <label for="">Price</label>
                        <input type="number" class="form-control" name="harga_ruangan" value="{{$data->harga_ruangan}}"  required>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="form-group">
                        <label for="">Room Status</label>
                        <select name="status" id="list" v-model="status" class="form-control" >
                        	<option value="{{$data->status}}">{{$data->status}}</option>
                            <option value="Clean">Clean</option>
                            <option value="In House">In House</option>
                            <option value="Dirty">Dirty</option>
                        </select>
                    </div>
                </div>

            </div>
            <label for="">Foto</label>
            <div class="custom-file">
                <input type="file" name="foto" class="custom-file-input" id="foto" v-on="foto" ref="fileInput">
                <label class="custom-file-label" for="foto">Choose file</label>
            </div>
            <img src="{{asset('data_file/'.$data->foto)}}" alt="">
            <button type="submit" class="btn btn-primary">Save Changes</button>
            </form>
        </div>
    </div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script>
    $('#foto').on('change',function(){
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            var cleanFileName = fileName.replace('C:\\fakepath\\', "");
            $(this).next('.custom-file-label').html(cleanFileName);
        })
</script>
@stop