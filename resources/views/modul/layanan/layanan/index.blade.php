@extends('layouts.template')

@section('title')
 Layanan
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Menu Layanan</div>
                    <div class="space-button">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#importExcel" @click="addBtn()">
                           <i class="fas fa-file-excel"></i> Import Excel
                        </button>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal" @click="addBtn()">
                           <i class="fas fa-plus"></i> Tambah Layanan
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Kategori Layanan</th>
                            <th>Nama Layanan</th>
                            <th>Harga Layanan</th>
                            <th>Status Layanan</th>
                            <th>Keterangan</th>
                            <th width="110">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas">
                            <td>@{{++index}}</td>
                            <td>@{{data.kategori_layanan[0].nama_kategori_layanan}}</td>
                            <td>@{{data.nama_layanan}}</td>
                            <td>@{{data.harga_layanan | currency}}</td>
                            <td>@{{data.status}}</td>
                            <td>@{{data.keterangan}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_layanan)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-edit"></i></button>
                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_layanan)"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="post" action="/layanan/import_excel" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                        </div>
                        <div class="modal-body">

                            {{ csrf_field() }}

                            <label>Pilih file excel</label>
                            <div class="form-group">
                                <input type="file" name="file" required="required">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

<div class="modal fade bd-example-modal-lg" id="tambahKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="list">Kategori Layanan</label>
                        <select name="kategori_layanan_id" id="list" v-model="kategori_layanan_id" class="form-control">
                            <option readonly value="">Pilih Kategori Layanan</option>
                            @foreach ($kategori as $data)
                            <option value="{{$data->id_kategori_layanan}}"> {{ $data->nama_kategori_layanan }} </option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group">
                  <label for="">Nama Layanan</label>
                  <input type="input" class="form-control" name="nama_layanan" v-model="nama_layanan" required>
                </div>
                <div class="form-group">
                  <label for="">Harga Layanan</label>
                  <input type="input" class="form-control" name="harga_layanan" v-model="harga_layanan" required>
                </div>
                <div class="form-group">
                  <label for="">Status Layanan</label>
                  <select name="status" v-model="status" required="" class="form-control">
                  <option>Aktif</option>
                  <option>Tidak Aktif</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Keterangan</label>
                  <input type="input" class="form-control" name="keterangan" v-model="keterangan" required>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal" @click="addData()" data-dismiss="modal" >Submit</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                            <div class="form-group">
                                <label for="list">Kategori Layanan</label>
                                <select name="kategori_layanan_id" id="list" v-model="kategori_layanan_id" class="form-control">
                                <option readonly value="">Pilih Kategori Layanan</option>
                                    @foreach ($kategori as $data)
                                <option value="{{$data->id_kategori_layanan}}"> {{ $data->nama_kategori_layanan }} </option>
                                     @endforeach
                                 </select>
                            </div>
                              <div class="form-group">
                                <label for="">Nama Layanan</label>
                                <input type="input" class="form-control" name="nama_layanan" v-model="nama_layanan">
                              </div>
                              <div class="form-group">
                                <label for="">Harga Layanan</label>
                                <input type="input" class="form-control" name="harga_layanan" v-model="harga_layanan">
                              </div>
                              <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" v-model="status" required="" class="form-control">
                                <option>Aktif</option>
                                <option>Tidak Aktif</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="input" class="form-control" name="keterangan" v-model="keterangan">
                                <input type="hidden" class="form-control" name="id_layanan" v-model="id_layanan">
                              </div>
                    <button class="btn btn-primary" data-dismiss="modal" @click="updateData()">Submit</button>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true
        });
    });
    Vue.filter('currency', function (value) {
            // symbol : 'Rp.',
            // thousandsSeparator: '.',
            // fractionCount: 0,
            // fractionSeparator: ',',
            // symbolPosition: 'front',
            // symbolSpacing: true
            return accounting.formatMoney(value, "Rp ", 2, ".", ",")
            // return '€' + parseFloat(value).toFixed(2);
        });
    // window.onload = function () {
        var app = new Vue({
            el:'#app',
            data:{
                id_layanan:"",
                kategori_layanan_id:"",
                nama_layanan:"",
                harga_layanan:"",
                status:"",
                keterangan:"",
                datas:[],
            },
            mounted(){
                this.getData();
            },

            methods:{
                getData(){
                    var self=this;
                    axios.get("{{route('layanan.data')}}").then(function(response){
                        self.datas = response.data;
                    });
                },

                addData(){
                    var self=this;
                    var url="{{route('layanan.add')}}";
                    
                    var formData = new FormData();
                    formData.append("kategori_layanan_id", self.kategori_layanan_id);
                    formData.append("nama_layanan", self.nama_layanan);
                    formData.append("harga_layanan", self.harga_layanan);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                    if(self.kategori_layanan_id == null){
                        swal("Error", "Kategori layanan tidak boleh kosong", "error");
                    }else if(self.nama_layanan == null){
                        swal("Error", "Nama Layanan tidak boleh kosong", "error");
                    }else if(self.harga_layanan == null){
                        swal("Error", "Harga Layanan tidak boleh kosong", "error");
                    }else if(self.status == null){
                        swal("Error", "Status tidak boleh kosong", "error");
                    }
                    else{
                        axios.post(url, formData).then(function(response){
                            self.getData();
                            swal("Success", "Data berhasil disimpan", "success");
                        }).catch(function(err){
                            swal("Error", "Koneksi ke database gagal", "error");
                        })
                    }
                },

                deleteData(id_layanan){
                    var self=this;
                    var url = "{{route('layanan.delete')}}"+"/"+id_layanan;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Success", "Data berhasil dihapus", "success");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke database gagal", "error");
                    })
                },

                searchData(id_layanan){
                    var url = "{{route('layanan.data')}}" + "/" + id_layanan;
                    var self=this;
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function (response){
                        self.kategori_layanan_id = response.data.kategori_layanan_id;
                        self.nama_layanan = response.data.nama_layanan;
                        self.harga_layanan = response.data.harga_layanan;
                        self.status = response.data.status;
                        self.keterangan = response.data.keterangan;
                        self.id_layanan = response.data.id_layanan;
                    })
                    console.log(id_layanan);
                },

                addBtn(){
                    var url = "{{route('layanan.data')}}";
                    var self=this;
                    axios.get(url).then(function (response){
                        self.kategori_layanan_id = null;
                        self.nama_layanan = null;
                        self.harga_layanan = null;
                        self.status = null;
                        self.keterangan = null;
                        self.id_layanan = null;
                    })
                },

                updateData(){
                    var self = this;
                    var url = "{{route('layanan.update')}}";
                    console.log("save");

                    var formData = new FormData();
                    formData.append("kategori_layanan_id", self.kategori_layanan_id);
                    formData.append("nama_layanan", self.nama_layanan);
                    formData.append("harga_layanan", self.harga_layanan);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                    formData.append("id_layanan", self.id_layanan);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Success", "Data berhasil diupdate" ,"success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke database gagal", "error");
                    })
                }
            }
        })
    
    </script>
@endsection