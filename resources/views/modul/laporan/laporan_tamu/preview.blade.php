<!DOCTYPE html>
<html>

<head>
    <title>Print Laporan Data Tamu</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('original/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap-grid.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fontawesome/css/all.css')}}">
    <style type="text/css" media="screen">
        .judul .title {
            font-size: 20px;
            font-weight: bold;
        }
        
        .tables {
            padding-top: 30px;
        }
    </style>
</head>

<body>
    <div class="judul">

        <div class="title" style="text-align: center; color: black; font-size: 32px; font-style: bold;">
            SMESA EDOTEL
        </div>
        <div class="title" style="text-align: center; color: red; font-size: 24px;">
            Laporan Tamu
        </div>


        <table>
            <tr>
                <th>Periode : </th>
                <th style="font-style: italic;">{{$input1}} s/d {{$input2}}</th>
            </tr>
        </table>
        <div>Total : {{$count}} data</div>
        <!-- <div>Periode : {{$today}}</div> -->

        
    </div>
    <div class="container">
    <div class="tables" style="padding-top: 30px;">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Nama Tamu</th>
                    <th scope="col">Alamat Tamu</th>
                    <th scope="col">Tipe Tamu </th>
                    <th scope="col">No. KTP</th>
                    <th scope="col">No. Telpon</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kamar as $no=> $data)
                <tr>
                    <td>{{$no+1}}</td>
                    <td>{{$data->nama_tamu}}</td>
                    <td>{{$data->alamat_tamu}}</td>
                    <td>{{$data->tipe_tamu}}</td>
                    <td>{{$data->no_ktp}}</td>
                    <td>{{$data->no_telp}}</td>
                    <td>{{$data->status}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    </div>
    <script type="text/javascript">
        window.print();
    </script>

</body>

</html>