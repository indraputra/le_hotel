@extends('layouts.template')

@section('title')
 Laporan Tamu
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Laporan Tamu</div>
                </div>
            </div>
            <div class="card-body">
                <div class="container">
                    <form action="{{route("laporan_tamu.preview")}}" target="_blank">
                    @csrf
                    <div class="row">
                      <div class="col-sm">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Tanggal Awal</label>
                            <input type="date" class="form-control" name="tanggal1">
                        </div>
                      </div>
                      <div class="col-sm">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Tanggal Akhir</label>
                            <input type="date" class="form-control" name="tanggal2">
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                  </div>
            </div>
        </div>
    </div>
    
    
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true,
            "oLanguage": {
                "sZeroRecords": "",
                "sEmptyTable": ""
            }
        });
    });

    // window.onload = function () {
        var app = new Vue({
            el:'#app',
            data:{
                id_pembayaran:"",
                tamu:"",
                nama_tamu:"",
                kategori_id:"",
                harga_kamar:"",
                tamu_id:"",
                status:"",
                nama_kategori:"",
                kamar:"",
                ruangan:"",
                layanan:"",
                nama_ruangan:"",
                nama_kamar:"",
                nama_layanan:"",
                kamar_id:"",
                ruangan_id:"",
                layanan_id:"",
                total_harga:"",
                cara_bayar:"",
                datas:[],
                bayar:[],

            },
            mounted(){
                this.getData();
            },

            methods:{
                getData(){
                    var self=this;
                    axios.get("{{route('pembayaran.data')}}").then(function(response){
                        self.datas = response.data;
                    });
                },

                addData(){
                    var self=this;
                    var url="{{route('kategori.add')}}";
                    
                    var formData = new FormData();
                    formData.append("nama_kategori", self.nama_kategori);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                    if(self.nama_kategori == null){
                        swal("Error", "Nama Kategori Tidak Boleh Kosong", "error");
                    }
                    else if(self.status == null){
                        swal("Error", "Status Tidak Boleh Kosong", "error");
                    }
                    else{
                        axios.post(url, formData).then(function(response){
                            $('#tambahKategoriModal').modal('hide');
                            self.getData();
                            self.addBtn();
                            swal("Success", "Data berhasil disimpan", "success");
                        }).catch(function(err){
                            swal("Error", "Koneksi ke Database Gagal", "error");
                        })
                    }
                },

                deleteData(id_pembayaran){
                    var self=this;
                    var url = "{{route('pembayaran.delete')}}"+"/"+id_pembayaran;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Sukses", "Data berhasil dihapus", "error");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                searchData(id_pembayaran){
                    var url = "{{route('pembayaran.data')}}" + "/" + id_pembayaran;
                    var self=this;
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    if (!konfirm) {
                        $('#editKategoriModal').modal('hide')
                        return false;
                    }else{
                        $('#editKategoriModal').modal('show')
                    }
                    axios.get(url).then(function (response){
                        self.tamu_id = response.data.tamu_id;
                        self.status = response.data.status;
                        self.id_pembayaran = response.data.id_pembayaran;
                    })
                },

                addBtn(){
                    var url = "{{route('kategori.data')}}";
                    var self=this;
                    axios.get(url).then(function (response){
                        self.nama_kategori = null;
                        self.status = null;
                        self.keterangan = null;
                        self.id_kategori = null;
                    })
                },

                updateData(){
                    var self = this;
                    var url = "{{route('pembayaran.update')}}";
                    console.log("save");

                    var formData = new FormData();
                    formData.append("tamu_id", self.tamu_id);
                    formData.append("status", self.status);
                    formData.append("id_pembayaran", self.id_pembayaran);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Sukses", "Data berhasil diupdate", "success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                bayarData(id_pembayaran){
                    var url = "{{route('pembayaran.pembayaran_list')}}" + "/" + id_pembayaran;
                    var self=this;
                    swal("Error","Masih dalam pengerjaan","error");
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    if (!konfirm) {
                        $('#pembayaranModal').modal('hide');
                        return false;
                    }else{
                        $('#pembayaranModal').modal('show');
                        axios.get(url).then(function (response){
                            self.bayar = response.data;
                            // 
                        });
                    }
                },

                // getTamu() {
                //     var self = this;
                //     axios.get("{{route('tamu.data')}}").then(function(response) {
                //         self.tamulist = response.data;
                //     });
                //     console.log("data berhasil diload");
                //     // this.$swal('Hello Vue world!!!');
                // },
            }
        })

    
    </script>

@endsection
