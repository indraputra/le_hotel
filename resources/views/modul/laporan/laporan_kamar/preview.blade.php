<!DOCTYPE html>
<html>

<head>
    <title>Print Laporan Data Barang</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('original/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap-grid.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fontawesome/css/all.css')}}">
    <style type="text/css" media="screen">
        .judul .title {
            font-size: 20px;
            font-weight: bold;
        }
        
        .tables {
            padding-top: 30px;
        }
    </style>
</head>

<body>
    <div class="judul">

        <div class="title" style="text-align: center; color: black; font-size: 32px; font-style: bold;">
            SMESA EDOTEL
        </div>
        <div class="title" style="text-align: center; color: red; font-size: 24px;">
            Laporan Kamar
        </div>


        <table>
            <tr>
                <th>Periode : </th>
                <th style="font-style: italic;">{{$input1}} s/d {{$input2}}</th>
            </tr>
        </table>
        <div>Total : {{$count}} data</div>
        <!-- <div>Periode : {{$today}}</div> -->

        
    </div>
    <div class="container">
    <div class="tables" style="padding-top: 30px;">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Nama Kamar</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Fasilitas</th>
                    <th scope="col">Harga Kamar</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kamar as $no=> $data)
                <tr>
                    <td>{{$no+1}}</td>
                    <td>{{$data->nama_kamar}}</td>
                    <td>{{$data->kategori[0]->nama_kategori}}</td>
                    <td>{{$data->fasilitas[0]->nama_fasilitas}}</td>
                    <td>{{$data->harga_kamar}}</td>
                    <td>{{$data->status}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    </div>
    <script type="text/javascript">
        window.print();
    </script>

</body>

</html>