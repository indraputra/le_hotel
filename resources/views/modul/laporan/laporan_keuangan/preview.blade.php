<!DOCTYPE html>
<html>

<head>
    <title>Print Laporan Data Keuangan</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('original/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap-grid.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fontawesome/css/all.css')}}">
    <style type="text/css" media="screen">
        .judul .title {
            font-size: 20px;
            font-weight: bold;
        }
        
        .tables {
            padding-top: 30px;
        }
    </style>
</head>

<body>
    <div class="judul">

        <div class="title" style="text-align: center; color: black; font-size: 32px; font-style: bold;">
            SMESA EDOTEL
        </div>
        <div class="title" style="text-align: center; color: red; font-size: 24px;">
            Laporan Keuangan
        </div>


        <table>
            <tr>
                <th>Periode : </th>
                <th style="font-style: italic;">{{$input1}} s/d {{$input2}}</th>
            </tr>
        </table>
        <div>Total : {{$count}} data</div>
        <!-- <div>Periode : {{$today}}</div> -->

        
    </div>
    <div class="container">
    <div class="tables" style="padding-top: 30px;">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Pembelian</th>
                    <th scope="col">Total Harga</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kamar as $no=> $data)
                <?php $harga = number_format($data->total_harga,0,",",".") ?>
                <tr>
                    <td>{{$no+1}}</td>
                    <td>{{$data->pembelian}}</td>
                    <td>Rp {{$harga}}</td>
                </tr>
                @endforeach
                <?php $all_price = number_format($grand_total,0,",",".") ?>
                <tr>
                    <th colspan="2">Grand Total</th>
                    <th>Rp {{$all_price}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    </div>
    <script type="text/javascript">
        window.print();
    </script>

</body>

</html>