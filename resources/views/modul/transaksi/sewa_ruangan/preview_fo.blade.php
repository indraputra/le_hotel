<!DOCTYPE html>
<html>

<head>
    <title>Cash Receipt</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('original/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap-grid.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fontawesome/css/all.css')}}">
    <style type="text/css" media="screen">
        .judul .title {
            font-size: 20px;
            font-weight: bold;
        }
        
        .tables {
            padding-top: 30px;
        }
    </style>
</head>

<body>
    <div class="judul">
        <div class="no" style="text-align: right;">
            No :...........................................
        </div>
        <div style="margin-top: 20px;position: fixed;">
            <img class="main-logo" src="{{asset('template/img/logo/logonyar.png')}}" alt="" />
        </div>
        <div class="title" style="text-align: center; color: black; font-size: 32px; font-style: bold;">
            SMESA EDOTEL
        </div>
        <div class="title" style="text-align: center; color: black; font-size: 24px;">
            CASH RECEIPT
        </div>

        <div style="position: fixed; margin-left: 170px;margin-top: 70px">
            <p>Pembayaran Check In</p>
        </div>
        <div style="position: fixed; margin-left: 170px;margin-top: 100px">
            <?php $harga = number_format($data,0,",",".") ?>
            <p><?php echo $harga; ?>,00</p>
        </div>

        <div style="position: fixed; margin-left: 170px;margin-top: 40px">
            <?php 

    // FUNGSI TERBILANG OLEH : MALASNGODING.COM
    // WEBSITE : WWW.MALASNGODING.COM
    // AUTHOR : https://www.malasngoding.com/author/admin


            function penyebut($nilai) {
                $nilai = abs($nilai);
                $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
                $temp = "";
                if ($nilai < 12) {
                    $temp = " ". $huruf[$nilai];
                } else if ($nilai <20) {
                    $temp = penyebut($nilai - 10). " belas";
                } else if ($nilai < 100) {
                    $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
                } else if ($nilai < 200) {
                    $temp = " seratus" . penyebut($nilai - 100);
                } else if ($nilai < 1000) {
                    $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
                } else if ($nilai < 2000) {
                    $temp = " seribu" . penyebut($nilai - 1000);
                } else if ($nilai < 1000000) {
                    $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
                } else if ($nilai < 1000000000) {
                    $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
                } else if ($nilai < 1000000000000) {
                    $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
                } else if ($nilai < 1000000000000000) {
                    $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
                }     
                return $temp;
            }

            function terbilang($nilai) {
                if($nilai<0) {
                    $hasil = "minus ". trim(penyebut($nilai));
                } else {
                    $hasil = trim(penyebut($nilai));
                }           
                return $hasil;
            }


            echo terbilang($data);
            ?>
        </div>

        <div style="position: fixed; margin-left: 170px;margin-top: 130px">
            <p>{{$today}}</p>
        </div>
        <table style="margin-top: 40px;font-size: 20px">
            <tr style="margin:50px">
                <th>Nama Amount :...............................................................................................................................................................</th>
            </tr><tr style="margin:50px">
                <th>Payment :........................................................................................................................................................................</th>
            </tr>
            <tr style="margin:50px">
                <th>Amounts :Rp .................................................................................................................................................................</th>
            </tr>
            <tr style="margin:50px">
                <th>Surabaya, ......................................................................................................................................................................</th>
            </tr>
        </table>

        <table width="100%" style="margin-top: 60px">
            <thead>
                <tr>
                    <th width="50%" style="text-align: center;">Guest</th>
                    <th width="50%" style="text-align: center;">Receiver’s</th>
                </tr>
            </thead>
        </table>

        <table width="100%" style="margin-top: 60px">
            <thead>
                <tr>
                    <th width="50%" style="text-align: center;">(...............................)</th>
                    <th width="50%" style="text-align: center;">(...............................)</th>
                </tr>
            </thead>
            <tbody style="margin-top: 50px">
            </tbody>
        </table>
    </div>
    <div class="container">

    </div>
    <script type="text/javascript">
        window.print();
    </script>

</body>

</html>