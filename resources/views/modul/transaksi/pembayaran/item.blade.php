@extends('layouts.template')

@section('title')
 Item Kasir
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Data Item Kasir </div>
                    <div class="space-button">
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal" @click="addBtn()">
                            Tambah Transaksi
                        </button>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#cicilModal" @click="addBtn()">
                            Tambah Cicilan
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <center><h1>Daftar Pembelian</h1></center>
                <table id="" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Pembelian</th>
                            <th>Harga</th>
                            <th width="130">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas">
                            <td>@{{++index}}</td>
                            <td>@{{data.pembelian}}</td>
                            <td>@{{data.total_harga | currency}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_pembayaran)" ><i class="fas fa-edit"></i></button>
                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_pembayaran)"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">Grand Total</th>
                            <th colspan="2">@{{total | currency}}</th>
                        </tr>
                    </tbody>
                </table>

                <center><h1>Daftar Dibayarkan</h1></center>
                <table id="" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Pembelian</th>
                            <th>Harga</th>
                            <th width="130">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in cicilan">
                            <td>@{{++index}}</td>
                            <td>@{{data.keterangan}}</td>
                            <td>@{{data.bayar | currency}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_pembayaran)" ><i class="fas fa-edit"></i></button>
                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_pembayaran)"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">Total Bayar</th>
                            <th colspan="2">@{{cicilans | currency}}</th>
                        </tr>
                    </tbody>
                </table>

                <!-- New Harga -->
                <input type="hidden" name="" v-bind:value="total">
                <!-- End New Harga -->

                <div class="row">
                    <div class="col-9"></div>
                    <div class="col-3">
                        <button type="button" class="btn btn-primary">Bayar</button>
                        <button type="button" class="btn btn-info" @click="maintance({{$id_pembayaran}})">Kwitansi</button>
                        <a href="../../pembayaran"><button type="button" class="btn btn-secondary">Close</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="cicilModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Tambah Cicilan</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Grand Total</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="total_pembayaran" v-bind:value="total" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Bayar</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="bayar" onkeyup="sumKembalian();" v-model="bayar">
                            <input type="hidden" class="form-control" id="pembayaran_id" value="{{$id_pembayaran}}">
                            <input type="hidden" class="form-control" id="cicilan_awal" v-bind:value="cicilans">
                            <input type="hidden" class="form-control" id="cicilan_akhir">
                            <input type="text" class="form-control" id="cek" v-bind:value="total">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Kembalian</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="kembalian" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="keterangan">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal" @click="addCicilan()" >Submit</button>
                </div>
              </div>
            </div>
          </div>

    {{-- Modal Tambah Transaksi --}}
    <div class="modal fade bd-example-modal-lg" id="tambahKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Tambah Transaksi</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="box-body"> --}}
                        {{-- <div class="form-group">
                            <label class="col-sm-3 control-label">Kode</label> --}}
    
                            {{-- <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="kode"  placeholder="Kode Produk">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal">Browse</button>
                                    </span>
                                </div>
                            </div> --}}
                        {{-- </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nama Produk</label>
                            <div class="col-sm-9">
                                <input type="text" id="nama" class="form-control" placeholder="Nama Produk">
                            </div>
                        </div>
                    </div> --}}

                    <div class="form-group">
                        <label for="">Pembelian</label>
                        {{-- <input type="input" id="nama_kategori" class="form-control" name="nama_kategori" v-model="nama_kategori" required> --}}
                        <div class="input-group">
                            <input type="text" class="form-control" id="nama">
                            <input type="hidden" class="form-control" id="pembayaran_id" value="{{$id_pembayaran}}">
                            <input type="hidden" class="form-control" id="harga_awal" v-bind:value="total">
                            <input type="hidden" class="form-control" id="harga_akhir">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal">Browse</button>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Total Beli</label>
                        <input type="input" id="total_beli" class="form-control" name="nama_kategori" onkeyup="TotalHarga();" required>
                    </div>

                    <div class="form-group">
                        <label for="">Harga Satuan</label>
                        <input type="input" id="harga_satuan" class="form-control" name="nama_kategori" onkeyup="TotalHarga();" required>
                    </div>

                    <div class="form-group">
                        <label for="">Total Harga</label>
                        <input type="input" id="total_harga" class="form-control" name="nama_kategori" disabled>
                    </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary"  data-toggle="modal" data-target="#loadingModal" @click="addData()" >Submit</button>
                </div>
              </div>
            </div>
          </div>

          {{-- Modal Daftar layanan --}}
    <div id="modal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form role="form" id="form-tambah" method="post" action="input.php">
                    <div class="modal-header">
                        <center>
                        <h3 class="modal-title">Pilih Produk</h3>
                        </center>
                    </div>
                        <div class="modal-body">                           
                                <table width="100%" class="table table-hover"  id="example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Produk</th>
                                            <th>Nama Produk</th>
                                            <th>Harga</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($layanan as $key => $data)
                                        <tr id="produk" data-nama="{{$data->nama_layanan}}" data-harga="{{$data->harga_layanan}}">
                                            <td>{{$key=+1}}</td>
                                            <td>{{$data->nama_layanan}}</td>
                                            <td>{{$data->kategori_layanan[0]->nama_kategori_layanan}}</td>
                                            <td>{{$data->harga_layanan}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div> 
                        <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            </div>
                </div>
            </div>
        </div>
    {{-- End Modal Daftar layanan --}}
          
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>


<script type="text/javascript">
    var id_item = {!! json_encode($id_pembayaran) !!};
    console.log(id_item);

    $(document).ready(function(){
        $('#table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true,
            "oLanguage": {
                "sZeroRecords": "",
                "sEmptyTable": ""
            }
        });

        $('#example').DataTable();
   
        $(document).on('click', '#produk', function (e) {
            document.getElementById("nama").value = $(this).attr('data-nama');
            document.getElementById("harga_satuan").value = $(this).attr('data-harga');
            $('#modal').modal('hide');
        });
    });

    function TotalHarga(){
        var txt1 = document.getElementById("total_beli").value;
        var txt2 = document.getElementById("harga_satuan").value;
        var hasil = parseInt(txt1) * parseInt(txt2);

        var harga_awal = document.getElementById("harga_awal").value;
        var harga_akhir = hasil;
        var total_harga = parseInt(harga_awal) + parseInt(harga_akhir);

        if (!isNaN(hasil)) {
            document.getElementById('total_harga').value = hasil;
            document.getElementById('harga_akhir').value = total_harga;
        }
    }

    function sumKembalian(){
        var txt1 = document.getElementById("total_pembayaran").value;
        var txt2 = document.getElementById("bayar").value;
        var txt3 = document.getElementById("cicilan_awal").value;
        var hasil = parseInt(txt2) - parseInt(txt1);
        var total_cicilan_baru = parseInt(txt2) + parseInt(txt3);

        if (!isNaN(hasil)) {
            if (hasil < 0) {
                document.getElementById('kembalian').value = "Belum Lunas";
            }else if (hasil == 0) {
                document.getElementById('kembalian').value = "Lunas";
            }else{
                document.getElementById('kembalian').value = hasil;
            }
            document.getElementById('cicilan_akhir').value = total_cicilan_baru;
        }
    }

    Vue.filter('currency', function (value) {
        return accounting.formatMoney(value, "Rp ", 2, ".", ",")
    });
    // window.onload = function () {
        var app = new Vue({
            el:'#app',
            data:{
                id_pembayaran:"",
                tamu:"",
                nama_tamu:"",
                kategori_id:"",
                harga_kamar:"",
                tamu_id:"",
                status:"",
                nama_kategori:"",
                kamar:"",
                ruangan:"",
                pembelian:"",
                layanan:"",
                nama_ruangan:"",
                nama_kamar:"",
                nama_layanan:"",
                kamar_id:"",
                ruangan_id:"",
                layanan_id:"",
                total_harga:"",
                cara_bayar:"",
                datas:[],
                bayar:[],
                cicilan:[{
                    bayar:"",
                    keterangan:"",
                }],

            },
            computed: {
                total: function(){
                    console.log(this.datas);
                    return this.datas.reduce(function(total, item){
                        return total + item.total_harga; 
                    },0);
                },

                cicilans: function(){
                    console.log(this.cicilan);
                    return this.cicilan.reduce(function(cicilans, item){
                        return cicilans + item.bayar; 
                    },0);
                },
            },
            mounted(){
                this.getData(id_item);
                this.getCicilan(id_item);
            },

            methods:{
                getData(id_item){
                    var self=this;
                    var url = "{{route('pembayaran.itemList')}}"+"/"+id_item;
                    axios.get(url).then(function(response){
                        self.datas = response.data;
                        // console.log(self.datas);
                    });
                },

                getCicilan(id_item){
                    var self=this;
                    var url = "{{route('bayar.data')}}"+"/"+id_item;
                    axios.get(url).then(function(response){
                        self.cicilan = response.data;
                        // console.log(self.datas);
                    });
                },

                addData(){
                    var self=this;
                    var url="{{route('pembayaran.beli')}}";
                    var txtPembelian = document.getElementById("nama").value;
                    var txtTotalHarga = document.getElementById("total_harga").value;
                    var txtId = document.getElementById("pembayaran_id").value;
                    var txtHargaAkhir = document.getElementById("harga_akhir").value;
                    console.log(txtPembelian);
                    console.log(txtTotalHarga);
                    console.log(txtHargaAkhir);
                    
                    var formData = new FormData();
                    formData.append("pembelian", txtPembelian);
                    formData.append("total_harga", txtTotalHarga);
                    formData.append("pembayaran_id", txtId);
                    formData.append("grand_total", txtHargaAkhir);
                    if(txtPembelian == null){
                        swal("Error", "Nama Layanan Tidak Boleh Kosong", "error");
                    }
                    // else if(self.status == null){
                    //     swal("Error", "Status Tidak Boleh Kosong", "error");
                    // }
                    else{
                        axios.post(url, formData).then(function(response){
                            $('#tambahKategoriModal').modal('hide');
                            self.getData(id_item);
                            self.addBtn();
                            swal("Success", "Data berhasil disimpan", "success");
                        }).catch(function(err){
                            swal("Error", "Koneksi ke Database Gagal", "error");
                        })
                    }
                },

                addCicilan(){
                    var self=this;
                    var url="{{route('bayar.add')}}";
                    var txtBayar = document.getElementById("bayar").value;
                    var txtKeterangan = document.getElementById("keterangan").value;
                    var txtId = document.getElementById("pembayaran_id").value;
                    var txtCicilanAkhir = document.getElementById("cicilan_akhir").value;
                    var txtCek = document.getElementById("cek").value;
                    
                    var formData = new FormData();
                    formData.append("bayar", txtBayar);
                    formData.append("keterangan", txtKeterangan);
                    formData.append("pembayaran_id", txtId);
                    formData.append("total_bayar", txtCicilanAkhir);
                    formData.append("cek", txtCek);
                    if(txtBayar == null){
                        swal("Error", "Nama Layanan Tidak Boleh Kosong", "error");
                    }
                    // else if(self.status == null){
                    //     swal("Error", "Status Tidak Boleh Kosong", "error");
                    // }
                    else{
                        axios.post(url, formData).then(function(response){
                            $('#tambahKategoriModal').modal('hide');
                            self.getCicilan(id_item);
                            self.addBtn();
                            swal("Success", "Data berhasil disimpan", "success");
                        }).catch(function(err){
                            swal("Error", "Koneksi ke Database Gagal", "error");
                        })
                    }
                },

                deleteData(id_pembayaran){
                    var self=this;
                    var url = "{{route('pembayaran.delete')}}"+"/"+id_pembayaran;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Sukses", "Data berhasil dihapus", "error");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                searchData(id_pembayaran){
                    var url = "{{route('pembayaran.data')}}" + "/" + id_pembayaran;
                    var self=this;
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    if (!konfirm) {
                        $('#editKategoriModal').modal('hide')
                        return false;
                    }else{
                        $('#editKategoriModal').modal('show')
                    }
                    axios.get(url).then(function (response){
                        self.tamu_id = response.data.tamu_id;
                        self.status = response.data.status;
                        self.id_pembayaran = response.data.id_pembayaran;
                    })
                },

                addBtn(){
                    var url = "{{route('kategori.data')}}";
                    var self=this;
                    axios.get(url).then(function (response){
                        self.nama_kategori = null;
                        self.status = null;
                        self.keterangan = null;
                        self.id_kategori = null;
                    })
                },

                updateData(){
                    var self = this;
                    var url = "{{route('pembayaran.update')}}";
                    console.log("save");

                    var formData = new FormData();
                    formData.append("tamu_id", self.tamu_id);
                    formData.append("status", self.status);
                    formData.append("id_pembayaran", self.id_pembayaran);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Sukses", "Data berhasil diupdate", "success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                bayarData(id_pembayaran){
                    // var url = "{{route('pembayaran.pembayaran_list')}}" + "/" + id_pembayaran;
                    // var self=this;
                    // swal("Error","Masih dalam pengerjaan","error");
                    // var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    // if (!konfirm) {
                    //     $('#pembayaranModal').modal('hide');
                    //     return false;
                    // }else{
                    //     $('#pembayaranModal').modal('show');
                    //     axios.get(url).then(function (response){
                    //         self.bayar = response.data;
                    //         // 
                    //     });
                    // }

                    window.location.href = "pembayaran/item/"+id_pembayaran;
                },

                maintance(id_item){
                    var url = "../../pembayaran/invoice/"+id_item;
                    window.open(url, "_blank");
                }

                // getTamu() {
                //     var self = this;
                //     axios.get("{{route('tamu.data')}}").then(function(response) {
                //         self.tamulist = response.data;
                //     });
                //     console.log("data berhasil diload");
                //     // this.$swal('Hello Vue world!!!');
                // },
            }
        })
    </script>

@endsection
