<!DOCTYPE html>
<html>
<head>
	<title>Invoice Hotel</title>
</head>
<body>
	<style type="text/css">
		.bawah th{
			padding: 5px;
			
		}

		.pading th, td {
  		text-align: left;
 		 padding: 8px;
		}

		.color tr:nth-child(even) {
 		 background-color: #f2f2f2;
		}
		.blank{
			border: 0;
			background-color: #fff;
		}
	</style>


	<img src="{{asset('header.png')}}" style="">

	<table class="bawah" style="text-align: left;margin-top: 20px;">
		<tbody>
			<tr>
				<th >Bill To </th>
				<th >:</th>
				<th >duar</th>
			</tr>
			<tr>
				<th >Company</th>
				<th >:</th>
				<th ></th>
			</tr>
			<tr>
				<th >Address Line</th>
				<th >:</th>
				<th></th>
			</tr>
			<tr>
				<th>City, State ZIP</th>
				<th>:</th>
				<th></th>
			</tr>
			<tr>
				<th>Arrival Date</th>
				<th>:</th>
				<th></th>
			</tr>
			<tr>
				<th>Departure Date</th>
				<th>:</th>
				<th></th>
			</tr>
		</tbody>
	</table>
	<table class="color bawah padding" border="1" style="margin-top: 40px;border-collapse: collapse;border-spacing: 0;width: 100%;border: 1px solid #ddd">
		<tbody>
			<tr style="background-color: #329ea8;">
				<td style="text-align: center; width: 40px">No.</td>
				<td style="text-align: center; width: 300px">Room Type</td>
				<td style="text-align: center; width: 200px;">Total</td>
			</tr>
			<?php $no = 1 ?>
			@foreach($preview as $data)
			<?php $total_harga = number_format($data->total_harga,0,",",".") ?>
			<tr>
				<td>{{$no++}}</td>
				<td>{{$data->pembelian}}</td>
				<td>Rp {{$total_harga}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<table style="border-collapse: collapse;border-spacing: 0;width: 100%;" border="0" class="pading">
		<tbody>
			<tr>
				<td style="width: 70px;">&nbsp;</td>
				<td style="width: 423px;">&nbsp;</td>
				<td style="text-align: left; border: 1px solid #ddd;background-color: #fff;">Subtotal</td>
				<?php $sub_total_convert = number_format($sub_total,0,",",".") ?>
				<td style="border: 1px solid #ddd; background-color: #fff;">Rp. {{$sub_total_convert}}</td>
			</tr>
			<tr>
				<td style="width: 70px;">&nbsp;</td>
				<td style="width: 423px;">&nbsp;</td>
				<td style="text-align: left; border: 1px solid #ddd;background-color: #f2f2f2;">Late Fees</td>
				<td style="border: 1px solid #ddd; background-color: #f2f2f2;">Rp. </td>
			</tr>
			<tr>
				<td style="width: 70px;">&nbsp;</td>
				<td style="width: 423px;">&nbsp;</td>
				<td style="text-align: left; border: 1px solid #ddd;background-color: #fff;">Taxes 10%</td>
				<td style="border: 1px solid #ddd; background-color: #fff;">Rp. </td>
			</tr>
			<tr>
				<td style="width: 70px;">&nbsp;</td>
				<td style="width: 423px;">&nbsp;</td>
				<td style="text-align: left; border: 1px solid #ddd;background-color: #f2f2f2;">Total Due</td>
				<td style="border: 1px solid #ddd; background-color: #f2f2f2;">Rp. </td>
			</tr>
		</tbody>
	</table>
	<div style="margin-top: 200px;">
	<p style="text-align: center;">Thank You for Staying With Us!</p>
	<p style="text-align: center;font-style: bold;">edotelsmesa@gmail.com</p>
	</div>
	<!-- <div style="width: 100%; height: 500px;background-color: red;">
	<center>
	<table>
		<tbody>
			<tr>
				<td style="font-size: 20px; font-family: calibri; font-style: bold;">Smesa edOTEL</td>
				<td style="padding: 15px"></td>
				<td>Tel: 0815-53580869</td>
				<td style="padding: 15px"></td>
				<td style="font-size: 50px" rowspan="3">Hotel Invoice</td>
			</tr>
			<tr>
				<td style="font-style: italic; font-size: 12px; font-family: calibri;" rowspan="3">
				"Student Serve Better"</td>
				<td></td>
				<td>Fax: (031) 8292038</td>
				<td></td>
				
			</tr>
			<tr>
				<td></td>
				<td style="font-style: bold;">edotelsmesa@gmail.com</td>
				<td></td>
			</tr>
		</tbody>
	</table>
	</center>
	</div> -->
	
</body>
</html>