@extends('layouts.template')

@section('title')
 Kasir
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Data Kasir</div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Nama Tamu</th>
                            <th>Status</th>
                            <th>Total Harga</th>
                            <th>Total Bayar</th>
                            <th width="130">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas" v-if="data.status == 'Belum Lunas'">
                            <td>@{{++index}}</td>
                            <td>@{{data.tamu[0].nama_tamu}}</td>
                            <td>@{{data.status}}</td>
                            <td>@{{data.total_harga | currency}}</td>
                            <td>@{{data.total_bayar | currency}}</td>
                            <td>
                                <button class="btn btn btn-outline-success btn-sm" @click="bayarData(data.id_pembayaran)"><i class="fa fa-dollar"></i></button>
                                <button class="btn btn btn-outline-primary btn-sm" @click="close(data.id_pembayaran)"><i class="fa fa-lock"></i></button>
                            </td>
                        </tr>

                        <tr v-for="(data,index) in datas" v-if="data.status == 'Lunas'" style="background-color: #a5eda4">
                            <td>@{{++index}}</td>
                            <td>@{{data.tamu[0].nama_tamu}}</td>
                            <td>@{{data.status}}</td>
                            <td>@{{data.total_harga | currency}}</td>
                            <td>@{{data.total_bayar | currency}}</td>
                            <td>
                                <button class="btn btn btn-outline-success btn-sm" @click="bayarData(data.id_pembayaran)"><i class="fa fa-dollar"></i></button>
                                <button disabled class="btn btn btn-outline-primary btn-sm" @click="close(data.id_pembayaran)"><i class="fa fa-lock"></i></button>
                            </td>
                        </tr>

                        <tr v-for="(data,index) in datas" v-if="data.status == 'Closed'" style="background-color: #eda4a4">
                            <td>@{{++index}}</td>
                            <td>@{{data.tamu[0].nama_tamu}}</td>
                            <td>@{{data.status}}</td>
                            <td>@{{data.total_harga | currency}}</td>
                            <td>@{{data.total_bayar | currency}}</td>
                            <td>
                                <button disabled class="btn btn btn-outline-success btn-sm" @click="bayarData(data.id_pembayaran)"><i class="fa fa-dollar"></i></button>
                                <button  class="btn btn btn-outline-primary btn-sm" @click="open(data.id_pembayaran)"><i class="fas fa-lock-open"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="modal fade bd-example-modal-lg" id="pembayaranModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Pembayaran</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    <div class="form-group">
                            <label for="">Nama Tamu</label>
                            <select name="status" id="list-tamu" v-model="tamu_id" class="form-control">
                                @foreach($tamu as $data)
                                <option value="{{$data->id_tamu}}">{{$data->nama_tamu}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" class="form-control" name="id_pembayaran" v-model="id_pembayaran">
                          </div>
                          <div class="form-group">
                            <label for="">Status</label>
                            <select name="status" id="list" v-model="status" class="form-control">
                                <option value="Lunas">Lunas</option>
                                <option value="Belum Lunas">Belum Lunas</option>
                            </select>
                          </div>
                    
                          <table id="table" class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="no-sort" width="10">No</th>
                                    <th>Pembelian</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(data,index) in bayar">
                                    <td>@{{++index}}</td>
                                    <td>@{{data.nama_kamar}}@{{data.nama_ruangan}}@{{data.nama_layanan}}</td>
                                    <td>@{{data.total_harga}}</td>
                                </tr>
                            </tbody>
                        </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" @click="updateData()"><i class="far fa-save"></i> Save Changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Data Kasir</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                                <label for="">Nama Tamu</label>
                                <select name="status" id="list-tamu" v-model="tamu_id" class="form-control">
                                    @foreach($tamu as $data)
                                    <option value="{{$data->id_tamu}}">{{$data->nama_tamu}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" class="form-control" name="id_pembayaran" v-model="id_pembayaran">
                              </div>
                              <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" id="list" v-model="status" class="form-control">
                                    <option value="Lunas">Lunas</option>
                                    <option value="Belum Lunas">Belum Lunas</option>
                                </select>
                              </div>
                              
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" @click="updateData()"><i class="far fa-save"></i> Save Changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true,
            "oLanguage": {
                "sZeroRecords": "",
                "sEmptyTable": ""
            }
        });
    });
    Vue.filter('currency', function (value) {
        return accounting.formatMoney(value, "Rp ", 2, ".", ",")
    });
    // window.onload = function () {
        var app = new Vue({
            el:'#app',
            data:{
                id_pembayaran:"",
                tamu:"",
                nama_tamu:"",
                kategori_id:"",
                harga_kamar:"",
                tamu_id:"",
                status:"",
                nama_kategori:"",
                kamar:"",
                ruangan:"",
                layanan:"",
                nama_ruangan:"",
                nama_kamar:"",
                nama_layanan:"",
                kamar_id:"",
                ruangan_id:"",
                layanan_id:"",
                total_harga:"",
                cara_bayar:"",
                datas:[],
                bayar:[],

            },
            mounted(){
                this.getData();
            },

            methods:{
                getData(){
                    var self=this;
                    axios.get("{{route('pembayaran.data')}}").then(function(response){
                        self.datas = response.data;
                    });
                },

                addData(){
                    var self=this;
                    var url="{{route('kategori.add')}}";
                    
                    var formData = new FormData();
                    formData.append("nama_kategori", self.nama_kategori);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                    if(self.nama_kategori == null){
                        swal("Error", "Nama Kategori Tidak Boleh Kosong", "error");
                    }
                    else if(self.status == null){
                        swal("Error", "Status Tidak Boleh Kosong", "error");
                    }
                    else{
                        axios.post(url, formData).then(function(response){
                            $('#tambahKategoriModal').modal('hide');
                            self.getData();
                            self.addBtn();
                            swal("Success", "Data berhasil disimpan", "success");
                        }).catch(function(err){
                            swal("Error", "Koneksi ke Database Gagal", "error");
                        })
                    }
                },

                deleteData(id_pembayaran){
                    var self=this;
                    var url = "{{route('pembayaran.delete')}}"+"/"+id_pembayaran;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Sukses", "Data berhasil dihapus", "error");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                searchData(id_pembayaran){
                    var url = "{{route('pembayaran.data')}}" + "/" + id_pembayaran;
                    var self=this;
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                    if (!konfirm) {
                        $('#editKategoriModal').modal('hide')
                        return false;
                    }else{
                        $('#editKategoriModal').modal('show')
                    }
                    axios.get(url).then(function (response){
                        self.tamu_id = response.data.tamu_id;
                        self.status = response.data.status;
                        self.id_pembayaran = response.data.id_pembayaran;
                    })
                },

                addBtn(){
                    var url = "{{route('kategori.data')}}";
                    var self=this;
                    axios.get(url).then(function (response){
                        self.nama_kategori = null;
                        self.status = null;
                        self.keterangan = null;
                        self.id_kategori = null;
                    })
                },

                updateData(){
                    var self = this;
                    var url = "{{route('pembayaran.update')}}";
                    console.log("save");

                    var formData = new FormData();
                    formData.append("tamu_id", self.tamu_id);
                    formData.append("status", self.status);
                    formData.append("id_pembayaran", self.id_pembayaran);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Sukses", "Data berhasil diupdate", "success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                bayarData(id_pembayaran){
                    window.location.href = "pembayaran/item/" + id_pembayaran;
                },

                close(id_pembayaran){
                    var self = this;

                    var formData = new FormData();
                    formData.append("id_pembayaran", id_pembayaran);

                    var url = "{{route('pembayaran.close')}}"; 
                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Sukses","Data Pembayaran berhasil ditutup","success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                open(id_pembayaran){
                    var self = this;

                    var formData = new FormData();
                    formData.append("id_pembayaran", id_pembayaran);

                    var url = "{{route('pembayaran.open')}}"; 
                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Sukses","Data Pembayaran berhasil dibuka","success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                // getTamu() {
                //     var self = this;
                //     axios.get("{{route('tamu.data')}}").then(function(response) {
                //         self.tamulist = response.data;
                //     });
                //     console.log("data berhasil diload");
                //     // this.$swal('Hello Vue world!!!');
                // },
            }
        })

    
    </script>

@endsection
