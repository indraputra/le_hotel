@extends('layouts.template')

@section('title')
 Check Out Kamar
@endsection

@section('content')
    
<div id="app">
<div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Data Check Out Kamar</div>
                </div>
            </div>
            <div class="card-body">
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Nama Tamu</th>
                            <th>Kamar</th>
                            <th>Lama Sewa</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas">
                            <td>@{{++index}}</td>
                            <td>@{{data.tamu[0].nama_tamu}}</td>
                            <td>@{{data.kamar[0].nama_kamar}}</td>
                            <td>@{{data.lamanya}} Hari</td>
                            <td>@{{data.status}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_sewa_kamar)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-edit"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
      <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Data Kasir</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label for="">Status</label>
                    <select name="status" id="list" v-model="status" class="form-control">
                        <option value="Check In">Check In</option>
                        <option value="Check Out">Check Out</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <input type="hidden" name="" v-model="tamu_id">
                    <input type="hidden" name="" v-model="kamar_id">
                  </div>      
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" @click="updateData()"><i class="far fa-save"></i> Save Changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer ></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer ></script>
<script src = "https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer ></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true,
            "oLanguage": {
                "sZeroRecords": "",
                "sEmptyTable": ""
            }
        });
    });

    // window.onload = function () {
        var app = new Vue({
            el:'#app',
            data:{
                id_pembayaran:"",
                tamu:"",
                nama_tamu:"",
                tamu_id:"",
                kamar:"",
                kamar_id:"",
                nama_kamar:"",
                status:"",
                nama_kategori:"",
                datas:[],
            },
            mounted(){
                this.getData();
            },

            methods:{
                getData(){
                    var self=this;
                    axios.get("{{route('check_out_kamar.data')}}").then(function(response){
                        self.datas = response.data;
                    }).catch(function(err){
                        swal("Error","Koneksi Kedatabase Gagal","error");
                    })
                },

                addData(){
                    var self=this;
                    var url="{{route('kategori.add')}}";
                    
                    var formData = new FormData();
                    formData.append("nama_kategori", self.nama_kategori);
                    formData.append("status", self.status);
                    formData.append("keterangan", self.keterangan);
                    if(self.nama_kategori == null){
                        swal("Error", "Nama Kategori Tidak Boleh Kosong", "error");
                    }
                    else if(self.status == null){
                        swal("Error", "Status Tidak Boleh Kosong", "error");
                    }
                    else{
                        axios.post(url, formData).then(function(response){
                            $('#tambahKategoriModal').modal('hide');
                            self.getData();
                            self.addBtn();
                            swal("Success", "Data berhasil disimpan", "success");
                        }).catch(function(err){
                            swal("Error", "Koneksi ke Database Gagal", "error");
                        })
                    }
                },

                deleteData(id_pembayaran){
                    var self=this;
                    var url = "{{route('pembayaran.delete')}}"+"/"+id_pembayaran;

                    var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                    if (!konfirm) {
                        return false;
                    }
                    axios.get(url).then(function(resposnse){
                        // alert(response.data.status);
                        self.getData();
                        swal("Sukses", "Data berhasil dihapus", "error");
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                searchData(id_sewa_kamar){
                    var url = "{{route('sewa_kamar.data')}}" + "/" + id_sewa_kamar;
                    var self=this;
                    var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");

                    if (!konfirm) {
                        return false;
                    }

                    axios.get(url).then(function (response){
                        self.tamu_id = response.data.tamu_id;
                        self.status = response.data.status;
                        self.tamu_id = response.data.tamu_id;
                        self.kamar_id = response.data.kamar_id;
                        self.id_sewa_kamar = response.data.id_sewa_kamar;
                        if (self.status == "Check Out") {
                            $('#editKategoriModal').modal('hide');
                            swal("Error", "Guest sudah check out", "error");
                        }else{

                        }
                    })
                },

                addBtn(){
                    var url = "{{route('kategori.data')}}";
                    var self=this;
                    axios.get(url).then(function (response){
                        self.nama_kategori = null;
                        self.status = null;
                        self.keterangan = null;
                        self.id_kategori = null;
                    })
                },

                updateData(){
                    var self = this;
                    var url = "{{route('sewa_kamar.update')}}";
                    console.log("save");

                    var formData = new FormData();
                    formData.append("status", self.status);
                    formData.append("id_sewa_kamar", self.id_sewa_kamar);
                    formData.append("tamu_id", self.tamu_id);
                    formData.append("kamar_id", self.kamar_id);

                    axios.post(url, formData).then(function(response){
                        // alert(response.data.status);
                        swal("Sukses", "Data berhasil diupdate", "success");
                        self.getData();
                    }).catch(function(err){
                        swal("Error", "Koneksi ke Database Gagal", "error");
                    })
                },

                bayarData(){
                    var self=this;
                    swal("Error","Masih dalam pengerjaan","error");
                }
            }
        })
        

    
    </script>

@endsection
