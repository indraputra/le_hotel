@extends('layouts.template') 
@section('title') 
Booking 
@endsection 
@section('content')

<div id="app">
    <div class="container-fluid header-pages">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-auto mr-auto">Data Booking</div>
                    <div class="space-button">
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahKategoriModal" @click="addBtn()">
                            <i class="fas fa-plus"></i> Tambah Booking
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <p v-if="success.length">
                    <ul>
                        <div id="save-success" v-for="error in success" class="alert alert-success alert-dismissible fade show" role="alert">
                            <li>@{{error}}</li>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    </ul>
                </p>
                
                <table id="table" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="no-sort" width="10">No</th>
                            <th>Atas Nama</th>
                            <th>Ruang/Kamar</th>
                            <th>Tanggal Penempatan</th>
                            <th>Status</th>
                            <th>Harga</th>
                            <th width="170">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data,index) in datas">
                            <td>@{{++index}}</td>
                            <td>@{{data.atas_nama}}</td>
                            <td>@{{data.ruangan[0].nama_ruangan}} @{{data.kamar[0].nama_kamar}}</td>
                            <td>@{{data.tanggal_penempatan}}</td>
                            
                            <td>@{{data.status}}</td>
                            <td>@{{data.ruangan[0].harga_ruangan}} @{{data.kamar[0].harga_kamar}}</td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" @click="searchData(data.id_booking)" data-toggle="modal" data-target="#editKategoriModal"><i class="fas fa-edit"></i></button>
                                <button class="btn btn btn-outline-danger btn-sm" @click="deleteData(data.id_booking)"><i class="fas fa-trash"></i></button>
                                <button class="btn btn-outline-success btn-sm" @click="accept(data.id_booking)" data-toggle="modal" data-target="#acceptKategoriModal"><i class="fas fa-user-check"></i></button>
                                <button class="btn btn btn-outline-info btn-sm" @click="kamarCheck(data.id_booking)" data-toggle="modal" data-target="#checkinKategoriModal"><i class="fas fa-check-square"></i></button>
                                <button class="btn btn btn-outline-info btn-sm" @click="ruanganCheck(data.id_booking)" data-toggle="modal" data-target="#checkin2KategoriModal"><i class="fas fa-check-circle"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="checkin2KategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Check-in Ruangan Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Atas Nama</label>
                        <input type="input" readonly id="atas_nama" class="form-control" name="atas_nama" v-model="atas_nama" required>
                        <p v-if="errors_atas_nama.length">
                            <ul>
                                <div id="atas-nama-alert" v-for="error in errors_atas_nama" class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <li>@{{error}}</li>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </ul>
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="list">Nama Tamu</label>
                        <select id="tamu-list" class="form-control" v-model="tamu_id" style="width: 100%"><br>
                        <option></option>
                        <option v-for="(data,index) in tamulist" v-bind:value="data.id_tamu">@{{data.nama_tamu}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Penempatan</label>
                        <input type="date" class="form-control" name="tanggal_penempatan" v-model="tanggal_penempatan" required>
                        <p v-if="errors_tanggal_penempatan.length">
                            <ul>
                                <div id="tanggal-penempatan-alert" v-for="error in errors_tanggal_penempatan" class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <li>@{{error}}</li>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                                </div>
                            </ul>
                        </p>
                    </div>

                    <div class="form-group">
                        <label for="">Tanggal Check-in</label>
                        <input type="date" class="form-control" name="tanggal_check_in" v-model="tanggal_check_in" required>
                    </div>

                    <div class="form-group">
                        <label for="">Tanggal Check-out</label>
                        <input type="date" class="form-control" name="tanggal_check_in" v-model="tanggal_check_out" required>
                    </div>

                    <div class="form-group">
                        <label for="">Jumlah Pengunjung</label>
                        <input type="number" class="form-control" name="jumlah_pengunjung" v-model="jumlah_pengunjung" required>
                    </div>

                    <div class="form-group">
                        <label for="">Lama Sewa</label>
                        <input type="number" id="lama_sewa2" class="form-control" name="lamanya" v-model="lamanya" required>
                    </div>

                    <div class="form-group">
                        <label for="">Harga Kamar 1 Malam</label>
                        <input type="input" class="form-control" name="harga_kamar" readonly id="harganya_ruangan" v-model="harga_kamar" required>
                        <button type="button" class="btn btn-success" id="generate-harga" onclick="getHarga2()">Cek Harga</button>
                    </div>

                    <div class="form-group">
                        <label for="">Total biaya</label>
                        <input type="number" id="total_biaya2" class="form-control" name="total_biaya" v-model="total_biaya" required>
                    </div>

                    <div class="form-group">
                        <label for="list">Kamar</label>
                        <select name="kamar_id" id="kamar-list" v-model="kamar_id" class="form-control" readonly>
                            @foreach ($kamar as $data)
                            <option value="{{$data->id_kamar}}" harga-kamar="{{$data->harga_kamar}}"> {{ $data->nama_kamar }} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="list">Ruangan</label>
                        <select name="ruangan_id" id="ruangan-list" v-model="ruangan_id" class="form-control" readonly>
                            @foreach ($ruangan as $data)
                            <option value="{{$data->id_ruangan}}" harga-ruangan="{{$data->harga_ruangan}}"> {{ $data->nama_ruangan }} </option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" data-toggle="modal" data-dismiss="" data-target="#loadingModal" @click="checkin2()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="checkinKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Check-in Kamar Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Atas Nama</label>
                        <input type="input" readonly id="atas_nama" class="form-control" name="atas_nama" v-model="atas_nama" required>
                        <input type="hidden" readonly id="id_booking" class="form-control" name="id_booking" v-model="id_booking" required>
                        {{-- <input type="input" readonly id="status_baru" class="form-control" name="status_baru" v-model="status_baru" required> --}}
                        <p v-if="errors_atas_nama.length">
                            <ul>
                                <div id="atas-nama-alert" v-for="error in errors_atas_nama" class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <li>@{{error}}</li>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </ul>
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="list">Nama Tamu</label>
                        <select id="tamu-list" class="form-control" v-model="tamu_id" style="width: 100%"><br>
                        <option></option>
                        <option v-for="(data,index) in tamulist" v-bind:value="data.id_tamu">@{{data.nama_tamu}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Penempatan</label>
                        <input type="date" class="form-control" name="tanggal_penempatan" v-model="tanggal_penempatan" required>
                        <p v-if="errors_tanggal_penempatan.length">
                            <ul>
                                <div id="tanggal-penempatan-alert" v-for="error in errors_tanggal_penempatan" class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <li>@{{error}}</li>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                                </div>
                            </ul>
                        </p>
                    </div>

                    <div class="form-group">
                        <label for="">Tanggal Check-in</label>
                        <input type="date" class="form-control" name="tanggal_check_in" v-model="tanggal_check_in" required>
                    </div>

                    <div class="form-group">
                        <label for="">Tanggal Check-out</label>
                        <input type="date" class="form-control" name="tanggal_check_in" v-model="tanggal_check_out" required>
                    </div>

                    <div class="form-group">
                        <label for="">Jumlah Pengunjung</label>
                        <input type="number" class="form-control" name="jumlah_pengunjung" v-model="jumlah_pengunjung" required>
                    </div>

                    <div class="form-group">
                        <label for="">Lama Sewa</label>
                        <input type="number" id="lama_sewa" class="form-control" name="lamanya" v-model="lamanya" required>
                    </div>

                    <div class="form-group">
                        <label for="">Harga Kamar 1 Malam</label>
                        <input type="input" class="form-control" name="harga_kamar" readonly id="harganya_kamar" v-model="harga_kamar" required>
                        <button type="button" class="btn btn-success" id="generate-harga" onclick="getHarga()">Cek Harga</button>
                    </div>

                    <div class="form-group">
                        <label for="">Total biaya</label>
                        <input type="number" id="total_biaya1" class="form-control" name="total_biaya" v-model="total_biaya" required>
                    </div>

                    <div class="form-group">
                        <label for="list">Kamar</label>
                        <select name="kamar_id" id="kamar-list" v-model="kamar_id" class="form-control" readonly>
                            @foreach ($kamar as $data)
                            <option value="{{$data->id_kamar}}" harga-kamar="{{$data->harga_kamar}}"> {{ $data->nama_kamar }} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="list">Ruangan</label>
                        <select name="ruangan_id" id="ruangan-list" v-model="ruangan_id" class="form-control" readonly>
                            @foreach ($ruangan as $data)
                            <option value="{{$data->id_ruangan}}" harga-ruangan="{{$data->harga_ruangan}}"> {{ $data->nama_ruangan }} </option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" data-toggle="modal" data-dismiss="" data-target="#loadingModal" @click="checkin()">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="tambahKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Atas Nama</label>
                        <input type="input" id="atas_nama" class="form-control" name="atas_nama" v-model="atas_nama" required>
                        <p v-if="errors_atas_nama.length">
                            <ul>
                                <div id="atas-nama-alert" v-for="error in errors_atas_nama" class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <li>@{{error}}</li>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                                </div>
                            </ul>
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Penempatan</label>
                        <input type="date" class="form-control" name="tanggal_penempatan" v-model="tanggal_penempatan" required>
                        <p v-if="errors_tanggal_penempatan.length">
                            <ul>
                                <div id="tanggal-penempatan-alert" v-for="error in errors_tanggal_penempatan" class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <li>@{{error}}</li>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                                </div>
                            </ul>
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="list">Kamar</label>
                        <select name="kamar_id" id="kamar-list" v-model="kamar_id" class="form-control">
                            @foreach ($kamar as $data)
                            <option value="{{$data->id_kamar}}" harga-kamar="{{$data->harga_kamar}}"> {{ $data->nama_kamar }} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="list">Ruangan</label>
                        <select name="ruangan_id" id="ruangan-list" v-model="ruangan_id" class="form-control">
                            @foreach ($ruangan as $data)
                            <option value="{{$data->id_ruangan}}" harga-ruangan="{{$data->harga_ruangan}}"> {{ $data->nama_ruangan }} </option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#loadingModal" @click="addData()">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="editKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Atas Nama</label>
                        <input type="input" id="atas_nama" class="form-control" name="atas_nama" v-model="atas_nama" required>
                        <input type="hidden" id="id_booking" class="form-control" name="id_booking" v-model="id_booking" required>
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Penempatan</label>
                        <input type="date" class="form-control" name="tanggal_penempatan" v-model="tanggal_penempatan" required>
                    </div>
                    <div class="form-group">
                        <label for="list">Kamar</label>
                        <select name="kamar_id" id="list" v-model="kamar_id" class="form-control">
                                @foreach ($kamar as $data)
                                <option value="{{$data->id_kamar}}"> {{ $data->nama_kamar }} </option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="list">Ruangan</label>
                        <select name="ruangan_id" id="list" v-model="ruangan_id" class="form-control">
                                @foreach ($ruangan as $data)
                                <option value="{{$data->id_ruangan}}"> {{ $data->nama_ruangan }} </option>
                                @endforeach
                            </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" @click="updateData()">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-xl" id="acceptKategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Accept Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <form action="{{route("tamu.add")}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Nama Tamu</label>
                            <input type="input" id="atas_nama" class="form-control" name="nama_tamu" v-model="atas_nama" required>
                            <input type="hidden" id="id_booking" class="form-control" name="id_booking" v-model="id_booking" required>
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal Penempatan</label>
                            <input type="date" readonly="" class="form-control" name="tanggal_penempatan" v-model="tanggal_penempatan" required>
                        </div>
                        <div class="form-group">
                            <label for="list">Kamar</label>
                            <select readonly name="kamar_id" id="list" v-model="kamar_id" class="form-control">
                                @foreach ($kamar as $data)
                                <option value="{{$data->id_kamar}}"> {{ $data->nama_kamar }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="list">Ruangan</label>
                            <select readonly name="ruangan_id" id="list" v-model="ruangan_id" class="form-control">
                                @foreach ($ruangan as $data)
                                <option value="{{$data->id_ruangan}}"> {{ $data->nama_ruangan }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Tipe Tamu</label>
                            <select name="tipe_tamu" id="list" class="form-control" required="">
                                <option value=""></option>
                                <option value="Tamu Pelanggan">Tamu Pelanggan</option>
                                <option value="Tamu Pegawai Kantor">Tamu Pegawai Kantor</option>
                                <option value="Tamu Asing">Tamu Asing</option>
                                <option value="Tamu Ireguler">Tamu Ireguler</option>
                                <option value="Tamu Anak Anak">Tamu Anak Anak</option>
                                <option value="Tamu Invalid">Tamu Invalid</option>
                                <option value="Tamu Orang Tua">Tamu Orang Tua</option>
                                <option value="Tamu Datang Sendirian">Tamu Datang Sendirian</option>
                                <option value="Tamu Gourmets">Tamu Gourmets</option>
                                <option value="Tamu Gourmands">Tamu Gourmands</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Alamat Tamu</label>
                            <input type="input" id="atas_nama" class="form-control" autofocus="" name="alamat_tamu" required>
                        </div>
                        <div class="form-group">
                            <label for="">Tempat Lahir</label>
                            <input type="input" id="atas_nama" class="form-control" autofocus="" name="tempat_lahir" required>
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal Lahir</label>
                            <input type="date" id="atas_nama" class="form-control" autofocus="" name="tanggal_lahir" required>
                        </div>
                        <div class="form-group">
                            <label for="">No KTP</label>
                            <input type="number" id="atas_nama" class="form-control" autofocus="" name="no_ktp" required>
                        </div>
                        <div class="form-group">
                            <label for="">Agama</label>
                            <select name="agama" id="list" class="form-control" required="">
                                <option value=""></option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Khonghucu">Khonghucu</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Pendidikan</label>
                            <select name="pendidikan" id="list" class="form-control" required="">
                                <option value=""></option>
                                <option value="None">None</option>
                                <option value="TK">TK</option>
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option>
                                <option value="SMA">SMA</option>
                                <option value="SMK">SMK</option>
                                <option value="Sarjana">Sarjana</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">No Telp</label>
                            <input type="number" id="atas_nama" class="form-control" autofocus="" name="no_telp" required>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-auto mr-auto"></div>
                            <div class="space-button">
                                <button type="submit" class="btn btn-primary" @onclick="addTamu()">Submit</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
{{--
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" defer></script>
<script src="http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" defer></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" defer rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" defer></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: 'Blfrtip',
            buttons: ['excel'],
            "lengthChange": true,
            "oLanguage": {
                "sZeroRecords": "",
                "sEmptyTable": ""
            }
        });
    });

    function getHarga() {
        var list = document.getElementById('kamar-list');
        document.getElementById("harganya_kamar").value = list.options[list.selectedIndex].getAttribute('harga-kamar');
        var lamaSewa = document.getElementById("lama_sewa").value;
        var hargaSemalam = document.getElementById("harganya_kamar").value;
        var hitung = parseInt(lamaSewa) * parseInt(hargaSemalam);
        console.log(hitung);
        document.getElementById("total_biaya1").value = hitung;
    }

    function getHarga2() {
        var list = document.getElementById('ruangan-list');
        document.getElementById("harganya_ruangan").value = list.options[list.selectedIndex].getAttribute('harga-ruangan');
        var lamaSewa = document.getElementById("lama_sewa2").value;
        var hargaSemalam = document.getElementById("harganya_ruangan").value;
        var hitung = parseInt(lamaSewa) * parseInt(hargaSemalam);
        console.log(hitung);
        document.getElementById("total_biaya2").value = hitung;
    }

    // window.onload = function () {
    // import VueSweetalert2 from 'vue-sweetalert2';
    // Vue.use(VueSweetalert2);
    var app = new Vue({
        el: '#app',
        data: {
            id_booking: "",
            atas_nama: "",
            ruangan_id: "",
            kamar_id: "",
            tanggal_penempatan: "",
            status: "",
            harga_kamar: "",
            harga_ruangan: "",
            ruangan: "",
            kamar: "",
            pesan_test: 'You loaded this page on ' + new Date().toLocaleString(),
            nama_ruangan: "",
            id_tamu: "",
            nama_tamu: "",
            nama_kamar: "",
            tamu_id: "",
            jumlah_pengunjung: "",
            tanggal_check_in: "",
            tanggal_check_out: "",
            total_biaya: "",
            lamanya: "",
            status_baru:"",
            errors_atas_nama: [],
            errors_tanggal_penempatan: [],
            success: [],
            datas: [],
            tamulist: [{
                nama_tamu: ""
            }]
        },
        mounted() {
            // this.getTamu();
            this.getData();
            // $(this.$refs.vuemodal).on("hidden.bs.modal", this.doSomethingOnHidden)
        },

        methods: {
            getTamu() {
                var self = this;
                axios.get("{{route('tamu.data')}}").then(function(response) {
                    self.tamulist = response.data;
                });
                console.log("data berhasil diload");
                // this.$swal('Hello Vue world!!!');
            },

            getData() {
                var self = this;
                axios.get("{{route('booking.data')}}").then(function(response) {
                    self.datas = response.data;
                });
                // this.$swal('Hello Vue world!!!');
            },

            addData() {
                var self = this;
                var url = "{{route('booking.add')}}";

                var formData = new FormData();
                formData.append("atas_nama", self.atas_nama);
                formData.append("ruangan_id", self.ruangan_id);
                formData.append("kamar_id", self.kamar_id);
                formData.append("tanggal_penempatan", self.tanggal_penempatan);
                if (self.atas_nama == null) {
                    self.errors_atas_nama.push("Atas nama harus diisi !!");
                    console.log(self.errors_atas_nama);
                    setInterval(function() {
                        $("#atas-nama-alert").alert('close');
                    }, 6000);
                }
                if (self.tanggal_penempatan == null) {
                    self.errors_tanggal_penempatan.push("Tanggal Penempatan harus diisi !!");
                    console.log(self.errors_tanggal_penempatan);
                    setInterval(function() {
                        $("#tanggal-penempatan-alert").alert('close');
                    }, 6000);
                } else {
                    axios.post(url, formData).then(function(response) {
                        // $('#tambahKategoriModal').modal('hide');
                        self.success.push("Data berhasil diinput");
                        console.log(self.tanggal_penempatan);
                        setInterval(function() {
                            $("#save-success").alert('close');
                        }, 10000);
                        swal("Sukses", "Data berhasil disimpan", "success");
                        self.getData();
                        // self.atas_nama = null;
                        // self.tanggal_penempatan = null;
                        // self.kamar_id = null;
                        // self.ruangan_id = null;
                        // self.id_kategori = null;
                    }).catch(function(err) {
                        swal("Error", "Koneksi kedatabase gagal segera hubungi admin", "error");
                    })
                }
            },

            addTamu() {
                var self = this;
                var url = "{{route('booking.add')}}";

                var formData = new FormData();
                formData.append("atas_nama", self.atas_nama);
                formData.append("ruangan_id", self.ruangan_id);
                formData.append("kamar_id", self.kamar_id);
                formData.append("tanggal_penempatan", self.tanggal_penempatan);
                if (self.atas_nama == null) {
                    self.errors_atas_nama.push("Atas nama harus diisi !!");
                    console.log(self.errors_atas_nama);
                    setInterval(function() {
                        $("#atas-nama-alert").alert('close');
                    }, 6000);
                }
                if (self.tanggal_penempatan == null) {
                    self.errors_tanggal_penempatan.push("Tanggal Penempatan harus diisi !!");
                    console.log(self.errors_tanggal_penempatan);
                    setInterval(function() {
                        $("#tanggal-penempatan-alert").alert('close');
                    }, 6000);
                } else {
                    axios.post(url, formData).then(function(response) {
                        $('#tambahKategoriModal').modal('hide');
                        self.success.push("Data berhasil diinput");
                        setInterval(function() {
                            $("#save-success").alert('close');
                        }, 10000);
                        self.getData();
                    }).catch(function(err) {
                        swal("Error", "Koneksi kedatabase gagal segera hubungi admin", "error");
                    })
                }
            },

            deleteData(id_booking) {
                var self = this;
                var url = "{{route('booking.delete')}}" + "/" + id_booking;

                var konfirm = confirm("Apakah anda yakin akan mendelete data ini?");
                if (!konfirm) {
                    return false;
                }
                axios.get(url).then(function(resposnse) {
                    // alert(response.data.status);
                    self.getData();
                    swal("Sukses", "Data berhasil dihapus", "error");
                }).catch(function(err) {
                    swal("Error", "Koneksi kedatabase gagal segera hubungi admin", "error");
                })
            },

            searchData(id_booking) {
                var url = "{{route('booking.data')}}" + "/" + id_booking;
                var self = this;
                var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                if (!konfirm) {
                    $('#editKategoriModal').modal('hide');
                    return false;
                }
                axios.get(url).then(function(response) {
                    self.atas_nama = response.data.atas_nama;
                    self.tanggal_penempatan = response.data.tanggal_penempatan;
                    self.ruangan_id = response.data.ruangan_id;
                    self.kamar_id = response.data.kamar_id;
                    self.status = response.data.status;
                    self.id_booking = response.data.id_booking;
                    self.getTamu();
                })
            },

            ruanganCheck(id_booking) {
                var url = "{{route('booking.data')}}" + "/" + id_booking;
                var self = this;
                var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                console.log("punch");
                if (!konfirm) {
                    return false;
                    $('#checkin2KategoriModal').modal('hide');
                }
                axios.get(url).then(function(response) {
                    self.atas_nama = response.data.atas_nama;
                    self.tanggal_penempatan = response.data.tanggal_penempatan;
                    self.ruangan_id = response.data.ruangan_id;
                    self.kamar_id = response.data.kamar_id;
                    self.status = response.data.status;
                    self.id_booking = response.data.id_booking;
                    self.getTamu();
                    console.log(response.data.ruangan_id);
                    if (response.data.ruangan_id == 0) {
                        swal("Error", "Client ini tidak menyewa ruangan", "error");
                        $('#checkin2KategoriModal').modal('hide');
                    }
                    else if (response.data.status == "Pending") {
                        swal("Error", "Client ini belum di Accept", "error");
                        $('#checkin2KategoriModal').modal('hide');
                    }
                })
            },

            kamarCheck(id_booking) {
                var url = "{{route('booking.data')}}" + "/" + id_booking;
                var self = this;
                var konfirm = confirm("Apakah anda yakin akan mengupdate data ini?");
                console.log("punch");
                if (!konfirm) {
                    return false;
                }
                axios.get(url).then(function(response) {
                    self.atas_nama = response.data.atas_nama;
                    self.tanggal_penempatan = response.data.tanggal_penempatan;
                    self.ruangan_id = response.data.ruangan_id;
                    self.kamar_id = response.data.kamar_id;
                    self.status = response.data.status;
                    self.id_booking = response.data.id_booking;
                    self.harga_kamar = response.data.harga_kamar;
                    self.status_baru = "Check In";
                    self.getTamu();
                    console.log(response.data.ruangan_id);
                    if (response.data.kamar_id == 0) {
                        swal("Error", "Client ini tidak menyewa kamar", "error");
                        $('#checkinKategoriModal').modal('hide');
                    }if (response.data.status == "Arrival") {
                        swal("Error", "Client ini sudah check in", "error");
                        $('#checkinKategoriModal').modal('hide');
                    }
                    else if (response.data.status == "Pending") {
                        swal("Error", "Client ini belum di Accept", "error");
                        $('#checkinKategoriModal').modal('hide');
                    }
                })
            },

            accept(id_booking) {
                var url = "{{route('booking.data')}}" + "/" + id_booking;
                var self = this;
                var konfirm = confirm("Apakah anda yakin akan meneruskan proses ini?");
                if (!konfirm) {
                    return false;
                }
                axios.get(url).then(function(response) {
                    self.atas_nama = response.data.atas_nama;
                    self.tanggal_penempatan = response.data.tanggal_penempatan;
                    self.ruangan_id = response.data.ruangan_id;
                    self.kamar_id = response.data.kamar_id;
                    self.status = response.data.status;
                    self.id_booking = response.data.id_booking;
                    console.log(self.atas_nama + "acc guys");
                    if (response.data.status == "Accept") {
                        swal("Error", "Booking atas nama "+ self.atas_nama + " ini sudah di Accept", "error");
                        $('#acceptKategoriModal').modal('hide');
                    }
                })
                console.log(id_booking + "acc guys");
            },

            addBtn() {
                var url = "{{route('booking.data')}}";
                var self = this;
                axios.get(url).then(function(response) {
                    self.atas_nama = null;
                    self.tanggal_penempatan = null;
                    self.kamar_id = null;
                    self.ruangan_id = null;
                    self.id_kategori = null;
                })
            },

            updateData() {
                var self = this;
                var url = "{{route('booking.update')}}";
                console.log("save");

                var formData = new FormData();
                formData.append("atas_nama", self.atas_nama);
                formData.append("ruangan_id", self.ruangan_id);
                formData.append("kamar_id", self.kamar_id);
                formData.append("tanggal_penempatan", self.tanggal_penempatan);
                formData.append("id_booking", self.id_booking);

                axios.post(url, formData).then(function(response) {
                    // alert(response.data.status);
                    self.success.push("Data berhasil diupdate");
                    setInterval(function() {
                        $("#save-success").alert('close');
                    }, 10000);
                    self.getData();
                    swal("Sukses", "Data berhasil diedit", "success");
                }).catch(function(err) {
                    swal("Error", "Koneksi ke database gagal segera hubungi admin", "error");
                })
            },

            checkin() {
                var self = this;
                var url = "{{route('booking.kamarSave')}}";
                var status = "Check In";

                var formData = new FormData();
                formData.append("tamu_id", self.tamu_id);
                formData.append("kamar_id", self.kamar_id);
                formData.append("jumlah_pengunjung", self.jumlah_pengunjung);
                formData.append("tanggal_check_in", self.tanggal_check_in);
                formData.append("tanggal_check_out", self.tanggal_check_out);
                formData.append("total_biaya", document.getElementById("total_biaya1").value);
                formData.append("lamanya", self.lamanya);
                formData.append("status_baru", status);
                formData.append("id_booking", self.id_booking);
                console.log(status);

                axios.post(url, formData).then(function(response) {
                    // alert(response.data.status);
                    $('#checkinKategoriModal').modal('hide');
                    self.tamu_id = null;
                    self.kamar_id = null;
                    self.jumlah_pengunjung = null;
                    self.tanggal_check_in = null;
                    self.tanggal_check_out = null;
                    self.total_biaya1 = null;
                    self.lamanya = null;
                    self.getData();
                    swal("Sukses", "Data berhasil disimpan", "success");
                }).catch(function(err) {
                    swal("Error", "Koneksi kedatabase gagal segera hubungi admin", "error");
                })
            },

            checkin2() {
                var self = this;
                var url = "{{route('booking.ruanganSave')}}";
                var status = "Check In";

                var formData = new FormData();
                formData.append("tamu_id", self.tamu_id);
                formData.append("ruangan_id", self.ruangan_id);
                formData.append("jumlah_pengunjung", self.jumlah_pengunjung);
                formData.append("tanggal_check_in", self.tanggal_check_in);
                formData.append("tanggal_check_out", self.tanggal_check_out);
                formData.append("total_biaya", document.getElementById("total_biaya2").value);
                formData.append("lamanya", self.lamanya);
                formData.append("status_baru", status);
                formData.append("id_booking", self.id_booking);
                console.log(status);

                axios.post(url, formData).then(function(response) {
                    // alert(response.data.status);
                    $('#checkinKategoriModal').modal('hide');
                    self.tamu_id = null;
                    self.kamar_id = null;
                    self.jumlah_pengunjung = null;
                    self.tanggal_check_in = null;
                    self.tanggal_check_out = null;
                    self.total_biaya1 = null;
                    self.lamanya = null;
                    self.getData();
                    swal("Sukses", "Data berhasil disimpan", "success");
                }).catch(function(err) {
                    swal("Error", "Koneksi kedatabase gagal segera hubungi admin", "error");
                })
            }
        }
    })
</script>

@endsection