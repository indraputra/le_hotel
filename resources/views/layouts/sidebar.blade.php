<div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="home"><img class="main-logo" src="{{asset('template/img/logo/logonyar.png')}}" alt="" /></a>
                <strong><img src="{{asset('template/img/logo/logosn.png')}}" alt="" /></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <li><a href="home" aria-expanded="false"><i class="fa fa-lg fa-home" aria-hidden="true"></i> <span class="mini-click-non">Home</span></a></li>
                        <?php $check = Auth::user()->type ?>
                        @if($check == "order_guest")
                        <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-building "></i> <span class="mini-click-non">Ruangan & Kamar </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a href="{{route("kamar_og.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Kamar</span></a></li>
                                <li><a href="{{route("ruangan_og.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Ruangan</span></a></li>
                            </ul>
                        </li>
                        @endif
                        @if($check == "admin")
                            <!-- <li><a href="" aria-expanded="false"><i class="fa fa-lg fa-envelope" aria-hidden="true"></i> <span class="mini-click-non">Reviews</span></a></li> -->
                            <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-building "></i> <span class="mini-click-non">Ruangan & Kamar </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                            <li><a href="{{route("kategori.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Kategori</span></a></li>
                                <li><a href="{{route("fasilitas.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Fasilitas</span></a></li>
                                <li><a href="{{route("kamar.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Kamar</span></a></li>
                                <li><a href="{{route("ruangan.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Ruangan</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-bars "></i> <span class="mini-click-non">Layanan </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a href="{{route("layanan.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Layanan List</span></a></li>
                                <li><a href="{{route("kategori_layanan.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Kategori Layanan</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-dollar "></i> <span class="mini-click-non">Transaction </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a href="{{route("booking.calender")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Booking Calender</span></a></li>
                                <li><a href="{{route("booking.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Booking</span></a></li>
                                <li><a href="{{route("pembayaran.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Kasir</span></a></li>
                                <li><a href="{{route("sewa_kamar.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Check in Kamar</span></a></li>
                                <li><a href="{{route("sewa_ruangan.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Check in Ruangan</span></a></li>
                                <li><a href="{{route('check_out_kamar.index')}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Check out Kamar</span></a></li>
                                <li><a href="{{route('check_out_ruangan.index')}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Check out Ruangan</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-users "></i> <span class="mini-click-non">Guest </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                            <li><a href="{{route("tamu.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Guest In House</span></a></li>
                                <li><a href=""><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Guest Category</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-clone "></i> <span class="mini-click-non">Gudang </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a href="{{route("DataBarang.index")}}""><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Data Barang</span></a></li>
                                <li><a href="{{route("pesan_barang.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Pesan Barang</span></a></li>
                                <li><a href="{{route("terima_barang.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Terima Barang</span></a></li>
                                <li><a href="{{route("suplier.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Master Suplier</span></a></li>
                                <li><a href="{{route("satuan.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Master Satuan</span></a></li>
                                <li><a href="{{route("tipe_barang.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Master Tipe Barang</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-folder "></i> <span class="mini-click-non">Laporan </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a href="{{route("laporan_kamar.view")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Laporan Kamar</span></a></li>
                                <li><a href="{{route("laporan_tamu.view")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Laporan Tamu</span></a></li>
                                <li><a href="{{route("laporan_keuangan.view")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Laporan Keuangan</span></a></li>
                            </ul>
                        </li>
                        @endif
                        @if($check == "pengurus")
                            <li>
                                <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-users "></i> <span class="mini-click-non">Guest </span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                <li><a href="{{route("tamu_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Guest In House</span></a></li>
                                    <!-- <li><a href=""><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Guest Category</span></a></li> -->
                                </ul>
                            </li>

                            <li>
                                <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-dollar "></i> <span class="mini-click-non">Transaction </span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a href="{{route("booking_fo.calender")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Booking Calender</span></a></li>
                                    <li><a href="{{route("booking_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Booking</span></a></li>
                                    <li><a href="{{route("pembayaran_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Kasir</span></a></li>
                                    <li><a href="{{route("sewa_kamar_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Check in Kamar</span></a></li>
                                    <li><a href="{{route("sewa_ruangan_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Check in Ruangan</span></a></li>
                                    <li><a href="{{route('check_out_kamar_fo.index')}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Check out Kamar</span></a></li>
                                    <li><a href="{{route('check_out_ruangan_fo.index')}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Check out Ruangan</span></a></li>
                                </ul>
                            </li>

                            <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-building "></i> <span class="mini-click-non">Ruangan & Kamar </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                            <li><a href="{{route("kategori_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Kategori</span></a></li>
                                <li><a href="{{route("fasilitas_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Fasilitas</span></a></li>
                                <li><a href="{{route("kamar_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Kamar</span></a></li>
                                <li><a href="{{route("ruangan_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Ruangan</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa fa-lg fa-bars "></i> <span class="mini-click-non">Layanan </span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a href="{{route("layanan_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i> <span class="mini-sub-pro">Layanan List</span></a></li>
                                <li><a href="{{route("kategori_layanan_fo.index")}}"><i class="fa fa-file sub-icon-mg" aria-hidden="true"></i><span class="mini-sub-pro">Kategori Layanan</span></a></li>
                            </ul>
                        </li>
                        @endif

                        
                    </ul>
                </nav>
            </div>
        </nav>
    </div>