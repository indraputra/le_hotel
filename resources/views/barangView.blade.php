@extends('layouts.template2')

@section('judul')
 Barang
@endsection

@section('content')
<div class="container">
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Tambah Data Barang
</button>
<table class="table" style="margin-top: 10px">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Nama Barang</th>
      <th scope="col">Stok Barang</th>
      <th scope="col">Satuan Barang</th>
      <th scope="col">Harga Barang</th>
      <th scope="col">Keterangan</th>
    </tr>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<form action="#" method="post">
  <div class="container" >
    <div class="form-group">
      <label for="">Nama Barang</label>
      <input type="text" class="form-control" name="nama_siswa"  required oninvalid="this.setCustomValidity('mohon diisi')" oninput="setCustomValidity('')" id="" placeholder="Nama Siswa" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="">Stok Barang</label>
      <input type="text" class="form-control" name="nama_siswa"  required oninvalid="this.setCustomValidity('mohon diisi')" oninput="setCustomValidity('')" id="" placeholder="Nama Siswa" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="">Satuan Barang</label>
      <input type="text" class="form-control" name="nama_siswa"  required oninvalid="this.setCustomValidity('mohon diisi')" oninput="setCustomValidity('')" id="" placeholder="Nama Siswa" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="">Harga Barang</label>
      <input type="text" class="form-control" name="nama_siswa"  required oninvalid="this.setCustomValidity('mohon diisi')" oninput="setCustomValidity('')" id="" placeholder="Nama Siswa" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="">Keterangan</label>
      <input type="text" class="form-control" name="nama_siswa"  required oninvalid="this.setCustomValidity('mohon diisi')" oninput="setCustomValidity('')" id="" placeholder="Nama Siswa" autocomplete="off">
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
  </div>
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
@endsection