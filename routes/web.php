<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user', 'HomeController@user')->name('user');
Route::get('chart', 'AdminController@chart')->name('pendapatan.perbulan');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'is_order_guest'],function(){
    Route::group([
        'prefix'=>'',
        'as'=>'order_guest.'
    ],function(){
        Route::get('order_guest',['as'=>'index', 'uses'=>'PengurusController@pengurus']);
    });
    Route::group([
        'prefix'=>'',
        'as'=>'kamar_og.'
    ],function(){
        Route::get('/kamar_og',['as'=>'index', 'uses'=>'Project\KamarController@view_og']);
        Route::get('/kamar_og/getData/{id_kamar?}',['as'=>'data', 'uses'=>'Project\KamarController@index_og']);
        Route::get('/kamar_og/edit/{id_kamar}',['as'=>'edit', 'uses'=>'Project\KamarController@edit_og']);
        Route::post('/kamar_og/addData',['as'=>'add', 'uses'=>'Project\KamarController@store_og']);
        Route::get('/kamar_og/deleteData/{id_kamar?}',['as'=>'delete', 'uses'=>'Project\KamarController@destroy_og']);
        Route::get('/kamar_og/detail/{id_kamar}',['as'=>'show', 'uses'=>'Project\KamarController@show_og']);
        Route::put('/kamar_og/updateData/{id_kamar}',['as'=>'update', 'uses'=>'Project\KamarController@update_og']);
    });
    Route::group([
        'prefix'=>'',
        'as'=>'ruangan_og.'
    ],function(){
        Route::get('ruangan_og',['as'=>'index', 'uses'=>'Project\RuanganController@view_og']);
        Route::get('ruangan_og/getData/{id_ruangan?}',['as'=>'data', 'uses'=>'Project\RuanganController@index_og']);
        Route::post('ruangan_og/addData',['as'=>'add', 'uses'=>'Project\RuanganController@store_og']);
        Route::get('ruangan_og/deleteData/{id_ruangan?}',['as'=>'delete', 'uses'=>'Project\RuanganController@destroy_og']);
        Route::put('ruangan_og/updateData/{id_ruangan}',['as'=>'update', 'uses'=>'Project\RuanganController@update_og']);
        Route::get('ruangan_og/detail/{id_ruangan}',['as'=>'show', 'uses'=>'Project\RuanganController@show_og']);
        Route::get('ruangan_og/edit/{id_ruangan}',['as'=>'edit', 'uses'=>'Project\RuanganController@edit_og']);
    });
});

Route::group(['middleware'=>'is_pengurus'],function(){
    Route::group([
        'prefix'=>'',
        'as'=>'pengurus.'
    ],function(){
        Route::get('pengurus',['as'=>'index', 'uses'=>'PengurusController@pengurus']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'layanan_fo.'
    ],function(){
        Route::get('layanan_fo',['as'=>'index', 'uses'=>'Project\LayananController@view_fo']);
        Route::get('layanan_fo/getData/{id_layanan?}',['as'=>'data', 'uses'=>'Project\LayananController@index_fo']);
        Route::post('layanan_fo/addData',['as'=>'add', 'uses'=>'Project\LayananController@store']);
        Route::get('layanan_fo/deleteData/{id_layanan?}',['as'=>'delete', 'uses'=>'Project\LayananController@destroy_fo']);
        Route::post('layanan_fo/updateData`',['as'=>'update', 'uses'=>'Project\LayananController@update_fo']);
        Route::post('layanan_fo/import_excel`',['as'=>'import', 'uses'=>'Project\LayananController@import_excel_fo']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'kategori_layanan_fo.'
    ],function(){
        Route::get('kategori_layanan_fo',['as'=>'index', 'uses'=>'Project\KategoriLayananController@view_fo']);
        Route::get('kategori_layanan_fo/getData/{id_kategori_layanan?}',['as'=>'data', 'uses'=>'Project\KategoriLayananController@index_fo']);
        Route::post('kategori_layanan_fo/addData',['as'=>'add', 'uses'=>'Project\KategoriLayananController@store_fo']);
        Route::get('kategori_layanan_fo/deleteData/{id_kategori_layanan?}',['as'=>'delete', 'uses'=>'Project\KategoriLayananController@destroy_fo']);
        Route::post('kategori_layanan_fo/updateData',['as'=>'update', 'uses'=>'Project\KategoriLayananController@update_fo']);
        Route::post('kategori_layanan_fo/import_excel',['as'=>'import', 'uses'=>'Project\KategoriLayananController@import_excel_fo']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'tamu_fo.'
    ],function(){
        Route::get('tamu_fo',['as'=>'index', 'uses'=>'Project\TamuController@view_fo']);
        Route::get('tamu_fo/getData/{id_tamu?}',['as'=>'data', 'uses'=>'Project\TamuController@index_fo']);
        Route::post('tamu_fo/addData',['as'=>'add', 'uses'=>'Project\TamuController@store_fo']);
        Route::get('tamu_fo/deleteData/{id_tamu?}',['as'=>'delete', 'uses'=>'Project\TamuController@destroy_fo']);
        Route::post('tamu_fo/updateData',['as'=>'update', 'uses'=>'Project\TamuController@update_fo']);
    });
    Route::group([
        'prefix'=>'',
        'as'=>'fasilitas_fo.'
    ],function(){
        Route::get('fasilitas_fo',['as'=>'index', 'uses'=>'Project\FasilitasController@view_fo']);
        Route::get('fasilitas_fo/getData/{id_fasilitas?}',['as'=>'data', 'uses'=>'Project\FasilitasController@index_fo']);
        Route::post('fasilitas_fo/addData',['as'=>'add', 'uses'=>'Project\FasilitasController@store_fo']);
        Route::get('fasilitas_fo/deleteData/{id_fasilitas?}',['as'=>'delete', 'uses'=>'Project\FasilitasController@destroy_fo']);
        Route::post('fasilitas_fo/updateData',['as'=>'update', 'uses'=>'Project\FasilitasController@update_fo']);
        Route::post('fasilitas_fo/import_excel',['as' => 'import', 'uses'=>'Project\FasilitasController@import_excel_fo']);
    });
    Route::group([
        'prefix'=>'',
        'as'=>'kategori_fo.'
    ],function(){
        Route::get('kategori_fo',['as'=>'index', 'uses'=>'Project\KategoriController@view_fo']);
        Route::get('kategori_fo/getData/{id_kategori?}',['as'=>'data', 'uses'=>'Project\KategoriController@index_fo']);
        Route::post('kategori_fo/addData',['as'=>'add', 'uses'=>'Project\KategoriController@store_fo']);
        Route::get('kategori_fo/deleteData/{id_kategori?}',['as'=>'delete', 'uses'=>'Project\KategoriController@destroy_fo']);
        Route::post('kategori_fo/updateData',['as'=>'update', 'uses'=>'Project\KategoriController@update_fo']);
        Route::post('kategori_fo/import_excel',['as' => 'import', 'uses'=>'Project\KategoriController@import_excel_fo']);
    });
    Route::group([
        'prefix'=>'',
        'as'=>'kamar_fo.'
    ],function(){
        Route::get('/kamar_fo',['as'=>'index', 'uses'=>'Project\KamarController@view_fo']);
        Route::get('/kamar_fo/getData/{id_kamar?}',['as'=>'data', 'uses'=>'Project\KamarController@index_fo']);
        Route::get('/kamar_fo/edit/{id_kamar}',['as'=>'edit', 'uses'=>'Project\KamarController@edit_fo']);
        Route::post('/kamar_fo/addData',['as'=>'add', 'uses'=>'Project\KamarController@store_fo']);
        Route::get('/kamar_fo/deleteData/{id_kamar?}',['as'=>'delete', 'uses'=>'Project\KamarController@destroy_fo']);
        Route::get('/kamar_fo/detail/{id_kamar}',['as'=>'show', 'uses'=>'Project\KamarController@show_fo']);
        Route::put('/kamar_fo/updateData/{id_kamar}',['as'=>'update', 'uses'=>'Project\KamarController@update_fo']);
    });
    Route::group([
        'prefix'=>'',
        'as'=>'ruangan_fo.'
    ],function(){
        Route::get('ruangan_fo',['as'=>'index', 'uses'=>'Project\RuanganController@view_fo']);
        Route::get('ruangan_fo/getData/{id_ruangan?}',['as'=>'data', 'uses'=>'Project\RuanganController@index_fo']);
        Route::post('ruangan_fo/addData',['as'=>'add', 'uses'=>'Project\RuanganController@store_fo']);
        Route::get('ruangan_fo/deleteData/{id_ruangan?}',['as'=>'delete', 'uses'=>'Project\RuanganController@destroy_fo']);
        Route::put('ruangan_fo/updateData/{id_ruangan}',['as'=>'update', 'uses'=>'Project\RuanganController@update_fo']);
        Route::get('ruangan_fo/detail/{id_ruangan}',['as'=>'show', 'uses'=>'Project\RuanganController@show_fo']);
        Route::get('ruangan_fo/edit/{id_ruangan}',['as'=>'edit', 'uses'=>'Project\RuanganController@edit_fo']);
    });
    Route::group([
        'prefix'=>'',
        'as'=>'booking_fo.'
    ],function(){
        Route::get('booking_fo',['as'=>'index', 'uses'=>'Project\BookingController@view_fo']);
        Route::get('bookingcalender_fo',['as'=>'calender', 'uses'=>'Project\BookingController@calender_fo']);
        Route::get('booking_fo/getData/{id_booking?}',['as'=>'data', 'uses'=>'Project\BookingController@index_fo']);
        Route::get('booking_fo/getDataTamu/{id_booking?}',['as'=>'dataTamu', 'uses'=>'Project\BookingController@tamuList_fo']);
        Route::post('booking_fo/addData',['as'=>'add', 'uses'=>'Project\BookingController@store_fo']);
        Route::post('booking_fo/checkInKamar',['as'=>'kamarSave', 'uses'=>'Project\BookingController@storeKamar_fo']);
        Route::post('booking_fo/checkInRuangan',['as'=>'ruanganSave', 'uses'=>'Project\BookingController@storeRuangan_fo']);
        Route::get('booking_fo/deleteData/{id_booking?}',['as'=>'delete', 'uses'=>'Project\BookingController@destroy_fo']);
        Route::post('booking_fo/updateData',['as'=>'update', 'uses'=>'Project\BookingController@update_fo']);
    });
    Route::group([
        'prefix'=>'',
        'as'=>'sewa_kamar_fo.'
    ],function(){
        Route::get('sewa_kamar_fo',['as'=>'index', 'uses'=>'Project\SewaKamarController@view_fo']);
        Route::get('sewa_kamar_fo/getData/{id_sewa_kamar?}',['as'=>'data', 'uses'=>'Project\SewaKamarController@index_fo']);
        // Route::post('sewa_kamar_fo/addData',['as'=>'add', 'uses'=>'Project\SewaKamarController@store']);
        Route::get('sewa_kamar_fo/deleteData/{id_sewa_kamar?}',['as'=>'delete', 'uses'=>'Project\SewaKamarController@destroy_fo']);
        Route::post('sewa_kamar_fo/updateData`',['as'=>'update', 'uses'=>'Project\SewaKamarController@update_fo']);
        Route::post('sewa_kamar_fo/import_excel`',['as'=>'import', 'uses'=>'Project\SewaKamarController@import_excel_fo']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'sewa_ruangan_fo.'
    ],function(){
        Route::get('sewa_ruangan_fo',['as'=>'index', 'uses'=>'Project\SewaRuaganController@view']);
        Route::get('sewa_ruangan_fo/getData/{id_sewa_ruangan?}',['as'=>'data', 'uses'=>'Project\SewaRuaganController@index_fo']);
        // Route::post('sewa_ruangan_fo/addData',['as'=>'add', 'uses'=>'Project\SewaKamarController@store']);
        Route::get('sewa_ruangan_fo/deleteData/{id_sewa_ruangan?}',['as'=>'delete', 'uses'=>'Project\SewaRuaganController@destroy_fo']);
        Route::post('sewa_ruangan_fo/updateData`',['as'=>'update', 'uses'=>'Project\SewaRuaganController@update_fo']);
        Route::post('sewa_ruangan_fo/import_excel`',['as'=>'import', 'uses'=>'Project\SewaRuaganController@import_excel_fo']);
        Route::get('/preview_check_in_ruangan/{id_sewa_ruangan?}',['as'=>'print', 'uses'=>'Project\SewaRuaganController@print_preview_fo']); 
    });

    Route::group([
        'prefix'=>'',
        'as'=>'check_out_ruangan_fo.'
    ],function(){
        Route::get('check_out_ruangan_fo',['as'=>'index', 'uses'=>'Project\SewaRuaganController@view_check_out_fo']);
        Route::get('check_out_ruangan_fo/getData/{id_sewa_ruangan?}',['as'=>'data', 'uses'=>'Project\SewaRuaganController@index_check_out_fo']);
        // Route::post('check_out_kamar/addData',['as'=>'add', 'uses'=>'Project\SewaKamarController@store']);
        // Route::get('check_out_kamar/deleteData/{id_sewa_kamar?}',['as'=>'delete', 'uses'=>'Project\SewaKamarController@destroy']);
        // Route::post('check_out_kamar/updateData`',['as'=>'update', 'uses'=>'Project\SewaKamarController@update']);
        // Route::post('check_out_kamar/import_excel`',['as'=>'import', 'uses'=>'Project\SewaKamarController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'check_out_kamar_fo.'
    ],function(){
        Route::get('check_out_kamar_fo',['as'=>'index', 'uses'=>'Project\SewaKamarController@view_check_out_fo']);
        Route::get('check_out_kamar_fo/getData/{id_sewa_kamar?}',['as'=>'data', 'uses'=>'Project\SewaKamarController@index_check_out_fo']);
        // Route::post('check_out_kamar/addData',['as'=>'add', 'uses'=>'Project\SewaKamarController@store']);
        // Route::get('check_out_kamar/deleteData/{id_sewa_kamar?}',['as'=>'delete', 'uses'=>'Project\SewaKamarController@destroy']);
        // Route::post('check_out_kamar/updateData`',['as'=>'update', 'uses'=>'Project\SewaKamarController@update']);
        // Route::post('check_out_kamar/import_excel`',['as'=>'import', 'uses'=>'Project\SewaKamarController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'pembayaran_fo.'
    ],function(){
        Route::get('pembayaran_fo',['as'=>'index', 'uses'=>'Project\PembayaranController@view_fo']);
        Route::get('pembayaran_fo/getData/{id_pembayaran?}',['as'=>'data', 'uses'=>'Project\PembayaranController@index_fo']);
        Route::get('pembayaran_list/getData/{id_pembayaran?}',['as'=>'pembayaran_list', 'uses'=>'Project\PembayaranController@pembayaranList_fo']);
        Route::post('pembayaran_fo/addData',['as'=>'add', 'uses'=>'Project\PembayaranController@store_fo']);
        Route::post('pembayaran_fo/beli',['as'=>'beli', 'uses'=>'Project\PembayaranController@storeLayanan_fo']);
        Route::get('pembayaran_fo/deleteData/{id_pembayaran?}',['as'=>'delete', 'uses'=>'Project\PembayaranController@destroy_fo']);
        Route::post('pembayaran_fo/updateData`',['as'=>'update', 'uses'=>'Project\PembayaranController@update_fo']);
        Route::post('pembayaran_fo/import_excel`',['as'=>'import', 'uses'=>'Project\PembayaranController@import_excel_fo']);
        Route::get('pembayaran_fo/item/{id_pembayaran?}',['as'=>'item', 'uses'=>'Project\PembayaranController@item_fo']);
        Route::get('pembayaran_fo/itemList/{id_pembayaran?}',['as'=>'itemList', 'uses'=>'Project\PembayaranController@itemList_fo']);
    });
});

Route::group(['middleware'=>'is_admin'],function(){
    Route::group([
        'prefix'=>'',
        'as'=>'admin.'
    ],function(){
        Route::get('admin',['as'=>'index', 'uses'=>'AdminController@admin']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'kategori.'
    ],function(){
        Route::get('kategori',['as'=>'index', 'uses'=>'Project\KategoriController@view']);
        Route::get('kategori/getData/{id_kategori?}',['as'=>'data', 'uses'=>'Project\KategoriController@index']);
        Route::post('kategori/addData',['as'=>'add', 'uses'=>'Project\KategoriController@store']);
        Route::get('kategori/deleteData/{id_kategori?}',['as'=>'delete', 'uses'=>'Project\KategoriController@destroy']);
        Route::post('kategori/updateData',['as'=>'update', 'uses'=>'Project\KategoriController@update']);
        Route::post('kategori/import_excel',['as' => 'import', 'uses'=>'Project\KategoriController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'bayar.'
    ],function(){
        // Route::get('bayar',['as'=>'index', 'uses'=>'Project\BayarController@view']);
        Route::get('bayar/getData/{id_bayar?}',['as'=>'data', 'uses'=>'Project\BayarController@index']);
        Route::post('bayar/addData',['as'=>'add', 'uses'=>'Project\BayarController@store']);
        // Route::get('bayar/deleteData/{id_bayar?}',['as'=>'delete', 'uses'=>'Project\BayarController@destroy']);
        // Route::post('bayar/updateData',['as'=>'update', 'uses'=>'Project\BayarController@update']);
        // Route::post('bayar/import_excel',['as' => 'import', 'uses'=>'Project\BayarController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'fasilitas.'
    ],function(){
        Route::get('fasilitas',['as'=>'index', 'uses'=>'Project\FasilitasController@view']);
        Route::get('fasilitas/getData/{id_fasilitas?}',['as'=>'data', 'uses'=>'Project\FasilitasController@index']);
        Route::post('fasilitas/addData',['as'=>'add', 'uses'=>'Project\FasilitasController@store']);
        Route::get('fasilitas/deleteData/{id_fasilitas?}',['as'=>'delete', 'uses'=>'Project\FasilitasController@destroy']);
        Route::post('fasilitas/updateData',['as'=>'update', 'uses'=>'Project\FasilitasController@update']);
        Route::post('fasilitas/import_excel',['as' => 'import', 'uses'=>'Project\FasilitasController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'kamar.'
    ],function(){
        Route::get('/kamar',['as'=>'index', 'uses'=>'Project\KamarController@view']);
        Route::get('/kamar/getData/{id_kamar?}',['as'=>'data', 'uses'=>'Project\KamarController@index']);
        Route::get('/kamar/edit/{id_kamar}',['as'=>'edit', 'uses'=>'Project\KamarController@edit']);
        Route::post('/kamar/addData',['as'=>'add', 'uses'=>'Project\KamarController@store']);
        Route::get('/kamar/deleteData/{id_kamar?}',['as'=>'delete', 'uses'=>'Project\KamarController@destroy']);
        Route::get('/kamar/detail/{id_kamar}',['as'=>'show', 'uses'=>'Project\KamarController@show']);
        Route::put('/kamar/updateData/{id_kamar}',['as'=>'update', 'uses'=>'Project\KamarController@update']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'ruangan.'
    ],function(){
        Route::get('ruangan',['as'=>'index', 'uses'=>'Project\RuanganController@view']);
        Route::get('ruangan/getData/{id_ruangan?}',['as'=>'data', 'uses'=>'Project\RuanganController@index']);
        Route::post('ruangan/addData',['as'=>'add', 'uses'=>'Project\RuanganController@store']);
        Route::get('ruangan/deleteData/{id_ruangan?}',['as'=>'delete', 'uses'=>'Project\RuanganController@destroy']);
        Route::put('ruangan/updateData/{id_ruangan}',['as'=>'update', 'uses'=>'Project\RuanganController@update']);
        Route::get('ruangan/detail/{id_ruangan}',['as'=>'show', 'uses'=>'Project\RuanganController@show']);
        Route::get('ruangan/edit/{id_ruangan}',['as'=>'edit', 'uses'=>'Project\RuanganController@edit']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'DataBarang.'
    ],function(){
        Route::get('DataBarang',['as'=>'index', 'uses'=>'Project\DataBarangController@view']);
        Route::get('DataBarang/getData/{id_barang?}',['as'=>'data', 'uses'=>'Project\DataBarangController@index']);
        Route::post('DataBarang/addData',['as'=>'add', 'uses'=>'Project\DataBarangController@store']);
        Route::get('DataBarang/deleteData/{id_barang?}',['as'=>'delete', 'uses'=>'Project\DataBarangController@destroy']);
        Route::post('DataBarang/updateData',['as'=>'update', 'uses'=>'Project\DataBarangController@update']);
        Route::post('DataBarang/import_excel', ['as' => 'import', 'uses'=>'Project\DataBarangController@import_excel']);
    });
    
    Route::group([
        'prefix'=>'',
        'as'=>'pesan_barang.'
    ],function(){
        Route::get('pesan_barang',['as'=>'index', 'uses'=>'Project\PesanBarangController@view']);
        Route::get('pesan_barang/getData/{id_pesan?}',['as'=>'data', 'uses'=>'Project\PesanBarangController@index']);
        Route::post('pesan_barang/addData',['as'=>'add', 'uses'=>'Project\PesanBarangController@store']);
        Route::get('pesan_barang/deleteData/{id_pesan?}',['as'=>'delete', 'uses'=>'Project\PesanBarangController@destroy']);
        Route::post('pesan_barang/updateData',['as'=>'update', 'uses'=>'Project\PesanBarangController@update']);
        Route::post('pesan_barang/import_excel', ['as' => 'import', 'uses'=>'Project\PesanBarangController@import_excel']);
    });
    
    Route::group([
        'prefix'=>'',
        'as'=>'suplier.'
    ],function(){
        Route::get('suplier',['as'=>'index', 'uses'=>'Project\SuplierController@view']);
        Route::get('suplier/getData/{id_suplier?}',['as'=>'data', 'uses'=>'Project\SuplierController@index']);
        Route::post('suplier/addData',['as'=>'add', 'uses'=>'Project\SuplierController@store']);
        Route::get('suplier/deleteData/{id_suplier?}',['as'=>'delete', 'uses'=>'Project\SuplierController@destroy']);
        Route::post('suplier/updateData',['as'=>'update', 'uses'=>'Project\SuplierController@update']);
        Route::post('suplier/import_excel', ['as' => 'import', 'uses'=>'Project\SuplierController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'tipe_barang.'
    ],function(){
        Route::get('tipe_barang',['as'=>'index', 'uses'=>'Project\TipeBarangController@view']);
        Route::get('tipe_barang/getData/{id_tipe_barang?}',['as'=>'data', 'uses'=>'Project\TipeBarangController@index']);
        Route::post('tipe_barang/addData',['as'=>'add', 'uses'=>'Project\TipeBarangController@store']);
        Route::get('tipe_barang/deleteData/{id_tipe_barang?}',['as'=>'delete', 'uses'=>'Project\TipeBarangController@destroy']);
        Route::post('tipe_barang/updateData',['as'=>'update', 'uses'=>'Project\TipeBarangController@update']);
        Route::post('tipe_barang/import_excel', ['as' => 'import', 'uses'=>'Project\TipeBarangController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'satuan.'
    ],function(){
        Route::get('satuan',['as'=>'index', 'uses'=>'Project\SatuanController@view']);
        Route::get('satuan/getData/{id_satuan?}',['as'=>'data', 'uses'=>'Project\SatuanController@index']);
        Route::post('satuan/addData',['as'=>'add', 'uses'=>'Project\SatuanController@store']);
        Route::get('satuan/deleteData/{id_satuan?}',['as'=>'delete', 'uses'=>'Project\SatuanController@destroy']);
        Route::post('satuan/updateData',['as'=>'update', 'uses'=>'Project\SatuanController@update']);
        Route::post('satuan/import_excel', ['as'=>'import', 'uses'=>'Project\SatuanController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'booking.'
    ],function(){
        Route::get('booking',['as'=>'index', 'uses'=>'Project\BookingController@view']);
        Route::get('bookingcalender',['as'=>'calender', 'uses'=>'Project\BookingController@calender']);
        Route::get('booking/getData/{id_booking?}',['as'=>'data', 'uses'=>'Project\BookingController@index']);
        Route::get('booking/getDataTamu/{id_booking?}',['as'=>'dataTamu', 'uses'=>'Project\BookingController@tamuList']);
        Route::post('booking/addData',['as'=>'add', 'uses'=>'Project\BookingController@store']);
        Route::post('booking/checkInKamar',['as'=>'kamarSave', 'uses'=>'Project\BookingController@storeKamar']);
        Route::post('booking/checkInRuangan',['as'=>'ruanganSave', 'uses'=>'Project\BookingController@storeRuangan']);
        Route::get('booking/deleteData/{id_booking?}',['as'=>'delete', 'uses'=>'Project\BookingController@destroy']);
        Route::post('booking/updateData',['as'=>'update', 'uses'=>'Project\BookingController@update']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'kategori_layanan.'
    ],function(){
        Route::get('kategori_layanan',['as'=>'index', 'uses'=>'Project\KategoriLayananController@view']);
        Route::get('kategori_layanan/getData/{id_kategori_layanan?}',['as'=>'data', 'uses'=>'Project\KategoriLayananController@index']);
        Route::post('kategori_layanan/addData',['as'=>'add', 'uses'=>'Project\KategoriLayananController@store']);
        Route::get('kategori_layanan/deleteData/{id_kategori_layanan?}',['as'=>'delete', 'uses'=>'Project\KategoriLayananController@destroy']);
        Route::post('kategori_layanan/updateData',['as'=>'update', 'uses'=>'Project\KategoriLayananController@update']);
        Route::post('kategori_layanan/import_excel',['as'=>'import', 'uses'=>'Project\KategoriLayananController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'tamu.'
    ],function(){
        Route::get('tamu',['as'=>'index', 'uses'=>'Project\TamuController@view']);
        Route::get('tamu/getData/{id_tamu?}',['as'=>'data', 'uses'=>'Project\TamuController@index']);
        Route::post('tamu/addData',['as'=>'add', 'uses'=>'Project\TamuController@store']);
        Route::get('tamu/deleteData/{id_tamu?}',['as'=>'delete', 'uses'=>'Project\TamuController@destroy']);
        Route::post('tamu/updateData',['as'=>'update', 'uses'=>'Project\TamuController@update']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'layanan.'
    ],function(){
        Route::get('layanan',['as'=>'index', 'uses'=>'Project\LayananController@view']);
        Route::get('layanan/getData/{id_layanan?}',['as'=>'data', 'uses'=>'Project\LayananController@index']);
        Route::post('layanan/addData',['as'=>'add', 'uses'=>'Project\LayananController@store']);
        Route::get('layanan/deleteData/{id_layanan?}',['as'=>'delete', 'uses'=>'Project\LayananController@destroy']);
        Route::post('layanan/updateData`',['as'=>'update', 'uses'=>'Project\LayananController@update']);
        Route::post('layanan/import_excel`',['as'=>'import', 'uses'=>'Project\LayananController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'pembayaran.'
    ],function(){
        Route::get('pembayaran',['as'=>'index', 'uses'=>'Project\PembayaranController@view']);
        Route::get('pembayaran/getData/{id_pembayaran?}',['as'=>'data', 'uses'=>'Project\PembayaranController@index']);
        Route::get('pembayaran_list/getData/{id_pembayaran?}',['as'=>'pembayaran_list', 'uses'=>'Project\PembayaranController@pembayaranList']);
        Route::post('pembayaran/addData',['as'=>'add', 'uses'=>'Project\PembayaranController@store']);
        Route::post('pembayaran/beli',['as'=>'beli', 'uses'=>'Project\PembayaranController@storeLayanan']);
        Route::get('pembayaran/deleteData/{id_pembayaran?}',['as'=>'delete', 'uses'=>'Project\PembayaranController@destroy']);
        Route::post('pembayaran/updateData`',['as'=>'update', 'uses'=>'Project\PembayaranController@update']);
        Route::post('pembayaran/close`',['as'=>'close', 'uses'=>'Project\PembayaranController@close']);
        Route::post('pembayaran/open`',['as'=>'open', 'uses'=>'Project\PembayaranController@open']);
        Route::post('pembayaran/import_excel`',['as'=>'import', 'uses'=>'Project\PembayaranController@import_excel']);
        Route::get('pembayaran/item/{id_pembayaran?}',['as'=>'item', 'uses'=>'Project\PembayaranController@item']);
        Route::get('pembayaran/itemList/{id_pembayaran?}',['as'=>'itemList', 'uses'=>'Project\PembayaranController@itemList']);
        Route::get('/pembayaran/invoice/{id_pembayaran?}',['as'=>'preview', 'uses'=>'Project\PembayaranController@preview']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'sewa_kamar.'
    ],function(){
        Route::get('sewa_kamar',['as'=>'index', 'uses'=>'Project\SewaKamarController@view']);
        Route::get('sewa_kamar/getData/{id_sewa_kamar?}',['as'=>'data', 'uses'=>'Project\SewaKamarController@index']);
        // Route::post('sewa_kamar/addData',['as'=>'add', 'uses'=>'Project\SewaKamarController@store']);
        Route::get('sewa_kamar/deleteData/{id_sewa_kamar?}',['as'=>'delete', 'uses'=>'Project\SewaKamarController@destroy']);
        Route::post('sewa_kamar/updateData`',['as'=>'update', 'uses'=>'Project\SewaKamarController@update']);
        Route::post('sewa_kamar/import_excel`',['as'=>'import', 'uses'=>'Project\SewaKamarController@import_excel']);
        Route::get('/preview_check_in/{id_sewa_kamar?}',['as'=>'print', 'uses'=>'Project\SewaKamarController@print_preview']); 
    });

    Route::group([
        'prefix'=>'',
        'as'=>'sewa_ruangan.'
    ],function(){
        Route::get('sewa_ruangan',['as'=>'index', 'uses'=>'Project\SewaRuaganController@view']);
        Route::get('sewa_ruangan/getData/{id_sewa_ruangan?}',['as'=>'data', 'uses'=>'Project\SewaRuaganController@index']);
        // Route::post('sewa_ruangan/addData',['as'=>'add', 'uses'=>'Project\SewaKamarController@store']);
        Route::get('sewa_ruangan/deleteData/{id_sewa_ruangan?}',['as'=>'delete', 'uses'=>'Project\SewaRuaganController@destroy']);
        Route::post('sewa_ruangan/updateData`',['as'=>'update', 'uses'=>'Project\SewaRuaganController@update']);
        Route::post('sewa_ruangan/import_excel`',['as'=>'import', 'uses'=>'Project\SewaRuaganController@import_excel']);
        Route::get('/preview_check_in_ruangan/{id_sewa_ruangan?}',['as'=>'print', 'uses'=>'Project\SewaRuaganController@print_preview']); 
    });

    Route::group([
        'prefix'=>'',
        'as'=>'check_out_ruangan.'
    ],function(){
        Route::get('check_out_ruangan',['as'=>'index', 'uses'=>'Project\SewaRuaganController@view_check_out']);
        Route::get('check_out_ruangan/getData/{id_sewa_ruangan?}',['as'=>'data', 'uses'=>'Project\SewaRuaganController@index_check_out']);
        // Route::post('check_out_kamar/addData',['as'=>'add', 'uses'=>'Project\SewaKamarController@store']);
        // Route::get('check_out_kamar/deleteData/{id_sewa_kamar?}',['as'=>'delete', 'uses'=>'Project\SewaKamarController@destroy']);
        // Route::post('check_out_kamar/updateData`',['as'=>'update', 'uses'=>'Project\SewaKamarController@update']);
        // Route::post('check_out_kamar/import_excel`',['as'=>'import', 'uses'=>'Project\SewaKamarController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'check_out_kamar.'
    ],function(){
        Route::get('check_out_kamar',['as'=>'index', 'uses'=>'Project\SewaKamarController@view_check_out']);
        Route::get('check_out_kamar/getData/{id_sewa_kamar?}',['as'=>'data', 'uses'=>'Project\SewaKamarController@index_check_out']);
        // Route::post('check_out_kamar/addData',['as'=>'add', 'uses'=>'Project\SewaKamarController@store']);
        // Route::get('check_out_kamar/deleteData/{id_sewa_kamar?}',['as'=>'delete', 'uses'=>'Project\SewaKamarController@destroy']);
        // Route::post('check_out_kamar/updateData`',['as'=>'update', 'uses'=>'Project\SewaKamarController@update']);
        // Route::post('check_out_kamar/import_excel`',['as'=>'import', 'uses'=>'Project\SewaKamarController@import_excel']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'terima_barang.'
    ],function(){
        Route::get('terima_barang',['as'=>'index', 'uses'=>'Project\TerimaBarangController@view']);
        Route::get('terima_barang/getData/{id_terima_barang?}',['as'=>'data', 'uses'=>'Project\TerimaBarangController@index']);
        Route::post('terima_barang/addData',['as'=>'add', 'uses'=>'Project\TerimaBarangController@store']);
        Route::get('terima_barang/deleteData/{id_terima_barang?}',['as'=>'delete', 'uses'=>'Project\TerimaBarangController@destroy']);
        Route::post('terima_barang/updateData',['as'=>'update', 'uses'=>'Project\TerimaBarangController@update']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'laporan_kamar.'
    ],function(){
        Route::get('/laporan_kamar',['as'=>'view', 'uses'=>'Project\LaporanKamarController@view']);
        Route::get('/preview_laporan_kamar',['as'=>'preview', 'uses'=>'Project\LaporanKamarController@preview']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'laporan_tamu.'
    ],function(){
        Route::get('/laporan_tamu',['as'=>'view', 'uses'=>'Project\LaporanTamuController@view']);
        Route::get('/preview_laporan_tamu',['as'=>'preview', 'uses'=>'Project\LaporanTamuController@preview']);
    });

    Route::group([
        'prefix'=>'',
        'as'=>'laporan_keuangan.'
    ],function(){
        Route::get('/laporan_keuangan',['as'=>'view', 'uses'=>'Project\LaporanKeuanganController@view']);
        Route::get('/preview_laporan_keuangan',['as'=>'preview', 'uses'=>'Project\LaporanKeuanganController@preview']);
    });
});
